function tonotopy(data_parameters, analysis_parameters)
% tonotopy(data_parameters, analysis_parameters)
% Analyze responses to tones and plot tonotopy across recordings


if ~exist('data_parameters','var') || isempty(data_parameters)
    [data_parameters,data_parameters_file_path] = uigetfile('.m','Select data parameter file :');
    if data_parameters==0, return; end
    run(fullfile(data_parameters_file_path,data_parameters));
else
    run(data_parameters);
end

if ~exist('analysis_parameters','var') || isempty(analysis_parameters)
    [analysis_parameters,analysis_parameters_file_path] = uigetfile('.m','Select analysis parameter file :',data_parameters_file_path);
    if analysis_parameters==0, return; end
    run(fullfile(analysis_parameters_file_path,analysis_parameters));
else
    run(analysis_parameters);
end

%% Load data
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : Baseline data (x,y,t)
% data.Anat : Image of slices (x,y,slice)

% [data,param] = InitAndLoad_Old_v2(param);
[data,param] = loadDataQ2(param);


%% Normalize a ativity and plot activation maps
%data2plt = data.Resp_corr(:,:,1:28,:,:,:);
%data2plt = data.Resp(:,:,1:28,:,:,:);

if param.CCAcorr
    data2plt = data.Resp_corr;
    data.Resp = [];
else
    data2plt = data.Resp;
    data.Resp_corr = [];
end

% Remove outliers
% for i = 1:size(data2plt,5)
%     sliceDat = data2plt(:,:,:,:,i,:);
%     thr = prctile(sliceDat(:),[1 99]);
%     sliceDat(sliceDat>thr(2) | sliceDat < thr(1)) = nan;
%     data2plt(:,:,:,:,i,:) = sliceDat;
%     
%     thr = 0.4;
%     sliceDat(sliceDat>thr(2) | sliceDat < thr(1)) = nan;
%     
% end

% Smooth temporal response at the single trial level
if param.temporalSmoothing
    data2plt = smoothdata(data2plt,3,'movmean',3,'omitnan');
end

param.nSteps = size(data2plt,3);
param.nTrials = size(data2plt,4);
if isempty(param.dv_coo)
    param.dv_coo = 1:size(data2plt,1);
end
if isempty(param.pa_coo)
    param.pa_coo = 1:size(data2plt,2);
end

param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));

switch [param.baselineType num2str(param.CCAcorr)]
    case 'pre0'
        Base_mean = nanmean(data.baseline_pre,3);
    case 'post0'
        Base_mean = nanmean(data.baseline_post,3);
    case 'pre1'
        Base_mean = nanmean(data.baseline_pre_c,3);
    case 'post1'
        Base_mean = nanmean(data.baseline_post_c,3);
    case {'bin1', 'bin0'}
         % Base_mean = nanmean(data.Base(:,:,1,:),4);
%         Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),[3 4 6]);
%         Base_mean = repmat(Base_mean,[1 1 size(data.Resp,3) size(data.Resp,4) 1 size(data.Resp,6)]);

%         Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),[3 4]);
%         Base_mean = repmat(Base_mean,[1 1 size(data.Resp,3) size(data.Resp,4) 1 1]);
        
        Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),3);
        Base_mean = repmat(Base_mean,[1 1 param.nSteps 1 1 1]);
        
    case {'estimate1', 'estimate0'}
        HRF_estimate = squeeze(nanmean(data2plt,[1 2 4 5 6]));
        [~, peak_bin] = max(HRF_estimate);
        inflection_bin = peak_bin - find(diff(flip(HRF_estimate(1:peak_bin)))>-0.05,1,'first') + 1;
        
        s = size(data2plt);
        x = 0:1/param.scale_factor:((s(3)-1)/param.scale_factor);
        figure;
        plot(x,HRF_estimate,'color','k','displayname','HRF estimate')
        hold on
        plot(x(inflection_bin),HRF_estimate(inflection_bin) + 0.005*HRF_estimate(inflection_bin),'linestyle','none','marker','v','markerfacecolor','r','markeredgecolor','k','displayname','inflection point')
        plot(x(peak_bin),HRF_estimate(peak_bin)+ 0.005*HRF_estimate(inflection_bin),'linestyle','none','marker','v','markerfacecolor','r','markeredgecolor','k','displayname','peak point')
        hold off
        title('Estimate from the HRF')
        xlabel('CBV')
        ylabel('time (s)')
        legend('location','northwest')
        
        param.MeanInterval = peak_bin-1:peak_bin+1;
        param.responseWindow = [param.MeanInterval(1)-1 param.MeanInterval(end)-1] ./ param.scale_factor;
        param.baselineBin = inflection_bin-1:inflection_bin;
        
        Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),3);
        Base_mean = repmat(Base_mean,[1 1 param.nSteps 1 1 1]);

    otherwise
       error('Unknown baseline / data option.')
end

data2plt = (data2plt - Base_mean) ./ Base_mean;

% switch param.signiftype % Not sure we need that switch here. I think param.regThreshold is interpreted differently between the cases.
%     case {'pearson','zscore','pearson2'}
%         param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));
%  
%     case 'tuning'
% %         param.Artefact = 1;
%         param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));
% end

[sig] = ActivationMap(data2plt,param,param.signiftype,param.filtertype,1); % ActivationMap(data,param,signiftype,filtertype,Mask)

% PlotAllTogether(sig,data.Anat,param,'Overlay');
% PlotAllTogether(sig,data.Anat,param,'Montage');
PlotAllTogether(sig,data.Anat,param,'Montage-Overlay');

% plot evoked activity as contours
figure;
I = cat(3,imtile(data.Anat),imtile(data.Anat),imtile(data.Anat));
I = I ./ max(I(:));
image(I)
hold on
contour(imtile(squeeze(mean(sig.SignalPixMean,6))),[0.05 0.1 0.2 0.3 0.4]);
hold off

% Plot activity for each stimulus
% dat = squeeze(nanmean(sig.SignalPixMean,3));
% maxVal = max(abs(dat(:)));
% for s = 1:size(sig.SignalPixNorm,6)
%     figure;
%     im = squeeze(dat(:,:,:,s));
%     im(~logical(sig.Significance_Mask)) =  nan;
%     imagesc(imtile(im));
%     set(gca,'clim',[-0.2 0.2])
%     colormap(french)
% %     title(param.stimList{s},'interpreter','none')
%     title(num2str(param.FreqListFinal(s)),'interpreter','none')
% end


% To test in more details :
% L = squeeze(sig.SignalPixMean(:,:,:,:,:,4));
% volumeViewer(L)


%% Slice explorer
param.sigMask = isnan(sig.BF);

if sum(param.sigMask(:))==0
    param.sigMask = sig.BF == 1;
end

maskedData = data2plt;
maskedData(permute(repmat(param.sigMask,[1 1 1 size(data2plt,[3 4 6])]),[1 2 4 5 3 6])) = NaN;
while true
    slice = input('Enter slice number (0 to exit) : ');
    if slice == 0 || isempty(slice), break; end
    
    r = input('Region [1] or Pixel [2] selection mode :','s');
    switch r
        case '1'
            selectionMode = 'Region';
        case {'2',''}
            selectionMode = 'Pixel';
    end
    plotTuningCurves2(maskedData,squeeze(data.Anat(:,:,slice)),param,slice,selectionMode)
%     for i = 1:size(maskedData,5)
%         plotTuningCurves2(maskedData,squeeze(data.Anat(:,:,i)),param,i,selectionMode)
%         plotTuningCurves_throughSlice(maskedData,data.Anat,param,1:size(maskedData,5),selectionMode);
%     end
end


