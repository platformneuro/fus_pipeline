function plot_overall_fluctuation(data_parameters, analysis_parameters)

if ~exist('data_parameters','var') || isempty(data_parameters)
    [data_parameters,data_parameters_file_path] = uigetfile('.m','Select data parameter file :');
    if data_parameters==0, return; end
    run(fullfile(data_parameters_file_path,data_parameters));
else
    run(data_parameters);
end

if ~exist('analysis_parameters','var') || isempty(analysis_parameters)
    [analysis_parameters,analysis_parameters_file_path] = uigetfile('.m','Select analysis parameter file :',data_parameters_file_path);
    if analysis_parameters==0, return; end
    run(fullfile(analysis_parameters_file_path,analysis_parameters));
else
    run(analysis_parameters);
end

zscoreData = false;

%% Load data
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : Baseline data (x,y,t)
% data.Anat : Image of slices (x,y,slice)
param.AllSessions = param.Session;
for s = 1:length(param.AllSessions)
	param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));
    param.MotorStepList = param.Filter;
    param.FreqListFinal = arrayfun(@num2str,1:8,'UniformOutput',false);
    param.Session = param.AllSessions{s};
    [data,paramSlice{s}] = loadDataQ2(param);

    if paramSlice{s}.CCAcorr
        data_norm{s} = data.Resp_corr;
        data.Resp = [];
    else
        data_norm{s} = data.Resp;
        data.Resp_corr = [];
    end
    
    paramSlice{s}.nSteps = size(data_norm{s},3);
    paramSlice{s}.nTrials = size(data_norm{s},4);
    if isempty(paramSlice{s}.dv_coo)
        paramSlice{s}.dv_coo = 1:size(data_norm{s},1);
    end
    if isempty(paramSlice{s}.pa_coo)
        paramSlice{s}.pa_coo = 1:size(data_norm{s},2);
    end
    
    if zscoreData % attenpt at normalizing left eye and right eye inputs
        data_norm{s} = zscore(data_norm{s},0,[1 2 3 4 6]);
    end

    switch [paramSlice{s}.baselineType num2str(paramSlice{s}.CCAcorr)]
        case 'pre0'
            Base_mean{s} = nanmean(data.baseline_pre,3);
        case 'post0'
            Base_mean{s} = nanmean(data.baseline_post,3);
        case 'pre1'
            Base_mean{s} = nanmean(data.baseline_pre_c,3);
        case 'post1'
            Base_mean{s} = nanmean(data.baseline_post_c,3);
        case {'bin1', 'bin0'}
             % Base_mean{s} = nanmean(data.Base(:,:,1,:),4);
             % Regular version, average over trials, repeats, time
%             Base_mean{s} = nanmean(data_norm(:,:,paramSlice{s}.baselineBin,:,:,:),[3 4 6]);
%             Base_mean{s} = repmat(Base_mean{s},[1 1 size(data.Resp,3) size(data.Resp,4) 1 size(data.Resp,6)]);
            
            % Average over time only
            Base_mean{s} = nanmean(data_norm{s}(:,:,paramSlice{s}.baselineBin,:,:,:),3);
            Base_mean{s} = repmat(Base_mean{s},[1 1 size(data.Resp,3) 1 1 1]);
            
            % Average over voxels to remove slice effect. Get a constant ?
            % Maybe do two rounds of normalization ?
            % Maybe z-score the data before 
            
        otherwise
           error('Unknown baseline / data option.')
    end

    allData_norm{s} = (data_norm{s} - Base_mean{s}) ./ Base_mean{s};
%     allData_norm{s} = (data_norm - Base_mean{s});
%     allData_norm{s} = (data_norm - Base_mean{s}) ./ Base_mean{s};
    allAnat{s} = data.Anat;

end

% Plot parameters
if isempty(param.dv_coo)
    param.dv_coo = 1:size(data.Anat,1);
end
if isempty(param.pa_coo)
    param.pa_coo = 1:size(data.Anat,2);
end


%% z-score left and right eyes
% for s = 1:length(param.AllSessions)
% %     mu = nanmean(allData_norm{s}(:,:,round(param.responseWindow*param.scale_factor),:,:,:),[1 2 3 4 6]);
% %     mu = repmat(mu,[size(allData_norm{s},[1 2 3 4]) 1 size(allData_norm{s},6)]);
%     sigma = std(allData_norm{s}(:,:,round(param.responseWindow*param.scale_factor),:,:,:),0,[1 2 3 4 6],'omitnan');
%     sigma = repmat(sigma,[size(allData_norm{s},[1 2 3 4]) 1 size(allData_norm{s},6)]);
% %     allData_norm{s} = (allData_norm{s} - mu) ./ sigma;
%     allData_norm{s} = allData_norm{s} ./ sigma;
% %     max_CBV = max(allData_norm{s},[],[1 2 3 4 6]);
% %     max_CBV = repmat(max_CBV,[size(allData_norm{s},[1 2 3 4]) 1 size(allData_norm{s},6)]);
% %     allData_norm{s} = allData_norm{s} ./ max_CBV;
% end

%% Selectivity maps

for s = 1:length(param.AllSessions)
    % ActivationMap(data,param,signiftype,filtertype,Mask)
    [sig{s}] = ActivationMap(allData_norm{s},paramSlice{s},param.signiftype,param.filtertype,1);

    PlotAllTogether(sig{s},allAnat{s},param,'Montage-Overlay');
end

%% Plot anatomy
figure
imagesc(imtile(sqrt(allAnat{1})))
colormap gray
set(gca,'XTick','')
set(gca,'YTick','')
title('Anatomy')

%% Plot evoked activity difference between eyes
dataStat = nan(size(data_norm{1},[1 2 5]));
dataDiff = nan(size(data_norm{1},[1 2 5]));
dataSum = nan(size(data_norm{1},[1 2 5]));


% TO DO
% Apply ActivationMap filters to the data before computing the dominance
% indexs. Apply soothing (3D or 2D), double check pearson2 smoothing 
% (although I think it's only smoothing the BFs for display purposes).
% Apply data diff to the BF stimulus only.

% Average activity over trials and stimuli.
BFlist = 1:length(param.FreqListFinal);
BFmsk1 = round(sig{1}.BF);
BFmsk2 = round(sig{2}.BF);
CBV{1} = zeros(size(data_norm{1},[1 2 5]));
CBV{2} = zeros(size(data_norm{2},[1 2 5]));
for i = 1:length(BFlist)
    tmp = squeeze(nanmean(data_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,:,i),[3 4 6]));
    CBV{1}(BFmsk1==i) = tmp(BFmsk1==i);
    tmp = squeeze(nanmean(data_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,:,i),[3 4 6]));
    CBV{2}(BFmsk2==i) = tmp(BFmsk2==i);
end

%  CBV{1} = squeeze(nanmean(data_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,:,:),[3 4 6]));
%  CBV{2} = squeeze(nanmean(data_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,:,:),[3 4 6]));

% if strcmp(param.filtertype.Map,'3Dfilter')
%     CBV{1} = imgaussfilt3(CBV{1});
%     CBV{2} = imgaussfilt3(CBV{2});
% end

dataDiffNoNorm = CBV{1} - CBV{2};
dataSumNoNorm = CBV{1} + CBV{2};

for s = 1:size(data_norm{1},5)
    % normalized data
    dataDiff(:,:,s) = squeeze(nanmean(sig{1}.SignalPixNorm(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         - nanmean(sig{2}.SignalPixNorm(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6]));
    
     dataSum(:,:,s) = squeeze(nanmean(sig{1}.SignalPixNorm(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         + nanmean(sig{2}.SignalPixNorm(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6]));

     x = squeeze(nanmean(allData_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),3));
     y = squeeze(nanmean(allData_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),3));
%      [~,p] = ttest(x,y,'Alpha',0.05,'dim',[3 4],'Tail','both');
%      [~,p] = ttest(x,y);
%      dataStat(:,:,s) = p;
    dataStat(:,:,s) = 0;
%     dataStat(:,:,s) = zeros(size(x,1),size(x,2));

    % non normalized data    
%     dataDiffNoNorm(:,:,s) = squeeze(nanmean(data_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
%          - nanmean(data_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6]));
%     
%     dataSumNoNorm(:,:,s) = squeeze(nanmean(data_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
%          + nanmean(data_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6]));

end



% Smooth selectivity, exclude regions too small
Im_pval = imtile(dataStat<param.analysis.diffAlpha);
% Im_pval = bwareaopen(Im_pval,10,8);

% Significance mask
% Keep voxel responding at least to one eye stimulation
sigMsk = sig{1}.Significance_Mask(:,:,:) | sig{2}.Significance_Mask(:,:,:);
% sigMsk = ~isnan(sig{1}.BF(param.dv_coo,param.pa_coo,:));
% dataDiff(~sigMsk) = NaN;
% dataSum(~sigMsk) = NaN;

% figure;
% minVal = min(dataDiff(:));
% maxVal = max(dataDiff(:));
% Im = imtile(dataDiff);
% imagesc(Im)
% set(gca,'clim',[-max(abs([minVal maxVal])) max(abs([minVal maxVal]))])
% colormap(parula)
% title('%CBV difference (common scale)')
% colorbar

% Plot ispi - contra activity for responsive voxls
figure
maxVal = squeeze(max(abs(dataDiffNoNorm),[],[1 2]));
for i = 1:length(maxVal)
    dataDiffNorm(:,:,i) = dataDiffNoNorm(:,:,i) / maxVal(i);
%     dataDiffNorm(:,:,i) = dataDiff(:,:,i) ./ dataSum(:,:,i);
end
Im = imtile(dataDiffNorm);
imagesc(Im)
hold on
voxMsk = Im_pval;
voxMsk(~imtile(sigMsk)) = 0;
contour(voxMsk,1,'color','k','linewidth',1)
% set(gca,'clim',[-1 1])
set(gca,'clim',[-prctile(abs(Im(:)),99) prctile(abs(Im(:)),99)]);
colormap(french);
title('%CBV difference (individual scaling)')
colorbar


% Plot Modulation index ispi / contra for responsive voxls
figure
for i = 1:length(maxVal)
    ModIdx(:,:,i) = dataDiffNoNorm(:,:,i) ./ dataSumNoNorm(:,:,i);
end
Im = imtile(ModIdx);
imagesc(Im)
hold on
voxMsk = Im_pval;
voxMsk(~imtile(sigMsk)) = 0;
contour(voxMsk,1,'color','k','linewidth',1)
% set(gca,'clim',[-1 1])
set(gca,'clim',[-max(abs(Im(:))) max(abs(Im(:)))]);
colormap(french);
title('Modulation index ipsi/contra')
colorbar

% Plot each slice individually
% for i = 1:size(dataDiffNorm,3)
%     Im = imtile(dataDiffNorm(:,:,i));
%     figure
%     imagesc(Im)
%     hold on
%     voxMsk = Im_pval;
%     voxMsk(~imtile(sigMsk)) = 0;
%     contour(voxMsk,1,'color','w','linewidth',1)
%     % set(gca,'clim',[-1 1])
%     set(gca,'clim',[-max(abs(Im(:))) max(abs(Im(:)))]);
%     colormap(french);
%     title(sprintf('MI slice %d',i))
%     colorbar
% end

thresh = 0.15;
Im(~Im_pval) = NaN;
anat = imtile(sqrt(allAnat{1}-min(allAnat{1}(:))));
anat = anat - min(anat(:));
cont = Im;
cont(cont<0) = NaN;
ipsi = Im;
ipsi(ipsi>0) = NaN;
ipsi = abs(ipsi);
cont = abs(cont);
ipsi(ipsi > thresh) = thresh;
cont(cont > thresh) = thresh;
figure;
imfuseQ(anat,abs(ipsi),abs(cont));

% export nii dataDiffNorm 
% exportAsNifti(fullfile(param.DataPath,[param.Animal '_' param.Session '_' 'dataDiffNorm.nii']),dataDiffNorm,[0.1 0.1 0.4])

%% plot ispi/contra diff across a slice

hAnat = figure;
sliceNB = size(allData_norm{1},5);
timeperiod = floor(param.responseWindow(1)*param.scale_factor):ceil(param.responseWindow(2)*param.scale_factor);
sliceN = input('slice number : ');
while ~isempty(sliceN)
    goOn = [];
    figure(hAnat)
    hold off
%     iV = indexVectorCCAcorr2;
%     mama = max(iV); xmax = mama(2); ymax = mama(1); slicemax = mama(3);
%         voxelidx = find(iV(:,3)==sliceN);
%         voxeln = length(voxelidx);
%         imagemat = nan(ymax,xmax);
%         for vxnum = 1:voxeln
%             x = iV(voxelidx(vxnum),1);
%             y = iV(voxelidx(vxnum),2);
%             imagemat(y,x) = mean(abs(fullDataCCAcorr2(voxelidx(vxnum))),2:5);
%         end
%         imagesc(imagemat',[0 60]);
    imagesc(squeeze(allAnat{1}(:,:,sliceN)))
%     set(gca,'YDir','norm')
    colormap(hot)
    while isempty(goOn)
        figure(hAnat)
        hold on
        [cx,cy,~] = improfile; % this gets x/y coordinates of the voxels
        cx = round(cx); cy = round(cy);
        for i = 1:sliceNB
            clear vxIdx tuningLine MItl t1 t2 eyeAct
            for vxNum = 1:length(cy)
                tuningLine(vxNum,:) = [mean(allData_norm{1}(cy(vxNum),cx(vxNum),timeperiod,:,i,:),3:6) mean(allData_norm{2}(cy(vxNum),cx(vxNum),timeperiod,:,i,:),3:6)];

                t1(vxNum,:) = [mean(allData_norm{1}(cy(vxNum),cx(vxNum),timeperiod,[1 4],i,:),3:6) mean(allData_norm{2}(cy(vxNum),cx(vxNum),timeperiod,1:3,i,:),3:6)];
                t2(vxNum,:) = [mean(allData_norm{1}(cy(vxNum),cx(vxNum),timeperiod,[2 3 5],i,:),3:6) mean(allData_norm{2}(cy(vxNum),cx(vxNum),timeperiod,4:5,i,:),3:6)];
                
                eyeAct(1,vxNum,:) = mean(allData_norm{1}(cy(vxNum),cx(vxNum),timeperiod,:,i,:),[3 5 6]);
                eyeAct(2,vxNum,:) = mean(allData_norm{2}(cy(vxNum),cx(vxNum),timeperiod,:,i,:),[3 5 6]);
            end
            MItl(:,1) = (t1(:,2)-t1(:,1))./(t1(:,2)+t1(:,1));
            MItl(:,2) = (t2(:,2)-t2(:,1))./(t2(:,2)+t2(:,1));
            MIt(:,i) = mean(MItl,2);
            figure;
            subplot(2,1,1); plot(tuningLine); axis('tight'); ylabel('dCBV')
            legend({'eye 1' 'eye 2'});
            subplot(2,1,2);
            plot(MItl);
            axis('tight');
    %         ylim([-.1 .1])
            hold on
            yline(0,'HandleVisibility','off')
            xlabel('distance over cx (in vx)'); ylabel('MI(contra over ipsi)')
            legend({'first trials' 'last trials'});
        end
        figure
        plot(MIt)
        title('MI lines')
        xlabel('distance over cx (in vx)'); ylabel('MI(contra over ipsi)')
        legend(param.Filter)
        
        [R,P] = corrcoef(MIt)
        
        goOn = input('Continue : ');
    end
    sliceN = input('slice number : ');
end

%% Laynii mapping

layers = [];
col = [];
hold on
% mapidx = [1 2 1 2];
mapidx = 1:size(dataDiffNorm,3);

projImg = nan(size(dataDiffNorm));
segMap = nan(size(dataDiffNorm));

layerMap = niftiread('C:\Users\Quentin Gaucher\code\LAYNII\test_data\param_Rambol_01_segmented_layers_equidist.nii');
colMap = niftiread('C:\Users\Quentin Gaucher\code\LAYNII\test_data\param_Rambol_01_segmented_columns50.nii');

for a = 1:size(dataDiffNorm,3)
%     [mappedData, projectedData, divMap] = mapOnCol(dataDiffNorm(:,:,a),...
%         paramSlice{1}.dataParam.laynii.layers(param.dv_coo,param.pa_coo,a),...
%         paramSlice{1}.dataParam.laynii.columns(param.dv_coo,param.pa_coo,a),...
%         layers,col,[],0);
%     [mappedData, projectedData, divMap] = mapOnCol(dataDiffNorm(:,:,a),...
%             paramSlice{1}.dataParam.laynii.layers(param.dv_coo,param.pa_coo,mapidx(a)),...
%             paramSlice{1}.dataParam.laynii.columns(param.dv_coo,param.pa_coo,mapidx(a)),...
%             layers,col,[],0);
    [mappedData, projectedData, divMap] = mapOnCol(dataDiffNorm(:,:,a),...
                layerMap(:,:,a),...
                colMap(:,:,a),...
                layers,col,[],0);
    mappedData2 = cellfun(@mean,mappedData);
    projImg(:,:,a) = projectedData;
    segMap(:,:,a) = divMap;
    
%     figure(h)
%     hold on
%     plot((mappedData2))

    figure
    imagesc(mappedData2)
%     set(gca,'clim',[-max(max(abs(dataDiffNorm(:,:,a)))) max(max(abs(dataDiffNorm(:,:,a))))])
    set(gca,'clim',[-0.05 0.05])
    colormap(french)

end
figure;
imagesc(imtile(projImg))
set(gca,'clim',[-0.05 0.05])
colormap(french)

figure;
imagesc(imtile(segMap))
colormap(lines)

exportAsNifti(fullfile(param.DataPath,[param.Animal '_' param.Session '_' 'columnProjection.nii']),projImg,[0.1 0.1 0.4])
projImg2 = projImg;
projImg2(projImg2>=0) = 1;
projImg2(projImg2<0) = 2;
exportAsNifti(fullfile(param.DataPath,[param.Animal '_' param.Session '_' 'columnProjection2.nii']),projImg2,[0.1 0.1 0.4])


%% Plot evoked activity difference through time for each eyes
dataStatTime1 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
dataDiffTime1 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
dataSumTime1 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
for s = 2:size(data_norm,5)
    dataDiffTime1(:,:,s-1) = squeeze(nanmean(sig{1}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         - nanmean(sig{1}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,1,:),[3 4 6]));
    
     dataSumTime1(:,:,s-1) = squeeze(nanmean(sig{1}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         + nanmean(sig{1}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,1,:),[3 4 6]));

     x = squeeze(nanmean(allData_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,1,:),3));
     y = squeeze(nanmean(allData_norm{1}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),3));
     [~,p] = ttest(x,y,'Alpha',0.01,'dim',[3 4],'Tail','both');
     dataStatTime1(:,:,s-1) = p;
end

dataStatTime2 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
dataDiffTime2 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
dataSumTime2 = nan([size(data_norm,[1 2]) size(data_norm,5)-1]);
for s = 2:size(data_norm,5)
    dataDiffTime2(:,:,s-1) = squeeze(nanmean(sig{2}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         - nanmean(sig{2}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,1,:),[3 4 6]));
    
     dataSumTime2(:,:,s-1) = squeeze(nanmean(sig{2}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,s,:),[3 4 6])...
         + nanmean(sig{2}.SignalPixNorm(param.dv_coo,param.pa_coo,round(param.responseWindow*param.scale_factor),:,1,:),[3 4 6]));

     x = squeeze(nanmean(allData_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,1,:),3));
     y = squeeze(nanmean(allData_norm{2}(:,:,round(param.responseWindow*param.scale_factor),:,s,:),3));
     [~,p] = ttest(x,y,'Alpha',0.01,'dim',[3 4],'Tail','both');
     dataStatTime2(:,:,s-1) = p;
end



%% Option to forward all variables to main worspace.
% Dirty, for debug or exploration only.
% Use with caution.
if param.throwVariablesOut
    throwAllVariablesToMain();
    disp('All variables have been forwarded to main workspace.')
end
