function scan = makeScan_manualscan(filePath,baseName)
% Create scan from a serie of recordings from the Iconeus one.
% filePath : str
% baseName : str | cell, identifier for the files to add to the scan.
% File organization : baseName_1_fus2D.source, baseName_2_fus2D.source, ...


if ~exist('baseName','var') || isempty(baseName)
    baseName = 'manScan1';
end
if ischar(baseName)
    baseName = {baseName};
end

for i = 1:length(baseName)
    sliceList = dir(fullfile(filePath, [baseName{i} '_*.source.scan']));
    a = arrayfun(@(x)(regexp(x.name,[baseName{i} '_([0-9]+)_'],'Tokens')),sliceList,'UniformOutput',false);
    [~, sliceOrder ] = sort(cellfun(@str2double,vertcat(a{:})));
    sliceList = sliceList(sliceOrder);
    
    for s = 1:length(sliceList)
        [data, nfo] = loadIconeusData(fullfile(sliceList(i).folder,sliceList(s).name));
        data = mean(squeeze(data),3);
        dat(:,:,s) = permute(data,[2 1]);
    end
    
    scan.(baseName{i}) = dat;

end