 function [sig] = ActivationMap(data,param,signiftype,filtertype,Mask)
    %Check for existing arguments / otherwise initialize // This is so
    %ugly... Need to put in a function or learn how to use varargin?
    
    %set default parameters
    if ~exist('signiftype','var') || isempty('signiftype')
        signiftype = 'pearson';
    end
    if ~exist('filtertype','var') || isempty('filtertype')
        filtertype.Data = '';
        filtertype.SigMap = '';
        filtertype.Map = 'withoutNaN';
    end
    if ~exist('Mask','var') || isempty('Mask')
        Mask = 1;
    end

    if strcmp(filtertype.Data,'3Dfilter')
        s = size(data);
        for ii = 1:s(3)
            for jj = 1:s(4)
                for ll = 1:s(6)
                    data(:,:,ii,jj,:,ll) = imgaussfilt3(squeeze(data(:,:,ii,jj,:,ll)),0.5);
                end
            end
        end
    end
    
    if strcmp(filtertype.Data,'2Dfilter')
        s = size(data);
        for ii = 1:s(3)
            for jj = 1:s(4)
                for kk = 1:s(5)
                    for ll = 1:s(6)
                        data(:,:,ii,jj,kk,ll) = imgaussfilt(squeeze(data(:,:,ii,jj,kk,ll)),0.5);
                    end
                end
            end
        end
    end
    
    % Normalize signal
    % Done outside of this function
    %[SignalPixNorm,data,~] = NormalizeSignal(data,param);
    SignalPixNorm = data;
    
%     sig.SignalPixNorm = nanmean(SignalPixNorm,4);
    sig.SignalPixNorm = nanmedian(SignalPixNorm,4);
    
%     sig.SignalPixMax = mean(sig.SignalPixNorm(:,:,param.PreStimSilence+param.MeanInterval,:,:,:),3);
%     sig.SignalPixMax = max(sig.SignalPixNorm(:,:,param.PreStimSilence+param.MeanInterval,:,:,:),[],3);
    sig.SignalPixMax = max(sig.SignalPixNorm(:,:,param.MeanInterval,:,:,:),[],3);
    sig.SignalPixMedian = median(sig.SignalPixNorm(:,:,param.MeanInterval,:,:,:),3);
    sig.SignalPixMean = mean(sig.SignalPixNorm(:,:,param.MeanInterval,:,:,:),3);
    
    %Activation mask:
    switch signiftype
        case 'zscore'
            if param.PreStimSilence > 1
                std_time = std(sig.SignalPixNorm(:,:,1:round(param.PreStimSilence * param.scale_factor),:,:,:),[], 3);

            else
%                 std_time = std(sig.SignalPixNorm(:,:,:,:,:,:),[], 3);
                outWindow = setdiff(1:size(sig.SignalPixNorm,3),param.MeanInterval);
                std_time = std(sig.SignalPixNorm(:,:,outWindow,:,:,:),[], 3);
            end
            
            %reference place chosen at random here / bad way to do it
            % Unused at the moment
%             tmp = reshape(sig.SignalPixNorm(1:10,1:10,1:param.PreStimSilence,:,:,:),[100*param.PreStimSilence,size(sig.SignalPixNorm,4),size(sig.SignalPixNorm,5),size(sig.SignalPixNorm,6)]);
            %std_spatial = repmat(permute(std(tmp,[], 1),[6 5 1 2 3 4]),[size(sig.SignalPixNorm,1) size(sig.SignalPixNorm,2) 1 1 1 1]);
            std_spatial = -inf;
            
            if ~Mask
                sig.Significance_Mask = double(any((sig.SignalPixMax > param.SpatialThreshold.*std_spatial)...
                    &(sig.SignalPixMax > param.TemporalThreshold.*std_time)...
                    &(sig.SignalPixMax > param.AmpThreshold),6));
            else
                sig.Significance_Mask = double(any((sig.SignalPixMax > param.SpatialThreshold.*std_spatial)...
                    &(sig.SignalPixMax > param.TemporalThreshold.*std_time)...
                    &(sig.SignalPixMax > param.AmpThreshold),6))...
                    .*reshape(param.ManualMask,[size(param.ManualMask,1),size(param.ManualMask,2), 1, 1, size(param.ManualMask,3)]);
            end
        case 'pearson'
            %define the response window...
            % Not sure whot not use the param.MeanInterval parameter ?
            % HDR(1:10) is manually set to 0 in HomeMadeHDR.m...
            s = size(sig.SignalPixNorm);
            s(length(s)+1) = 1;
            if isfield(param, 'HDR')
                HDRestimate = permute(param.HDR,[1 3 2]);
            elseif isfield(param,'MeanInterval') % Quentin - Try to bypass HDR
                HDRestimate = HomeMadeHDR(data,param);
%                 HDRestimate = zeros([1 1 param.nSteps]);
%                 HDRestimate(:,:,param.MeanInterval) = 1;
            else % Default length of 5 bins after initial silence period
                HDRestimate = zeros([1 1 param.nSteps]); HDRestimate(:,:,param.PreStimSilence+1:param.PreStimSilence+5) = 1;
            end
            HDRestimate = repmat(HDRestimate, [s(1) s(2) 1 s(4) s(5) s(6)]);
            
            % Compute correlation
            r = PearsonCorr(sig.SignalPixNorm,HDRestimate,[3 6]);
            r(r==1) = NaN;
            
            % Fisher Transformation for time zscore
            % Makes the pearson coefficients normally distributed
            Z = sqrt(param.nSteps-3)/2*log((1+r)./(1-r));
            
            if ~Mask
                sig.Significance_Mask = double(any((Z > param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6));
            else    
                sig.Significance_Mask = double(any((Z > param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)) ...
                    .*reshape(param.ManualMask,[size(param.ManualMask,1),size(param.ManualMask,2), 1, 1, size(param.ManualMask,3)]);
            end
        case 'pearson2'
            %define the response window...
            % Not sure whot not use the param.MeanInterval parameter ?
            % HDR(1:10) is manually set to 0 in HomeMadeHDR.m...
            % I think the used a step function to approximate the HRD at
            % some point, rather than the actual average of the response
            % time course of responsive voxels. This makes sense if you
            % expect few voxels to respond, and to have a weak estimate of
            % the HDR.
            % Not sure if we should estimate the correlation across the
            % entire time course, or limit it to the user-defined response
            % window.
            
            s = size(data);
            if length(s) == 5, s(6) = 1; end % dirty fix for condition with only 1 stim
%             s(length(s)+1) = 1;
            if isfield(param, 'HDR') % Old version, part of Agnes codes and data structures. I think.
                HDRestimate = permute(param.HDR,[1 3 2]);
            elseif isfield(param,'MeanInterval')
                % Quentin - estimate HDR
                % Use the entire response for now
                HDRestimate = HomeMadeHDR(data,param);
                HDRestimate = permute(HDRestimate,[1 3 2]); % Trick to have 2 first dimensions empty. 
                 
%                 HDRestimate = zeros([1 1 param.nSteps]);
%                 HDRestimate(:,:,param.MeanInterval) = 1;
            else
                % Creates a step function on the response window.
                % (Was implemented before.
                HDRestimate = zeros([1 1 param.nSteps]);
                HDRestimate(:,:,param.MeanInterval) = 1;
            end
            HDRestimate = repmat(HDRestimate, [s(1) s(2) 1 s(4) s(5) s(6)]);
            
            % Compute correlation
            % The dimensions could be adjusted to prevent over selective
            % voxels to be ignored.
            r = PearsonCorr(sig.SignalPixNorm,HDRestimate,[3 4 6]);
            r(r==1) = NaN;
                        
            % Fisher Transformation for time zscore
            % Makes the pearson coefficients normally distributed
            Z = sqrt(param.nSteps-3)/2*log((1+r)./(1-r));
            
            if ~Mask
                sig.Significance_Mask = squeeze(double(any((Z > param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)));
            else
                sig.Significance_Mask = squeeze(double(any((Z > param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)) ...
                    .*reshape(param.ManualMask,[size(param.ManualMask,1),size(param.ManualMask,2), 1, 1, size(param.ManualMask,3)]));
            end
        case 'tuning'
            s = size(SignalPixNorm);
            ANOVASig = NaN(s(1),s(2),1, 1, s(5));
            for pp = 1:s(5)
                disp(['ANOVA on Slice ' num2str(pp)])
                tmp = param.ManualMask(:,:,pp);
                idxmask = find(tmp(:));
                for mm = 1:length(idxmask)
                    [ii,jj] = ind2sub([s(1),s(2)],idxmask(mm));
                    Response = squeeze(nanmean(SignalPixNorm(ii,jj,param.MeanInterval,:,pp,:),3));
                    ANOVASig(ii,jj,pp) = anova1(Response,[],'off');
                end
            end
            
            if ~Mask
                sig.Significance_Mask = double(any((ANOVASig < param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6));
            else
                sig.Significance_Mask = double(any((ANOVASig < param.RegThreshold) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)) ...
                    .*reshape(param.ManualMask,[size(param.ManualMask,1),size(param.ManualMask,2), 1, 1, size(param.ManualMask,3)]);
            end
            
        case 'ttest'
            % basic ttest between pre-stim and post-stim windows
            % cf Provansal et al. 2021 nature scientific reports
            % ttest for all stim, keep if any passes it
            % Not good at all. 
            for sl = 1:size(data,5)
                for st = 1:size(data,6)
                    [hSlice(:,:,st), pSlice(:,:,st)] = ttest(mean(data(:,:,param.baselineBin,:,sl,st),3),...
                        mean(data(:,:,param.MeanInterval,:,sl,st),3),...
                        'Dim',4,...
                        'Alpha',0.05,...
                        'Tail','left');
                end
                hSlice(isnan(hSlice)) = 0;
                h(:,:,1,1,sl) = logical(sum(hSlice,3));
            end
            if ~Mask
                sig.Significance_Mask = squeeze(double(any((h == 1) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)));
            else
                sig.Significance_Mask = squeeze(double(any((h == 1) ...
                    &(sig.SignalPixMax > param.AmpThreshold),6)) ...
                    .*reshape(param.ManualMask,[size(param.ManualMask,1),size(param.ManualMask,2), 1, 1, size(param.ManualMask,3)]));
            end
        otherwise
            error('Unknown significativity option.')
    end
    sig.Significance_Mask = squeeze(sig.Significance_Mask);
    
    %remove roaming pixels
    if ~isempty(strfind(filtertype.Data,'3Dfilter'))
        sig.Significance_Mask = medfilt3(sig.Significance_Mask,[3, 3, 3]);
    else
        for ii=1:length(param.MotorStepList)
            sig.Significance_Mask(:,:,ii) = medfilt2(sig.Significance_Mask(:,:,ii),[3, 3]);
        end
    end
    
    % No smoothing
%     [MaxRespBF, BF] = max(sig.SignalPixMax,[],6);
    % Smooth tuning curves (mov. average, 3 bins)
%     [MaxRespBF, BF] = max(smoothdata(sig.SignalPixMax,6,'movmean',3,'omitnan'),[],6);
    
    % No smoothing
%     [MaxRespBF, BF] = max(sig.SignalPixMean,[],6);
    [MaxRespBF, BF] = max(sig.SignalPixMedian,[],6);
     
    % Smooth tuning curves (mov. average, 3 bins)
%     [MaxRespBF, BF] = max(smoothdata(sig.SignalPixMean,6,'movmean',3,'omitnan'),[],6);

    % Test BF estimation
%     v = mean(SignalPixNorm(:,:,param.MeanInterval,:,:,:),3);
%     for i = 1:size(v,5)
%         vsort = reshape(v(:,:,:,:,i,:),[size(v,1) size(v,2) size(v,4)*size(v,6)]);
%     end

    s = size(sig.SignalPixMax);
    sig.BF = zeros([s(1) s(2) s(5)]);
    myfilter = fspecial('gaussian',[3 3],0.5);
    switch filtertype.Map
        case 'withNaN'            
            for ii=1:s(5)
                sig.BF(:,:,ii) = imfilter(BF(:,:,ii),myfilter, 'replicate');
                sig.MaxRespBF(:,:,ii) = imfilter(MaxRespBF(:,:,ii),myfilter, 'replicate');
            end
        case 'withoutNaN'   
            tmp1 = squeeze(BF);
            tmp1(sig.Significance_Mask == 0) = NaN;
            tmp2 = squeeze(MaxRespBF);
            tmp2(sig.Significance_Mask == 0) = NaN;
            sig.BF = tmp1;
            sig.MaxRespBF = tmp2;
%             for ii=1:s(5)
%                 sig.BF(:,:,ii) = nanconv(tmp1(:,:,ii),myfilter, 'nanout');
%                 sig.MaxRespBF(:,:,ii) = nanconv(tmp2(:,:,ii),myfilter, 'nanout');
%             end
            %sig.MaxRespBF = MaxRespBF;
        case {'nofilter','3Dfilter_median'}
            sig.BF = squeeze(BF);
            sig.MaxRespBF = squeeze(MaxRespBF);
            
        case '3Dfilter'
            sig.BF = imgaussfilt3(squeeze(BF));
            sig.MaxRespBF = imgaussfilt3(squeeze(MaxRespBF));
            
        case '2Dfilter'
            sig.BF = imgaussfilt(squeeze(BF));
            sig.MaxRespBF = imgaussfilt(squeeze(MaxRespBF));
    end
    
    switch param.signiftype
        case {'pearson','zscore','pearson2'}
            sig.Intensity_Mask = (sig.MaxRespBF)/0.1;
        case 'tuning'
            sig.Intensity_Mask = (sig.MaxRespBF)/0.1;
%             sig.Intensity_Mask = -log10(ANOVASig)/5;
    end
%     sig.Intensity_Mask(sig.Intensity_Mask>1) = 1;
%     sig.Intensity_Mask(sig.Intensity_Mask<0) = 0;    
%     sig.Intensity_Mask(isnan(sig.Intensity_Mask)) = 0;
end