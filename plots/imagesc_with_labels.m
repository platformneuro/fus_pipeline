function h = imagesc_with_labels(x,y,im, names, colormap_name, background_color)
% h = imagesc_with_labels(im, names, colormap_name, background_color)
% imagesc_with_labels plots matrix im using imagesc(im).
% A legend is created with names entries matching unique values in im.
% colormap_name : color map used. Default : jet
% background_color : NaN values are transparent, background is set to background_color. Default : 'k'

if ~exist('colormap_name','var') || isempty(colormap_name)
    colormap_name = 'jet';
end
if ~exist('background_color','var') || isempty(background_color)
    background_color = 'k';
end
if ischar(colormap_name)
    C = eval(colormap_name);
else
    C = colormap_name;
end

h = gcf;
imagesc(x,y,im,'AlphaData',~isnan(im))
set(gca,'Color',background_color)
hold on
colormap(C)
L = size(C,1);
colororder(C(round(linspace(1, L, length(names))),:))
plot(NaN(2,length(names)))
legend(names,'Interpreter','None','Color',[1 1 1])
hold off