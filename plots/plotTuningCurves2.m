function plotTuningCurves2(data,map,param,position,area)
    %Local Tuning curve
    b0_tot = data(:,:,:,:,position,:); %data.Resp(:,:,:,:,position,:);
    fin = [];
    PSTH = 2; % Unused plot option at the moment
%     map = data.Anat(:,:,position);
%     h1 = figure;
    while isempty(fin)
        figure;
        imagesc(real(map));
        hold on
        contour(squeeze(param.sigMask(:,:,position)),'color','w');
        hold off
        colormap 'hot(256)'
        switch area
            case 'Region'
                mask = roipoly;                
            case 'Pixel'
                coo = ginput(1);
                coo = floor(coo);
%                 mask = logical(zeros(size(data.Anat(:,:,position))));
                mask = false(size(map));
                mask(coo(2)-1:coo(2)+1,coo(1)-1:coo(1)+1) = 1;
%                 mask(coo(2):coo(2),coo(1):coo(1)) = 1;
        end        
        signal_ROI = b0_tot(repmat(mask, [1 1 param.nSteps param.nTrials length(param.FreqListFinal)]));        
        signal_ROI = reshape(signal_ROI, sum(mask(:)), param.nSteps, param.nTrials, length(param.FreqListFinal));
        signal_ROI = squeeze(mean(signal_ROI,1));        
%         signal_ROI = squeeze(NormalizeSignal(reshape(signal_ROI,[1, 1, param.nSteps, param.nTrials, length(param.FreqListFinal)]),param))*100;

%         Response = squeeze(mean(signal_ROI(param.PreStimSilence+param.MeanInterval,:,:)));
        Response = squeeze(mean(signal_ROI(param.MeanInterval,:,:)));
        Mean = nanmean(Response);
        Std = nanstd(Response);
        figure('units','normalized','position',[ 0.2526    0.6553    0.5307    0.1883]),
        subplot(121)
        errorbar(param.FreqListFinal, Mean,Std./sqrt(sum(~isnan(Response))),'k','LineWidth',2); hold on
        [~,i] = max(Mean);
        scatter(param.FreqListFinal(i),Mean(i),60,'r','LineWidth',2);
        
        if length(param.FreqListFinal) > 1
            xlim([min(param.FreqListFinal) max(param.FreqListFinal)])
        end
%         ylim([0.0 30])
%         set(gca, 'XScale', 'log')
        ylabel('%CBV')
        xlabel('Frequency (Hz)')
        a = anova1(Response,[],'off');
        title(['One-way anova, p = ' num2str(a)]);
       
        clear p groups
        % signif
        if ~isnan(a)
            
            nn = 0;
            for aa = 1:size(Response,2)-1
                for aaa = aa+1:size(Response,2)
                    nn = nn+1;
                    p(nn) = ranksum(Response(:,aa),Response(:,aaa)); groups{nn} = [param.FreqListFinal(aa) param.FreqListFinal(aaa)];
                end
            end
            sigstar(groups(p<0.05),p(p<0.05))
        else
            warning('No non-NaN values in selected data.')
        end
        
        subplot(122)
        mean_signal_ROI = squeeze(nanmean(signal_ROI,2));
        std_signal_ROI = squeeze(nanstd(signal_ROI,[],2));
        t = linspace(0,(size(mean_signal_ROI,1)-1)/param.scale_factor,size(mean_signal_ROI,1));
        hold all;
        colmap = jet(length(param.FreqListFinal));
        for FreqNum = 1:length(param.FreqListFinal)
            %boundedline(1:nSteps, mean_signal_ROI(:,FreqNum),1.96*std_signal_ROI(:,FreqNum)/sqrt(nTrials),'alpha','cmap',colmap(FreqNum,:),'transparency',0.05);
            plot(t,mean_signal_ROI(:,FreqNum),'color',colmap(FreqNum,:),'displayname',[num2str(param.FreqListFinal(FreqNum)) 'Hz'],'LineWidth',2)
        end
        ax = axis;
%         if param.PreStimSilence > 0
%             plot([param.PreStimSilence param.PreStimSilence],[ax(3) ax(4)],'--','Color','k','LineWidth',2)
%         end
        plot([param.PreStimSilence param.PreStimSilence; param.PreStimSilence+param.Duration param.PreStimSilence+param.Duration]',[ax(3) ax(4); ax(3) ax(4)]','--','Color','k','LineWidth',2,'displayname','stimulus end')
        plot([param.MeanInterval(1) param.MeanInterval(1); param.MeanInterval(end) param.MeanInterval(end)]'./param.scale_factor',[ax(3) ax(4); ax(3) ax(4)]',':','Color','k','LineWidth',1,'displayname','analysis window')
%         plot([param.PreStimSilence+param.MeanInterval(end) param.PreStimSilence+param.MeanInterval(end)],[ax(3) ax(4)],':','Color','k','LineWidth',1,'displayname','response')
        legend('show','Location','eastoutside');
        legend boxoff
        
        ylabel('%CBV')
        xlabel('Time (s)')
%         gf = xticks();
%         xticklabels(gf/param.scale_factor)
%         xlim([0 param.nSteps])
%         ylim([-5 40])

        map(mask) = min(map(:));
        
        
        % Overlapping PSTHs
        switch PSTH
            case 1
                % Draw all trials for the stimulus with the highest evoked
                % activity
                figure;
                freq = param.FreqListFinal(i);
                f =  find(param.FreqListFinal == freq);
                plot(t,squeeze(signal_ROI(:,:,f)),'color',[0.6 0.6 0.6])
                hold on
                plot(t,squeeze(mean(signal_ROI(:,:,f),2)),'color','k','linewidth',2)
                ax = axis;
                plot([param.PreStimSilence param.PreStimSilence; param.PreStimSilence+param.Duration param.PreStimSilence+param.Duration]',[ax(3) ax(4); ax(3) ax(4)]','--','Color','k','LineWidth',2,'displayname','stimulus end')
                plot([param.MeanInterval(1) param.MeanInterval(1); param.MeanInterval(end) param.MeanInterval(end)]'./param.scale_factor',[ax(3) ax(4); ax(3) ax(4)]',':','Color','k','LineWidth',1,'displayname','analysis window')
                ylabel('%CBV')
                xlabel('Time (s)')
                title(['stimulus: ' num2str(param.FreqListFinal(i))])
                
            case 2 % Plot all PSTHs
                for ii = 1:length(param.FreqListFinal)
                    figure;
                    freq = param.FreqListFinal(ii);
                    f =  find(param.FreqListFinal == freq);
                    plot(t,squeeze(signal_ROI(:,:,f)),'color',[0.6 0.6 0.6])
                    hold on
                    plot(t,squeeze(mean(signal_ROI(:,:,f),2)),'color','k','linewidth',2)
                    ax = axis;
                    plot([param.PreStimSilence param.PreStimSilence; param.PreStimSilence+param.Duration param.PreStimSilence+param.Duration]',[ax(3) ax(4); ax(3) ax(4)]','--','Color','k','LineWidth',2,'displayname','stimulus end')
                    plot([param.MeanInterval(1) param.MeanInterval(1); param.MeanInterval(end) param.MeanInterval(end)]'./param.scale_factor',[ax(3) ax(4); ax(3) ax(4)]',':','Color','k','LineWidth',1,'displayname','analysis window')
                    ylabel('%CBV')
                    xlabel('Time (s)')
                    title(['stimulus: ' num2str(param.FreqListFinal(ii))])
                end
                
            otherwise
                warning('Unknown option for PSTH plots.')
        end
        fin = input('Continue ?');
    end
end    