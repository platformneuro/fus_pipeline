function [cc_matrix] = compute_stack_autocorrelation(stack,do_plots)
%compute_stack_autocorrelation Summary of this function goes here
%   Detailed explanation goes here

if ndim(stack) ~= 3
    error('stack must be a 3d matrix (x,z,t)')
end
if ~exist('do_plots','var') || isempty(do_plots)
    do_plots = false;
end

N = size(stack,3);
cc_matrix = nan(N,N);
for i = 1:N
    for j = i:N
        cc_matrix(j,i) = corr2(squeeze(stack(:,:,i)),squeeze(stack(:,:,j)));
        cc_matrix(i,j) = cc_matrix(j,i);
    end
end

if do_plots
    figure
    imagesc(cc_matrix)
    colormap(gray)
    colorbar
    xlabel('time (image)')
    yabel('time (image)')
end

end

