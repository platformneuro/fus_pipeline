function plot_tuning_as_matrix(freq,stimNames,tuning,smoothFlag)

if smoothFlag
    tuning = smoothdata(tuning,1,'movmean',3);
end

figure
imagesc(tuning)
set(gca, ...
    'XTick',1:length(freq), ...
    'YTick',1:length(stimNames), ...
    'XTickLabel',freq, ...
    'YTickLabel',stimNames, ...
    'TickDir','out',...
    'clim',[0 max(tuning(:))])
xlabel('F0')
ylabel('stimulus')
colorbar
colormap(parula)