function f = PlotAllTogether(sig,Anat,param,type)
    %close all
    
    switch type
        case 'Overlay'
            s = [size(sig.BF) 1];
            f = figure;
            for position = 1:s(3)
                ax = subplot(ceil(sqrt(s(3))), round(sqrt(s(3))), position);
                i = Anat(:,:,position); i = i-nanmean(i(:))/2; i(i<0) = 0;
                X = repmat(i/max(i(:)), [1 1 3]);
                X(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = 0;
                
                G = sig.BF(param.dv_coo,param.pa_coo,position);
                G(isnan(G)) = 0;
                %G(G<2) = 2; G(G>9) = 9; G = G-1; %to use to rescale the
                %10freq recordings...
                imagesc(G); caxis([0 length(param.FreqListFinal)]);
                colormap jet
                % Now make an RGB image that matches display from IMAGESC:
                
                
                C = colormap;
                
                % Get the figure's colormap.
                L = size(C,1);
                % Scale the matrix to the range of the map.
                if sum(G(:)) > 0
                    G(G>length(param.FreqListFinal)) = length(param.FreqListFinal); % precision problem, sometimes 5.0000001
                    Gs = round(interp1(linspace(0, length(param.FreqListFinal),L),1:L,G));
                else
                    Gs = G;
                end
                H = reshape(C(Gs,:),[size(Gs) 3]);
                X(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = H(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3]));
                I_Mask = sig.Intensity_Mask.*sig.Significance_Mask;
                I_Mask(I_Mask == 0) = 1;
                h = imagesc(X);
%                 set(h, 'AlphaData', I_Mask(param.dv_coo,param.pa_coo,position));               
%                 title(['Slice ' param.MotorStepList(position)])
                axis equal tight
                %set(ax,'HitTest','off')
                %set(gcf,'WindowButtonDownFcn',@CreateNewFig)
                set(gca,'XTick',[],'YTick',[],'ButtonDownFcn',@CreateNewFig)
                
            end
        case 'Separate'
            figAnat = figure;
            figResp = figure;
            hMaxActivity = figure;
            for position = 1:length(param.MotorStepList)
                figure(figAnat);
                ax = subplot(ceil(sqrt(length(param.MotorStepList))), round(sqrt(length(param.MotorStepList))), position);
                imagesc(real(Anat(param.dv_coo,param.pa_coo,position)));
                title(['Slice ' param.MotorStepList(position)])
                axis equal
                colormap hot
                
                figure(figResp);
                ax = subplot(ceil(sqrt(length(param.MotorStepList))), round(sqrt(length(param.MotorStepList))), position);
                h = imagesc(sig.BF(param.dv_coo,param.pa_coo,position));
                caxis([1,length(param.FreqListFinal)])
                title(['Slice ' param.MotorStepList(position)])
%                 set(h, 'AlphaData', sig.Intensity_Mask(param.dv_coo,param.pa_coo,position));
                colormap jet
                axis equal 
                
                figure(hMaxActivity)
                ax = subplot(ceil(sqrt(length(param.MotorStepList))), round(sqrt(length(param.MotorStepList))), position);
                h = imagesc(sig.SignalPixNorm(param.dv_coo,param.pa_coo,position));
%                 caxis([1,length(param.FreqListFinal)])
                title(['Slice ' param.MotorStepList(position)])
%                 set(h, 'AlphaData', sig.Intensity_Mask(param.dv_coo,param.pa_coo,position));
                colormap jet
                axis equal 
                
            end
        case 'Montage'
            % To do : Add ref system to have proper coordinates
            figAnat = figure;
            figResp = figure;
            hMaxActivity = figure;
            
            figure(figAnat)
%             Anat = Anat(param.dv_coo,param.pa_coo,:);
            minVal = min(Anat(:));
            maxVal = max(Anat(:));
            Im = imtile(Anat);
            imagesc(Im)
            set(gca,'clim',[minVal maxVal],'ColorMap',hot)
%             colormap(hot)
            title('Anatomy')
            
            figure(figResp);
%             sig.BF = sig.BF(param.dv_coo,param.pa_coo,:);
            minVal = min(sig.BF(:));
            maxVal = max(sig.BF(:));
            Im = imtile(sig.BF);
            imagesc(Im);
            if (~isnan(minVal) && ~ isnan(maxVal)) && minVal > maxVal
                set(gca,'clim',[minVal maxVal])
            end
            colormap(jet)
            title('Preferred stimulus')
            
            figure(hMaxActivity);
%             activity = squeeze(mean(sig.SignalPixNorm(param.dv_coo,param.pa_coo,:,:,:,:),[3 4 6]));
            activity = squeeze(mean(sig.SignalPixNorm(:,:,param.MeanInterval,:,:,:),[3 4 6]));
            activity(isnan(sig.BF)) = NaN;
            minVal = min(activity(:));
            maxVal = max(activity(:));
            Im = imtile(activity);
            imagesc(Im);
            if (~isnan(minVal) && ~ isnan(maxVal))
                set(gca,'clim',[-max(abs([minVal maxVal])) max(abs([minVal maxVal]))])
            end
            colormap(french);
            colorbar
            title('Average evoked activity')
            
        case 'Montage-Overlay'
            
            deltaCBV_lim = [-0.2 0.2];
            s = [size(sig.BF) 1];
            img = nan([s(1) s(2) 3 s(3)]);
            img_activity = nan([s(1) s(2) 3 s(3)]);
            for position = 1:s(3)
                %             ax = subplot(ceil(sqrt(s(3))), round(sqrt(s(3))), position);
                i = Anat(:,:,position);
                i = i-nanmean(i(:))/2;
                i(i<0) = 0;
                X = repmat(i/max(i(:)), [1 1 3]);
                X(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = 0;
                
                X_activity = repmat(i/max(i(:)), [1 1 3]);
                X_activity(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = 0;
                
                G = sig.BF(param.dv_coo,param.pa_coo,position);
                G(isnan(G)) = 0;
                
                G_activity = squeeze(mean(sig.SignalPixMean(:,:,:,:,position,:),6));
                G_activity(isnan(G)) = deltaCBV_lim(1);
                G_activity(G_activity<deltaCBV_lim(1)) = deltaCBV_lim(1);
                G_activity(G_activity>deltaCBV_lim(2)) = deltaCBV_lim(2);
                %G(G<2) = 2; G(G>9) = 9; G = G-1; %to use to rescale the
                %10freq recordings...
                %             imagesc(G); caxis([0 length(param.FreqListFinal)]);
                % Now make an RGB image that matches display from IMAGESC:
                C = colormap(jet);
                C_activity = colormap(french);
                % Get the figure's colormap.
                L = size(C,1);
                % Scale the matrix to the range of the map.
                if sum(G(:)) > 0
                    G(G>length(param.FreqListFinal)) = length(param.FreqListFinal); % precision problem, sometimes 5.0000001
                    Gs = round(interp1(linspace(0, length(param.FreqListFinal),L),1:L,G));
                    H = reshape(C(Gs,:),[size(Gs) 3]);
                    X(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = H(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3]));
                    
                    %                 G(G>length(param.FreqListFinal)) = length(param.FreqListFinal); % precision problem, sometimes 5.0000001
                    Gs_activity = round(interp1(linspace(deltaCBV_lim(1), deltaCBV_lim(2),L),1:L,G_activity));
                    Gs_activity(isnan(Gs_activity)) = 1;
                    H_activity = reshape(C_activity(Gs_activity,:),[size(Gs_activity) 3]);
                    X_activity(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3])) = H_activity(repmat((squeeze(sig.Significance_Mask(:,:,position)) > 0), [1 1 3]));
                    
                    %             else
                    %                 Gs = G;
                end
                img(:,:,:,position) = X;
                img_activity(:,:,:,position) = X_activity;
                %             I_Mask = sig.Intensity_Mask.*sig.Significance_Mask;
                %             I_Mask(I_Mask == 0) = 1;
            end
            f = figure;
            h = imagesc(imtile(img));
            colormap('jet');
            if isfield(param,'stimList')
                hold on
                colororder(C(round(linspace(1, L, length(param.FreqListFinal))),:))
                plot(nan(2,length(param.FreqListFinal)))
                colororder(jet(length(param.FreqListFinal)))
%                 legend(num2str(param.FreqListFinal),'Interpreter','None')
                legend([ arrayfun(@num2str,param.FreqListFinal,'UniformOutput',false)],'Interpreter','None')
            end
            
            f = figure;
            h = imagesc(imtile(img_activity));
            
            figAnat = figure;
            %             Anat = Anat(param.dv_coo,param.pa_coo,:);
            minVal = min(Anat(:));
            maxVal = max(Anat(:));
            Im = imtile(Anat);
            imagesc(Im)
            set(gca,'clim',[minVal maxVal])
            colormap(hot)
            title('Anatomy')
            %                 set(h, 'AlphaData', I_Mask(param.dv_coo,param.pa_coo,position));
            %                 title(['Slice ' param.MotorStepList(position)])
            %         axis equal tight
            %set(ax,'HitTest','off')
            %set(gcf,'WindowButtonDownFcn',@CreateNewFig)
            %         set(gca,'XTick',[],'YTick',[],'ButtonDownFcn',@CreateNewFig)

        

    end
    
    % Plot average dCBV transient
    % Significant voxels
    s = size(sig.SignalPixNorm);
    x = 0:1/param.scale_factor:((s(3)-1)/param.scale_factor);
    idx = logical(repmat(sig.Significance_Mask,[1 1 1 s(3) s(4) s(6)]));
    idx = permute(idx,[1 2 4 5 3 6]);
    v = nan(size(idx));
    v(idx) = sig.SignalPixNorm(idx);
    a = squeeze(nanmean(v,[1 2 4 5 6]));
    e = squeeze(nanstd(v,[],[1 2 4 5 6]));
    
    figure;
    shadedErrorBar(x,a,e);
    hold on
    line([param.PreStimSilence param.PreStimSilence+param.Duration;param.PreStimSilence param.PreStimSilence+param.Duration],[-0.1 -0.1; 0.15 0.15],'color','k','displayname','stimulus')
    line([param.responseWindow; param.responseWindow],[-0.1 -0.1; 0.15 0.15],'color','k','lineStyle','--','displayname','response window')
    ylabel('%CBV')
    xlabel('time (s)')
    title('dCBV transient - significant voxels')
    
    % All voxels
    a = squeeze(nanmean(sig.SignalPixNorm,[1 2 4 5 6]));
    e = squeeze(nanstd(sig.SignalPixNorm,[],[1 2 4 5 6]));
    figure;
    shadedErrorBar(x,a,e);
    hold on
    line([param.PreStimSilence param.PreStimSilence+param.Duration;param.PreStimSilence param.PreStimSilence+param.Duration],[-0.1 -0.1; 0.15 0.15],'color','k','displayname','stimulus')
    line([param.responseWindow; param.responseWindow],[-0.1 -0.1; 0.15 0.15],'color','k','lineStyle','--','displayname','response window')
    ylabel('%CBV')
    xlabel('time (s)')
    title('dCBV transient - all voxels')

end