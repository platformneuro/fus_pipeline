function map = french
% Blue,White,Red colormap.
% @Yves Boubenec

if nargin < 1
   f = get(groot,'CurrentFigure');
   if isempty(f)
      m = size(get(groot,'DefaultFigureColormap'),1);
   else
      m = size(f.Colormap,1);
   end
end

CM = [ [ linspace(0,1,100)' ; ones(100,1)] ,...
    [ linspace(0,1,100)' ; linspace(1,0,100)' ] ,...
    [ ones(100,1) ; linspace(1,0,100)' ] ];

P = size(CM,1);
map = interp1(1:size(CM,1), CM, linspace(1,P,m), 'linear');