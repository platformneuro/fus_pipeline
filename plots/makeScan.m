function scan = makeScan(filePath,fileList)
% Create scan from the angio3D scan of the Iconeus one

if ~exist('fileList','var') || isempty(fileList)
    fileList = dir(fullfile(filePath,'*_angio3D.source.scan'));
    fileList = {fileList.name}';
end

if ischar(fileList)
    fileList = {fileList};
end

for i = 1:length(fileList)
    scanName = strrep(fileList{i},'_angio3D.source.scan','');
    scanName = strrep(scanName,'-','_');
    [data, nfo] = loadIconeusData(fullfile(filePath,fileList{i}));
%     data = h5read(nfo.Filename,'/Data',start,dim); % Load raw data
    dat = squeeze(data);
    dat = permute(dat,[2 1 3]);
    scan.(scanName) = dat;
%     fileName = fullfile(savePath,'')
%     save('scan','scan')
end