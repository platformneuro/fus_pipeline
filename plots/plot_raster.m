function plot_raster(F0_toplot, stim_toplot, stimList,F0List, unitSpikes)

if ischar(stim_toplot)
    stim_toplot = {stim_toplot};
end

% stims = unique(type);
% Flist = unique(F0);
repeats = unique(unitSpikes(:,5));
repeats = repeats(repeats~=0);
% units = unique(unitSpikes(:,3));
% for each stim
spike_times = cell(length(stim_toplot),1);
for ss = 1:length(stim_toplot)
    spike_times{ss} = cell(length(repeats),length(F0_toplot)); % create an emtpy matrix to hold spiking in each trial
    
    % for each F0
    for ff = 1:length(F0_toplot)
        stimNum = find(strcmp(stimList,stim_toplot{ss}) & (F0List==F0_toplot(ff)));  % unique name for combination of stim type and F0
        if isempty(stimNum) % if this stim type and fo combo wasn't presented
            spike_times{ss}{:,ff} = [];
            continue
        end
        % for each presentation of this stim
        for rr = 1:length(repeats)
            spikeIDXs = unitSpikes(:,4)==stimNum & unitSpikes(:,5)==repeats(rr);
            spike_times{ss}{rr,ff} = unitSpikes(spikeIDXs,2);
        end
    end % ends F0 loop
    
end % end stim loop

% plot
step = 0.5;
for ss = 1:length(stim_toplot)
    figure
    hold on
    yVal = 10;
    yticks = [];
    % for each F0
    for ff = 1:length(F0_toplot)
        % for each presentation of this stim
        for rr = 1:length(repeats)
            yVal = yVal + step;
            if isempty(spike_times{ss}{rr,ff})
                continue;
            end
            y = repmat(yVal,[1 length(spike_times{ss}{rr,ff})]);
            plot(spike_times{ss}{rr,ff},y,'color','k','marker','|','linestyle','none','markerSize',2)
        end
        yticks(ff) = yVal;
        yVal = yVal + step;
    end % ends F0 loop
    hold off
    title(stim_toplot{ss})
    xlabel('time from onset')
    ylabel('F0 x repeats')
    set(gca,'YTick',yticks,'YTickLabel',F0_toplot,'TickDir','out')
end % end stim loop

