function PlotTuningCurve(data,param,position,area)
    %Local Tuning curve
    b0_tot = data.Resp(:,:,:,:,position,:);
    fin = []; PSTH = 1;
    map = data.Anat(:,:,position);
    while isempty(fin)
        f = figure; imagesc(map)
        colormap 'hot(256)'
        switch area
            case 'Region'
                mask = roipoly;                
            case 'Pixel'
                coo = ginput(1);
                coo = floor(coo);
                mask = logical(zeros(size(data.Anat(:,:,position))));
                mask(coo(2)-1:coo(2)+1,coo(1)-1:coo(1)+1) = 1;
%                 mask(coo(2):coo(2),coo(1):coo(1)) = 1;
        end        
        signal_ROI = b0_tot(repmat(mask, [1 1 param.nSteps param.nTrials length(param.FreqListFinal)]));        
        signal_ROI = reshape(signal_ROI, sum(mask(:)), param.nSteps, param.nTrials, length(param.FreqListFinal));
        signal_ROI = squeeze(mean(signal_ROI,1));        
        signal_ROI = squeeze(NormalizeSignal(reshape(signal_ROI,[1, 1, param.nSteps, param.nTrials, length(param.FreqListFinal)]),param))*100;

        Response = squeeze(mean(signal_ROI(param.PreStimSilence+param.MeanInterval,:,:)));
        Mean = nanmean(Response);
        Std = nanstd(Response);
        figure('units','normalized','position',[ 0.2526    0.6553    0.5307    0.1883]),
        subplot(121)
        errorbar(param.FreqListFinal, Mean,Std./sqrt(sum(~isnan(Response))),'k','LineWidth',2); hold on
        [~,i] = max(Mean);
        scatter(param.FreqListFinal(i),Mean(i),60,'r','LineWidth',2);
        xlim([min(param.FreqListFinal) max(param.FreqListFinal)])
        ylim([0.0 30])
        set(gca, 'XScale', 'log')
        ylabel('%CBV')
        xlabel('Frequency (Hz)')
        a = anova1(Response,[],'off')
        title(['One-way anova, p ' num2str(a)]);
       
        % signif
        clear p groups
        nn = 0;
        for aa = 1:size(Response,2)-1
            for aaa = aa+1:size(Response,2)
                nn = nn+1;
                p(nn) = ranksum(Response(:,aa),Response(:,aaa)); groups{nn} = [param.FreqListFinal(aa) param.FreqListFinal(aaa)];
            end
        end
        sigstar(groups(p<0.05),p(p<0.05))
        
        subplot(122)
        mean_signal_ROI = squeeze(nanmean(signal_ROI,2));
        std_signal_ROI = squeeze(nanstd(signal_ROI,[],2));
        hold all;
        colmap = jet(length(param.FreqListFinal));
        for FreqNum = 1:length(param.FreqListFinal)
            %boundedline(1:nSteps, mean_signal_ROI(:,FreqNum),1.96*std_signal_ROI(:,FreqNum)/sqrt(nTrials),'alpha','cmap',colmap(FreqNum,:),'transparency',0.05);
            plot(mean_signal_ROI(:,FreqNum),'color',colmap(FreqNum,:),'displayname',[num2str(param.FreqListFinal(FreqNum)) 'Hz'],'LineWidth',2)
        end
        legend('show'); legend boxoff
        ax = axis;
        plot([param.PreStimSilence param.PreStimSilence],[ax(3) ax(4)],'--','Color','k','LineWidth',2)
        plot([param.PreStimSilence+param.Duration param.PreStimSilence+param.Duration],[ax(3) ax(4)],'--','Color','k','LineWidth',2)
        plot([param.PreStimSilence+param.MeanInterval(1) param.PreStimSilence+param.MeanInterval(1)],[ax(3) ax(4)],':','Color','k','LineWidth',1)
        plot([param.PreStimSilence+param.MeanInterval(end) param.PreStimSilence+param.MeanInterval(end)],[ax(3) ax(4)],':','Color','k','LineWidth',1)
        
        
        ylabel('%CBV')
        xlabel('Time (s)')
        gf = xticks()
        xticklabels(gf/param.scale_factor)
        xlim([0 param.nSteps])
        ylim([-5 40])
        fin = input('Continue ?');
        close(f);
        map(mask) = min(map(:));
    end

    % Overlapping PSTHs
    if PSTH
        % Draw all trials for a single depth/frequency
        figure;
        freq = param.FreqListFinal(i);
        f =  find(param.FreqListFinal == freq);
        for ii=1:param.nTrials
            ax = subplot( 4, ceil(param.nTrials/4), ii);
            hold all
            plot(1:param.nSteps, signal_ROI(:,ii,f))
            line([param.PreStimSilence param.PreStimSilence],get(ax,'Ylim'),'Color','k')
            line([param.PreStimSilence+param.Duration param.PreStimSilence+param.Duration],get(ax,'Ylim'),'Color','k')
%             line([param.PreStimSilence+param.MeanInterval(1) param.PreStimSilence+param.MeanInterval(1)],get(ax,'Ylim'),'Color','g')
%             line([param.PreStimSilence+param.MeanInterval(2) param.PreStimSilence+param.MeanInterval(2)],get(ax,'Ylim'),'Color','g')
%       
        
        end
    end    
end    