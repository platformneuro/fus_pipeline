% THIS PARAMETER FILE IS A TEMPLATE.
% PLEASE MAKE A COPY BEFORE CHANGING.

%% Tonotopy parameters
param.signiftype = 'pearson'; %'pearson' : Threshold on response autocorelation ; 'tuning' : ANOVA on stim and repeats; 'zscore' : Threshold on param.TemporalThreshold x std(baseline)
param.filtertype.Data = ''; % 3Dfilter, 2Dfilter or empty. Smooth the data across slices or by slices.
param.filtertype.SigMap = ''; % Unused at the moment
param.filtertype.Map = 'withoutNaN';
%  'withNaN' : Plots data with values on masked pixels (ignores
%  significance)
% 'withoutNaN' : Plots data with NaNs (applies significance)
% 'nofilter' : Do not apply significance filter
% '3Dfilter' : smooth BFs across slices
% '3Dfilter_median' : Unused at the moment


%% window parameters
param.responseWindow = [5 7]; % analysis window in seconds after trial onset

param.baselineType = 'bin'; % Compute baseline from beginning of recording (pre), end of recording (post), inter-trial silence (bin)
param.baselineBin = [1 2 3 4]; % Used only if param.baselineType = 'bin'

param.PreStimSilence = 4; % Duration of the silence before stim. onset (in second)
param.Duration = 1; % Stimulus duration (in second)

%% Selectivity parameters
% 
% parameters for pearson signif type
param.RegThreshold = -Inf; % Threshold on trial to trial reliability. 1.645 for 5%, 2.33 for 1%, 3.1 for 0.1% (one-tailed test)
param.AmpThreshold = -Inf; % Threshold on minimal d(CBV) value
% param.SR = 1; %FIX THIS, unused at the moment.
% param.Artefact = 1; % Threshold on maximal d(CBV) value. Unused at the moment, can be implemented.

% Parameters for z-score signif type
% param.AmpThreshold is also used in the signif type.
param.TemporalThreshold = 2; % baseline_std x param.TemporalThreshold theshold.
param.SpatialThreshold = 2; % baseline std x param.SpatialThreshold. Unused at the moment

%% Plotting parameters
% Crop region for plots
param.dv_coo = []; %10:50; % X (dorso-ventral) axis limits (in bins). Default : []
param.pa_coo = []; %40:120; % Y (antero-posterior) axis limits (in bins). Default : []