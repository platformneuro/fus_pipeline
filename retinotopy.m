function retinotopy(data_parameters, analysis_parameters)
% Analyze responses to bars and plot retinotopy across recordings


if ~exist('data_parameters','var') || isempty(data_parameters)
    [data_parameters,data_parameters_file_path] = uigetfile('.m','Select data parameter file :');
    if data_parameters==0, return; end
    run(fullfile(data_parameters_file_path,data_parameters));
else
    run(data_parameters);
end

if ~exist('analysis_parameters','var') || isempty(analysis_parameters)
    [analysis_parameters,analysis_parameters_file_path] = uigetfile('.m','Select analysis parameter file :',data_parameters_file_path);
    if analysis_parameters==0, return; end
    run(fullfile(analysis_parameters_file_path,analysis_parameters));
else
    run(analysis_parameters);
end

%% Load data
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : Baseline data (x,y,t)
% data.Anat : Image of slices (x,y,slice)

% [data,param] = InitAndLoad_Old_v2(param);
[data,param] = loadDataQ2(param);


%% Normalize a ativity and plot activation maps
%data2plt = data.Resp_corr(:,:,1:28,:,:,:);
%data2plt = data.Resp(:,:,1:28,:,:,:);

if param.CCAcorr
    data2plt = data.Resp_corr;
    data.Resp = [];
else
    data2plt = data.Resp;
    data.Resp_corr = [];
end

% Remove outliers
% for i = 1:size(data2plt,5)
%     sliceDat = data2plt(:,:,:,:,i,:);
%     thr = prctile(sliceDat(:),[1 99]);
%     sliceDat(sliceDat>thr(2) | sliceDat < thr(1)) = nan;
%     data2plt(:,:,:,:,i,:) = sliceDat;
%     
%     thr = 0.4;
%     sliceDat(sliceDat>thr(2) | sliceDat < thr(1)) = nan;
%     
% end

% Smooth temporal response at the single trial level
if param.temporalSmoothing
    data2plt = smoothdata(data2plt,3,'movmean',3,'omitnan');
end

param.nSteps = size(data2plt,3);
param.nTrials = size(data2plt,4);
if isempty(param.dv_coo)
    param.dv_coo = 1:size(data2plt,1);
end
if isempty(param.pa_coo)
    param.pa_coo = 1:size(data2plt,2);
end

switch [param.baselineType num2str(param.CCAcorr)]
    case 'pre0'
        Base_mean = nanmean(data.baseline_pre,3);
    case 'post0'
        Base_mean = nanmean(data.baseline_post,3);
    case 'pre1'
        Base_mean = nanmean(data.baseline_pre_c,3);
    case 'post1'
        Base_mean = nanmean(data.baseline_post_c,3);
    case {'bin1', 'bin0'}
         % Base_mean = nanmean(data.Base(:,:,1,:),4);
%         Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),[3 4 6]);
%         Base_mean = repmat(Base_mean,[1 1 size(data.Resp,3) size(data.Resp,4) 1 size(data.Resp,6)]);

%         Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),[3 4]);
%         Base_mean = repmat(Base_mean,[1 1 size(data.Resp,3) size(data.Resp,4) 1 1]);
        
        Base_mean = nanmean(data2plt(:,:,param.baselineBin,:,:,:),3);
        Base_mean = repmat(Base_mean,[1 1 size(data.Resp,3) 1 1 1]);
    otherwise
       error('Unknown baseline / data option.')
end

data2plt = (data2plt - Base_mean) ./ Base_mean;


switch param.signiftype % Not sure we need that switch here. I think param.regThreshold is interpreted differently between the cases.
    case {'pearson','zscore','pearson2'}
        param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));
 
    case 'tuning'
%         param.Artefact = 1;
        param.MeanInterval = round((param.responseWindow(1)*param.scale_factor)):round((param.responseWindow(2)*param.scale_factor));
end

stimIdx = [3 1 6 4 2 5];
param.stimListOr = param.stimList;
param.stimList = param.stimList(stimIdx);
data2plt = data2plt(:,:,:,:,:,stimIdx);
[sig] = ActivationMap(data2plt,param,param.signiftype,param.filtertype,1); % ActivationMap(data,param,signiftype,filtertype,Mask)
% 
[sig_h] = ActivationMap(data2plt(:,:,:,:,:,[1 2 3]),param,param.signiftype,param.filtertype,1); % ActivationMap(data,param,signiftype,filtertype,Mask)
[sig_v] = ActivationMap(data2plt(:,:,:,:,:,[4 5 6]),param,param.signiftype,param.filtertype,1); % ActivationMap(data,param,signiftype,filtertype,Mask)

% PlotAllTogether(sig,data.Anat,param,'Overlay');
% PlotAllTogether(sig,data.Anat,param,'Montage');


PlotAllTogether(sig,data.Anat,param,'Montage-Overlay');

% figure;
% im = sig.BF;
% im(isnan(im)) = 0;
% imagesc(imtile(im))
% title("prefered stimulus")
% colormap([[1 1 1];jet])

C = 'parula';
background = 'k';

im = imtile(sig_h.BF);
figure;
imagesc_with_labels(1:size(im,1),1:size(im,2),im, param.stimList([1 2 3]), C, background);
title("prefered elevation")

figure;
im = imtile(sig_v.BF);
imagesc_with_labels(1:size(im,1),1:size(im,2),im, param.stimList([4 5 6]), C, background);
title("prefered azimuth")


%% Plot activity for each stimulus
plot_activity = true;

if plot_activity
    dat = squeeze(nanmean(sig.SignalPixMean,3));
    maxVal = max(abs(dat(:)));
    dCBV_lim = [-0.2 0.2];
    for s = 1:size(sig.SignalPixNorm,6)
        figure;
        im = imtile(squeeze(dat(:,:,:,s)));
%         minVal = min(im(:));
%         slice_msk = ~isnan(im);
        sig_msk(:,:,s) = imtile(logical(sig.Significance_Mask));
%         im(im < dCBV_lim(1)) = dCBV_lim(1);
%         im(im > dCBV_lim(2)) = dCBV_lim(2);
%         im = im - dCBV_lim(1);
%         im = im ./ (dCBV_lim(2)-dCBV_lim(1));
    %     im(~sig_msk) = NaN;
%         im = uint8(im*255);
%         im = ind2rgb(im, french);
%         im = insertText(im,[1 1],param.stimList{s},'BoxOpacity',1,'BoxColor','white','TextColor','black');    
%         allIm(:,:,:,s) = im;
        imagesc(imtile(im));
        set(gca,'clim',[-0.2 0.2])
        colormap(french)
        title(param.stimList{s},'interpreter','none')
    end
%     figure
%     imagesc(imtile(allIm));
%     hold on
%     contour(imtile(sig_msk),[0.5 0.5],'k')
%     title(sprintf('%s - evoked dCBV per stimulus',param.Animal))
end
%% 3d plot
plot_3d_view = true;
if plot_3d_view
    dCBV_threshold = 0.15;
    % All stim
    dat = squeeze(nanmean(sig.SignalPixMean,3));
    x = (1:size(dat,2))*0.1;
    y = (1:size(dat,1))*0.1;
    z = (1:size(dat,3))*0.3;
    
    h_iso = figure;
    hold on;
    % See anatomy as contours
    % contourslice(x,y,z,Anat,[],[],z,1)
    % See anatomy as patch of isosurface
    Anats = smooth3(data.Anat);
    hiso(1) = patch(isosurface(x,y,z,Anats,5),...
        'FaceColor',[0.7 0.7 0.7],...
        'FaceAlpha',0.4,...
        'EdgeColor','none');
    C = jet;
    L = size(jet,1);
    c_list = C(round(linspace(1,L,size(sig.SignalPixNorm,6))),:);

    labels = zeros(size(dat,[1 2 3]));
    for s = 1:size(sig.SignalPixNorm,6)
        %     figure;
        im = squeeze(dat(:,:,:,s));
        %     im(im<dCBV_threshold) = 0;
        im = smooth3(im,'box',[3 3 3]);
        %     contourslice(x,y,z,im,[],[],z,3)
        %     contourslice(im,[],[],1:size(im,3),3)
        
        figure(h_iso)
        hiso(s+1) = patch(isosurface(x,y,z,im,dCBV_threshold),...
            'FaceColor',c_list(s,:),...
            'EdgeColor','none',...
            'FaceAlpha',0.6);
        
        labels(im > dCBV_threshold) = s;
    end
    legend(['Anatomy'; param.stimList],'Interpreter','None')
    grid on
    set(gca,'Ydir','reverse')
    xlabel('medio-lateral (mm)')
    ylabel('Depth (mm)')
    zlabel('Antero-posterior (mm)')
    title('All bars')
    view(180,0)
    
    % horizontal bars
    dat = squeeze(nanmean(sig_h.SignalPixMean,3));
    
    h_iso_h = figure;
    hold on;
    % See anatomy as contours
    % contourslice(x,y,z,Anat,[],[],z,1)
    % See anatomy as patch of isosurface
    hiso_h(1) = patch(isosurface(x,y,z,Anats,5),...
        'FaceColor',[0.7 0.7 0.7],...
        'FaceAlpha',0.4,...
        'EdgeColor','none');
    C = jet;
    L = size(jet,1);
    c_list = C(round(linspace(1,L,size(sig_h.SignalPixNorm,6))),:);

    labels = zeros(size(dat,[1 2 3]));
    for s = 1:size(sig_h.SignalPixNorm,6)
        %     figure;
        im = squeeze(dat(:,:,:,s));
        %     im(im<dCBV_threshold) = 0;
        im = smooth3(im,'box',[3 3 3]);
        %     contourslice(x,y,z,im,[],[],z,3)
        %     contourslice(im,[],[],1:size(im,3),3)
        
        figure(h_iso_h)
        hiso_h(s+1) = patch(isosurface(x,y,z,im,dCBV_threshold),...
            'FaceColor',c_list(s,:),...
            'EdgeColor','none',...
            'FaceAlpha',0.6);
        
        labels(im > dCBV_threshold) = s;
    end
    legend(['Anatomy'; param.stimList([1 2 3])],'Interpreter','None')
    grid on
    set(gca,'Ydir','reverse')
    xlabel('medio-lateral (mm)')
    ylabel('Depth (mm)')
    zlabel('Antero-posterior (mm)')
    title('Horizontal bars')
    view(180,0)
    
    % Vertical bars
    dat = squeeze(nanmean(sig_v.SignalPixMean,3));
    
    h_iso_v = figure;
    hold on;
    % See anatomy as contours
    % contourslice(x,y,z,Anat,[],[],z,1)
    % See anatomy as patch of isosurface
    hiso_v(1) = patch(isosurface(x,y,z,Anats,5),...
        'FaceColor',[0.7 0.7 0.7],...
        'FaceAlpha',0.4,...
        'EdgeColor','none');
    C = jet;
    L = size(jet,1);
    c_list = C(round(linspace(1,L,size(sig_h.SignalPixNorm,6))),:);

    labels = zeros(size(dat,[1 2 3]));
    for s = 1:size(sig_v.SignalPixNorm,6)
        %     figure;
        im = squeeze(dat(:,:,:,s));
        %     im(im<dCBV_threshold) = 0;
        im = smooth3(im,'box',[3 3 3]);
        %     contourslice(x,y,z,im,[],[],z,3)
        %     contourslice(im,[],[],1:size(im,3),3)
        
        figure(h_iso_v)
        hiso_v(s+1) = patch(isosurface(x,y,z,im,dCBV_threshold),...
            'FaceColor',c_list(s,:),...
            'EdgeColor','none',...
            'FaceAlpha',0.6);
        
        labels(im > dCBV_threshold) = s;
    end
    legend(['Anatomy'; param.stimList([4 5 6])],'Interpreter','None')
    grid on
    set(gca,'Ydir','reverse')
    xlabel('medio-lateral (mm)')
    ylabel('Depth (mm)')
    zlabel('Antero-posterior (mm)')
    title('Vertical bars')
    view(180,0)
end
% L = squeeze(sig.SignalPixMean(:,:,:,:,:,4));
% volumeViewer(L)

%% Plot activity per stim overlayed on anat
max_activity = 0.2;
min_activity = 0.07;

figure;
im1 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,1)));
im2 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,2)));
im3 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,3)));

msk = imtile(sig_h.Significance_Mask);
im1(~msk) = NaN; im2(~msk) = NaN; im3(~msk) = NaN;
im1(im1 > max_activity) = max_activity; im2(im2 > max_activity) = max_activity; im3(im3 > max_activity) = max_activity;
im1(im1 < min_activity) = NaN; im2(im2 < min_activity) = NaN; im3(im3 < min_activity) = NaN;

hFig = imfuseQ(imtile(data.Anat),im1,im2,im3);
title('horizontal bars - R Down - G Center - B Up')

figure;
im1 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,4)));
im2 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,5)));
im3 = imtile(squeeze(sig.SignalPixMean(:,:,:,:,:,6)));

msk = imtile(sig_v.Significance_Mask);
im1(~msk) = NaN; im2(~msk) = NaN; im3(~msk) = NaN;
im1(im1 > max_activity) = max_activity; im2(im2 > max_activity) = max_activity; im3(im3 > max_activity) = max_activity;
im1(im1 < min_activity) = NaN; im2(im2 < min_activity) = NaN; im3(im3 < min_activity) = NaN;

hFig = imfuseQ(imtile(data.Anat),im1,im2,im3);
title('vertical bars - R left - G Center - B right')

%% plot Selectivity across layers

hAnat = figure;
sliceNB = size(data2plt,5);
timeperiod = floor(param.responseWindow(1)*param.scale_factor):ceil(param.responseWindow(2)*param.scale_factor);
sliceList = input('index of slices to analyze : ');
pltVx = false;
use_existing_coordinates = false;
% normalize_ipsi_contra = true;

if use_existing_coordinates
    load('lineCoordinates_volume')
end

while ~isempty(sliceList) && sliceList(1) ~= 0
%     sliceN = sliceList(1);
    goOn = [];

    while isempty(goOn)
        clear MIt tuningLine voxel_values cx_or cy_or
        for i = 1:length(sliceList)
            clear vxIdx MItl t1 t2 eyeAct
            
            figure(hAnat)
            hold off
            I = squeeze(data.Anat(:,:,sliceList(i)));
            imagesc(I)
            %     set(gca,'YDir','norm')
            colormap(hot)
            hold on
            
        if ~use_existing_coordinates
            [cx_or{i},cy_or{i},~] = improfile; % this gets x/y coordinates of the voxels
            cx = round(cx_or{i}); cy = round(cy_or{i});
            for vxNum = 2:length(cy)
                
%                 deg(vxNum) = (cx_or{i}(vxNum)-cx_or{i}(vxNum-1))/(cy_or{i}(vxNum)-cy_or{i}(vxNum-1));
%                 if isinf(deg(vxNum))
%                     deg(vxNum) = 1;
%                 end
%                 if isnan(deg(vxNum))
%                     deg(vxNum) = 0;
%                 end
                mskVoxel = zeros(size(data2plt,1:2));
                mskVoxel(cy(vxNum),cx(vxNum)) = 2;
                mskVoxel = imdilate(mskVoxel,strel("rectangle",[10 5]));
                mskVoxel(cy(vxNum),cx(vxNum)) = 3;
                [row{i}{vxNum},col{i}{vxNum}] = ind2sub(size(mskVoxel),find(logical(mskVoxel)));
                
                if pltVx
                    mskVoxel(mskVoxel==0) = I(mskVoxel==0);
                    figure(hAnat)
                    imagesc(mskVoxel)
                    pause(0.05)
                end
            end
        end

            for vxNum = 1:length(row{i})
                for st = 1:size(data2plt,6)
                    tuningLine{i}(vxNum,st) = nanmean(data2plt(row{i}{vxNum},col{i}{vxNum},timeperiod,:,sliceList(i),st),1:6);
                end
            end
            
%             if normalize_ipsi_contra
%                 tuningLine{i}(:,:) = tuningLine{i}(:,:) - nanmean(tuningLine{i}(:,:),1);
%             end
            
            [MIt_values{i}, MIt{i}] = max(tuningLine{i},[],2);
            
            figure;
            subplot(2,1,1);
            plot(tuningLine{i}(:,:)); axis('tight'); ylabel('dCBV')
            grid on
            legend(param.stimList,'interpreter','none');
            subplot(2,1,2);
            plot(MIt{i});
            axis('tight');
            grid on
%             ylim([-.1 .1])
            hold on
            yline(0,'HandleVisibility','off')
            xlabel('distance over cx (in vx)');
            ylabel('Prefered stimulus')
            set(gca,'YTick',1:size(data2plt,6),'YTickLabel',param.stimList)
        end
%         figure
%         hold on
%         cellfun(@(x,y)(plot3(1:length(x),x,ones(1,length(x))*y)),MIt,num2cell(1:length(sliceList)))
%         title('Selectivity')
%         xlabel('distance over cx (in vx)'); ylabel('MI(contra over ipsi)')
%         grid on
%         legend([param.Filter {'anatomy'}])
        
        
        maxL = max(cellfun(@length,MIt));
        for i = 1:length(MIt), starts(i) = cx_or{i}(1); end
        starts = round(starts);
        starts = abs(starts - max(starts))+1;
        maxL = max(cellfun(@(x,y)(length(x)+y),MIt,num2cell(starts)));
%         MIt_padd = cell2mat(cellfun(@(x)(padarray(x,maxL-length(x),nan,'post')),MIt,'UniformOutput', false));
        MIt_padd = nan(maxL,length(starts));
        for i =1:length(starts)
            MIt_padd(starts(i):(starts(i)+length(MIt{i})-1),i) = MIt{i};
        end
        figure;
%         surf(0:0.3:(size(MIt_padd,2)-1)*0.3,0:0.1:(size(MIt_padd,1)-1)*0.1,MIt_padd,'edgecolor','none','FaceColor','interp')
%         surf(0:0.3:(size(MIt_padd,2)-1)*0.3,0:0.1:(size(MIt_padd,1)-1)*0.1,MIt_padd,'edgecolor','none')
        imagesc_with_labels(0:0.1:(size(MIt_padd,1)-1)*0.1,0:0.3:(size(MIt_padd,2)-1)*0.3,MIt_padd',param.stimList,lines(6),'w');
% 
%         colormap(lines(6))
%         colorbar
        xlabel('medio-lateral')
        ylabel('antero-posterior')
%         zlabel('prefered stimulus')
        title('retinotopy')
        set(gca,'Ydir','norm')
%         set(gca,'clim',[-max(abs(get(gca,'clim'))) max(abs(get(gca,'clim')))])
%         axis equal
%         view(-90,90)
        
        
        if pltVx
            % Add color patch
            hold on
            patch([1:length(cy_or{i}) nan],[zeros(1,length(cy_or{i})) nan],[zeros(1,length(cy_or{i})) nan],[1:length(cy_or{i}) nan],'edgecolor','interp');
            colormap(jet)
            
            % Plot anatomy with patch
            I2 = zeros(size(I,1),size(I,2),3);
            I2(:,:,1) = (I./max(I(:)));
            figure;
            imagesc(I2);
            hold on
            patch([cx_or{i}' nan],[cy_or{i}' nan],[cy_or{i}' nan],[1:length(cy_or{i}) nan],'edgecolor','interp');
            colormap(jet)
        end
            
        goOn = input('Continue : ');
    end
    sliceList = input('index of slices to analyze : ');
end
% save('lineCoordinates_volume','row','col','MIt','tuningLine','cx_or','cy_or','allAnat')


%% test grid
% dat = squeeze(nanmean(sig.SignalPixMean,3));
pairs = [1 4; 1 5; 1 6; 2 4; 2 5; 2 6; 3 4; 3 5; 3 6];

pooledData = nan([size(data2plt,1:3) size(data2plt,4)*size(pairs,2) size(data2plt,5) size(pairs,1)]);

for s = 1:size(pairs,1)
    stimNames{s} = sprintf('%s-%s',param.stimList{pairs(s,1)}, param.stimList{pairs(s,2)});
    pooledData(:,:,:,:,:,s) = cat(4,data2plt(:,:,:,:,:,pairs(s,1)),data2plt(:,:,:,:,:,pairs(s,2)));
end
% clear('data2plt')
param_pooled = param;
[sig_pool] = ActivationMap(pooledData,param_pooled,param.signiftype,param.filtertype,1); % ActivationMap(data,param,signiftype,filtertype,Mask)
sig_pool.BF(isnan(sig_pool.BF)) = 0;

figure;
imagesc_with_labels(imtile(sig_pool.BF), stimNames, 'jet', background);
title('interpolated localizations')

%% Slice explorer
param.sigMask = isnan(sig.BF);

if sum(param.sigMask(:))==0
    param.sigMask = sig.BF == 1;
end

maskedData = data2plt;
maskedData(permute(repmat(param.sigMask,[1 1 1 size(data2plt,[3 4 6])]),[1 2 4 5 3 6])) = NaN;
while true
    slice = input('Enter slice number (0 to exit) : ');
    if slice == 0 || isempty(slice), break; end
    
    r = input('Region [1] or Pixel [2] selection mode :','s');
    switch r
        case '1'
            selectionMode = 'Region';
        case {'2',''}
            selectionMode = 'Pixel';
    end
    plotTuning(maskedData,squeeze(data.Anat(:,:,slice)),param,slice,selectionMode)
%     for i = 1:size(maskedData,5)
%         plotTuningCurves2(maskedData,squeeze(data.Anat(:,:,i)),param,i,selectionMode)
%         plotTuningCurves_throughSlice(maskedData,data.Anat,param,1:size(maskedData,5),selectionMode);
%     end
end


