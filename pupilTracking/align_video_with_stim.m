clear
close all

rootDir = "D:\fUS\ocdom\Pavin\video\Pavin_2023-03-13\";
condition = '01';
slice = 'a';
stim = 'oriFlashStim';

videoPath = dir(fullfile(rootDir,['*_' stim '_' condition slice '_Basler*']));
stimPath = dir(fullfile(strrep(rootDir,'video','MatFiles'),['*_' stim '_*' condition slice '_*']));
trigPath = dir(fullfile(strrep(rootDir,'video','TrigFiles'),['*_' stim '_*' condition slice '_*']));

% fUSPath = "D:\fUS\ocdom\Pavin\Iconeus\FilteredData_20230313_co\oriFlashStim_01a_fus2D.source.scan";

% ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles', stimDataPathSubFolderName,[ '*_' param.exp.Type '_' Ref '_stim_log.csv']));
% if isempty(ReferenceName)
%     ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles', stimDataPathSubFolderName,[ '*_' param.exp.Type '_*_' Ref '_stim_log.csv']));
% end
%

%Get video object
vid = VideoReader(fullfile(videoPath.folder,videoPath.name));

param.fUS_ITI = 400;
param.baphy_ITI = 1050;
param.strict = 1; % be strict when cutting into trials or not
param.offset = 2; % Shift the trial onset relative to the baphy trigger.
param.trigStr = 'On-';
param.trigStr2 = 'Off-';
param.stimDur = 1;
param.exp.trial_offset = param.offset;

% Cut_into_trials
% Create a fUS like object with video frames per trial and stim
% Create a video object with visual stimuli per frames.
[videoBytrial, videoFrames] = CutIntoTrials(vid, fullfile(trigPath.folder,trigPath.name), param, 1);

% Cut into stimulus and repeats
[videoByStim, videoFramesLabeled, param] = cutIntoStimAndRepeats(videoBytrial,videoFrames,fullfile(stimPath.folder,stimPath.name),param);

videoSavePath = fullfile(rootDir,sprintf('chuncked_video_%s_%s%s',stim,condition,slice));
if ~exist(videoSavePath,'dir')
    mkdir(videoSavePath)
end

save(fullfile(videoSavePath,sprintf('videoFramesLabeled_%s_%s%s.mat',stim,condition,slice)),'videoFramesLabeled')

% Generate chuncked video files
generateVideoChunck(vid,videoByStim,param,videoSavePath)



%% functions
function [vidbytrial, videoFrames] = CutIntoTrials(vid, trig_file, param, plt)
% CutIntoTrials reshapes the fUS raw signal into trials aligned with Baphy trials. 
%
%    [fUSbytrial, baseline_before, baseline_after] = CutIntoTrials(fUS_file, trig_file, param, plt)
%
% Outputs :
%    fUSbytrial : matrix of fUS signal (x,y,time,trial)
%    baseline_before : fUS signal recorded before the first Baphy trial
%    baseline_after : fUS signal recorded after the last Baphy trial
%
% Inputs :
%    fUS_file : path to raw fUS data or matrix of raw fUS data
%    trig_file : path to Baphy triggers csv file
%    param : optional parameters :
%        param.fUS_ITI : fUS inter trial intervals (400 msec);  
%        param.baphy_ITI = Baphy inter trial intervals (1050 msec);
%        param.strict : be strict when cutting into trials or not (0/1)
%        param.offset : Shifts the trial onset relative to the baphy trigger (0  msec).
%    plt : plot triggers (0/1)

if nargin < 4
    plt = 1;
end
if nargin < 3
    % time between 2 fUS trigs (inter-trig interval) in ms
    param.fUS_ITI = 400;  
    param.baphy_ITI = 1050;
    param.strict = 1; % be strict when cutting into trials or not
    param.offset = 2; % Shift the trial onset relative to the baphy trigger.
    param.trigStr = 'On-';
    param.trigStr2 = 'Off-';
end

% Load trigger information file
file = readtable(trig_file,'ReadVariableNames',false);

% fUS trigs
trig_info = table2cell(file(:,2));
f_trigs = trig_info(cellfun(@(x) isequal(x,1),strfind(trig_info,'F-')));
f_trigs = cell2mat(cellfun(@(x) str2double(x(3:end)),f_trigs,'UniformOutput',false));

% baphy trigs
b_trigs = trig_info(cellfun(@(x) isequal(x,1),strfind(trig_info,param.trigStr)));
b_trigs = cell2mat(cellfun(@(x) str2double(x(length(param.trigStr)+1:end)),b_trigs,'UniformOutput',false));

% video trigs
v_trigs = trig_info(cellfun(@(x) isequal(x,1),strfind(trig_info,'C-')));
v_trigs = cell2mat(cellfun(@(x) str2double(x(3:end)),v_trigs,'UniformOutput',false));

% Adjust case where the first fUS trig did not happen at time 0
% This is setup-dependant
fUS_first_trigger_time = f_trigs(1);
if fUS_first_trigger_time ~= 0
    f_trigs = f_trigs - fUS_first_trigger_time;
    b_trigs = b_trigs - fUS_first_trigger_time;
    v_trigs = v_trigs - fUS_first_trigger_time;
end

% Detect new trial when more than 1 sec between trigs
BetweenTrialsBreak = find(diff(b_trigs) > param.baphy_ITI);
NewTrialsBaphy = [b_trigs(1); b_trigs(BetweenTrialsBreak+1)];
n_trials = length(NewTrialsBaphy);
lag = nan(n_trials,1);
NewTrialsfUS_idx = nan(n_trials,1);
NewTrialsVid_idx = nan(n_trials,1);
for baphy_trial = 1:n_trials
    
    % Get fUS trial index matching stimulus index
    lags = NewTrialsBaphy(baphy_trial) - f_trigs;
    lags(lags < 0) = []; % keep only fUS trigs that happened BEFORE baphy
    
%     [lag(baphy_trial),NewTrialsfUS_idx(baphy_trial)] = min(lags);
    if isempty(lags) && baphy_trial == 1
        lag(baphy_trial) = 0;
        NewTrialsfUS_idx(baphy_trial) = 1;
    else
        [lag(baphy_trial),NewTrialsfUS_idx(baphy_trial)] = min(lags);
    end
    
    % Get camera frame index matching stimulus index
    lags = NewTrialsBaphy(baphy_trial) - v_trigs;
    lags(lags < 0) = []; % keep only fUS trigs that happened BEFORE baphy
    
%     [lag(baphy_trial),NewTrialsfUS_idx(baphy_trial)] = min(lags);
    if isempty(lags) && baphy_trial == 1
        lag_v(baphy_trial) = 0;
        NewTrialsVid_idx(baphy_trial) = 1;
    else
        [lag_v(baphy_trial),NewTrialsVid_idx(baphy_trial)] = min(lags);
    end
    
end

NewTrialsfUS = f_trigs(NewTrialsfUS_idx);
% Trig idx by assuming that they happen every 400ms, to compensate for
% missing fUS trigs
% NewTrialsfUS_corrIdx = round((NewTrialsfUS+param.fUS_ITI)/param.fUS_ITI);

NewTrialsfUS_corrIdx = round(NewTrialsfUS/param.fUS_ITI);

%% Load fUS data

% if ischar(fUS_file)
%     nfo = h5info(fUS_file); % Get HDF5 info, including data set names
%     data = squeeze(h5read(nfo.Filename,'/Data')); % Load raw data
% else
%     data = fUS_file;
% end

if plt
%     t = 0:param.fUS_ITI:(size(data,3)-1)*param.fUS_ITI;
    h = figure;
    hold on
%     dataPlt = squeeze(mean(data-mean(data(:)),[1 2]));
%     dataPlt = (dataPlt ./ max(dataPlt)) * 0.5;
%     plot(t, dataPlt+1.4)
    scatter(f_trigs/1000,ones(length(f_trigs),1),'+');
    scatter(b_trigs/1000,1.2*ones(length(b_trigs),1),'+');
    scatter(v_trigs/1000,0.75*ones(length(v_trigs),1),'+');
    missing_trigs = find(diff(f_trigs)>param.fUS_ITI+50);
    scatter((f_trigs(missing_trigs)+param.fUS_ITI)/1000,0.9*ones(length(missing_trigs),1),'g+');
    scatter(NewTrialsBaphy/1000,1.4*ones(n_trials,1),'o');
    scatter(NewTrialsfUS/1000,1.4*ones(n_trials,1),'o');
    ylim([0 3])
%     legend('CBV','fUS','Baphy','missing fUS trigs','Baphy trials start','fUS trials starts')
    legend('fUS','Baphy','Camera','missing fUS trigs','Baphy trials start','fUS trials starts')
    xlabel('time (s)')
    title('triggers')
end

%% Segment video by trial

if param.strict
    min_ITI = min(diff(NewTrialsfUS));
    length_trial_fUS = floor((min_ITI/param.fUS_ITI));
    length_trial_vid = floor(min_ITI*vid.FrameRate/1000)-1;
else
    max_ITI = max(diff(NewTrialsfUS));
    length_trial_fUS = floor((max_ITI/param.fUS_ITI));
    length_trial_vid = floor(max_ITI*vid.FrameRate/1000)-1;
end
% fUSbytrial = nan(size(data,2),size(data,1),length_trial_fUS,n_trials);
vidbytrial = nan(length_trial_vid,n_trials);

videoFrames = zeros(1,vid.NumFrames);
stim_duration_video_bin = round(param.stimDur * vid.FrameRate);
for trial = 1:n_trials
    % Cut fUS signal
%     if trial == n_trials
%         trial_ITI =  size(data,3) - NewTrialsfUS_corrIdx(trial) - 1;
%     else
%         trial_ITI = (NewTrialsfUS_corrIdx(trial+1)- NewTrialsfUS_corrIdx(trial))-1;
%     end
% %     interval = NewTrialsfUS_corrIdx(trial)+(1:min(trial_ITI,length_trial_fUS));
%     interval = (NewTrialsfUS_corrIdx(trial)+(1:min(trial_ITI,length_trial_fUS))) - round(param.offset/(param.fUS_ITI/1000));
%     if interval(end) > size(data,3)
%         break
%     elseif interval(1) < 1
%         i = find(interval>=0,1,'first');
%         interval = [(interval(end)+1):(interval(end)+i) interval(i+1:end)];
%         warning('Offset value is out of range for trial %i. The begining of the following trial will be used.',trial)
%     end
%     fUSbytrial(:,:,1:min(trial_ITI,length_trial_fUS),trial) = permute(data(:,:,interval),[2 1 3]);
    % --- Debug plots ---
%     if trial == 1, h = figure; else, figure(h); end
%     nanmat = isnan(squeeze(fUSbytrial(1,1,:,:)));
%     imagesc(nanmat)
%     pause(0.2)
    % --- /Debug plots ---
    
    % Cut video frame times
    if trial == n_trials
        trial_ITI =  length(v_trigs) - NewTrialsfUS_corrIdx(trial)-1;
    else
        trial_ITI = (NewTrialsVid_idx(trial+1)- NewTrialsVid_idx(trial))-1;
    end
%     interval = NewTrialsfUS_corrIdx(trial)+(1:min(trial_ITI,length_trial_fUS));
    interval = (NewTrialsVid_idx(trial)+(1:min(trial_ITI,length_trial_vid))) - round(param.offset*vid.FrameRate);
    if interval(end) > length(v_trigs)
        break
    elseif interval(1) < 1
        i = find(interval>=0,1,'first');
        interval = [(interval(end)+1):(interval(end)+i) interval(i+1:end)];
        warning('Offset value is out of range for trial %i. The begining of the following trial will be used.',trial)
    end
    % interval in time relative to fUS first trigger
    interval = v_trigs(interval);
    % interval in time relative to video start time
    interval = interval + fUS_first_trigger_time;
    % Interval in video frame index
    interval = floor((interval/1000) * vid.FrameRate);
    
    vidbytrial(1:min(trial_ITI,length_trial_vid),trial) = interval;
    videoFrames(interval(1):(interval(1) + stim_duration_video_bin)) = 1;
end


% % keep time before first trial as baseline, only if lasts more than 1Os
% if NewTrialsfUS_corrIdx(1) > 10000/param.fUS_ITI
%     baseline_before = permute(data(:,:,1:NewTrialsfUS_corrIdx(1)),[2 1 3]);
% else 
%     baseline_before = [];
% end
% 
% % keep time after last trial as baseline (5 s after last trig), only if lasts more than 1Os
% LastBaphyTrig = ceil((b_trigs(end)+5000)/param.fUS_ITI);
% if (size(data,3) - LastBaphyTrig) > 10000/param.fUS_ITI
%     baseline_after = permute(data(:,:,LastBaphyTrig:end),[2 1 3]);
% else
%     baseline_after = [];
% end
end

function generateVideoChunck(vid,videoByStim,param,videoSavePath)
for s = 1:size(videoByStim,2)
    vid_w = VideoWriter(fullfile(videoSavePath,[param.stims.stimList{s}]));
    vid_w.FrameRate = vid.FrameRate;
    open(vid_w);
    for r = 1:param.nRep
        v = vid.read(videoByStim([1 size(videoByStim,1)],s,r));
        % Add visual for stim
        v(260:300,360:400,:,round(param.offset*vid.FrameRate):(round(param.offset*vid.FrameRate) + round(param.stimDur*vid.FrameRate))) = 255;
        writeVideo(vid_w,v);
    end
    close(vid_w);
end
end

function [videoByStim, videoFramesLabeled, param] = cutIntoStimAndRepeats(videoBytrial,videoFrames,stimPath,param)

% EXPT = readtable(fullfile(ReferenceName.folder,ReferenceName.name),"FileType" ,'Text');
EXPT = readtable(stimPath);
if ismember('stimulus',EXPT.Properties.VariableNames) % general case
    stims.stimName = EXPT.stimulus;
elseif ismember('shape',EXPT.Properties.VariableNames)
    stims.stimName = EXPT.shape;
elseif ismember('orientation',EXPT.Properties.VariableNames)
    stims.stimName = EXPT.orientation;
else
    error('Could not read stimulus type from stimulus log.')
end
if ~iscell(stims.stimName)
    % Save stim names as strings for consistency
    stims.stimName = arrayfun(@(x)(num2str(x)),stims.stimName,'UniformOutput',false);
end
nTrial = length(stims.stimName);

% Specific case when baphy and fUS don't have same nb of trials
if nTrial ~= size(videoBytrial,2)
    disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
    b0(:,:,:,min(nTrial,size(b0,4))+1:nTrial) = nan;
    stims.lastTrial = size(b0,4);
end
% Chunk video in repeats
stims.stimList = unique(stims.stimName);
nStim = length(stims.stimList);
param.nRep = ceil(nTrial / nStim);
% Ceiling nRep to handle cases where few trials were skipped at the end of the recording
videoFramesLabeled = zeros(size(videoFrames));
trialStart_video_frames = find(diff(videoFrames)==1)+1;
trialEnd_video_frames = find(diff(videoFrames)==-1);
videoByStim = nan(size(videoBytrial,1),nStim,param.nRep);
for i = 1:nStim
    idx = strcmp(stims.stimName,stims.stimList{i});
    videoByStim(:,i,1:sum(idx)) = videoBytrial(:,idx);
    idx = find(idx);
    for r = 1:length(idx)
        videoFramesLabeled(trialStart_video_frames(idx(r)):trialEnd_video_frames(idx(r))) = i;
    end
end
param.stims = stims;

end
