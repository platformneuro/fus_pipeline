% _,img=cap.read()
%         img = cv2.convertScaleAbs(img,alpha=3)
%         inv = cv2.bitwise_not(img)
%         thresh = cv2.cvtColor(inv, cv2.COLOR_BGR2GRAY)
%         kernel = np.ones((2,2),np.uint8)
%         erosion = cv2.erode(thresh,kernel,iterations = 1)
%         ret,thresh1 = cv2.threshold(erosion,white_threshold[session_folder.parts[-2][-3:]],255,cv2.THRESH_BINARY)
%         cnts, hierarchy = cv2.findContours(thresh1, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

%% Open video
clear
videofile = "D:\fUS\ocdom\Pavin\video\Pavin_2023-03-08\Pavin_retinotopy_01b.mp4";

vid = VideoReader(videofile);
a = vid.readFrame;
figure;
imagesc(a);
roiEye = roipoly;

y_crop_idx = 20:140;
x_crop_idx = 140:300;

%% pupil detection
vid.CurrentTime = 0;
a = vid.readFrame;
I = a(y_crop_idx,x_crop_idx,:);
I = double(rgb2gray(I));
I = I - min(I(:));
I = I ./ max(I(:));

n=1;
avg = mean2(I);
sigma = std2(I);
J = imadjust(I,[0 0.2],[0 1],1.3);

kernel = strel("rectangle",[2 2]);
J2 = imerode(J,kernel);
figure;
imshowpair(I,J2,'montage')

threshold = 0.15;
BW = imbinarize(J2,threshold);
figure;
imshowpair(J2,BW,'montage');

[~,threshold] = edge(J2,'sobel');
fudgeFactor = 0.25;
BWs = edge(J2,'sobel',threshold * fudgeFactor);
figure;
imshowpair(J2,BWs,'montage');

[~,threshold] = edge(BW,'sobel');
fudgeFactor = 0.5;
BWs = edge(BW,'sobel',threshold * fudgeFactor);
figure;
imshowpair(BW,BWs,'montage');

%% motion energy
vid.CurrentTime = 0;
a = vid.readFrame;
% a = a(y_crop_idx,x_crop_idx,:);
a = double(rgb2gray(a));
a = a(roiEye);
% I = I - min(I(:));
% I = I ./ max(I(:));
motion_energy = zeros(1,vid.NumFrames);
t = zeros(1,vid.NumFrames);
for f = 2:vid.NumFrames
    b = vid.readFrame;
%     b = b(y_crop_idx,x_crop_idx,:);
    b = double(rgb2gray(b));
    b = b(roiEye);
    motion_energy(f) = mean(abs(b - a),[1 2]);
    t(f) = vid.CurrentTime;
    a = b;
end

%% Plot motion energy
vid.CurrentTime = 657;
hvid = figure;
hFrame = imagesc(zeros(vid.Height,vid.Width));
hold on
% hRect = rectangle('Position',[x_crop_idx(1) y_crop_idx(1) x_crop_idx(end)-x_crop_idx(1) y_crop_idx(end)-y_crop_idx(1)],'EdgeColor','r');
cont = contourc(double(roiEye),[0.5 0.5]);

hax = gca;

hdata = figure;
plot(t,motion_energy)
hold on
hLine = line([vid.CurrentTime vid.CurrentTime],get(gca,'ylim'),'Color','r');
vidFrameLeft = vid.NumFrames - round(vid.CurrentTime * vid.FrameRate);
for f = 1:vidFrameLeft
    I = vid.readFrame;
    figure(hvid)
    delete(hFrame);
    hFrame = imagesc(I);
    hax.Children = circshift(hax.Children, -1); % Put rectangle back at foreground of image
    
    hLine.XData = [vid.CurrentTime vid.CurrentTime];
    
    pause(1/vid.FrameRate)
end