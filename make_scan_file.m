function make_scan_file(parameter_file)
% Create a global param file with the names of the different slices and
% their position on a reference scan.

if nargin < 1
    [parameter_file,parameter_file_path] = uigetfile;
    if parameter_file == 0, return; end
    run(fullfile(parameter_file_path,parameter_file));
else
    run(parameter_file);
end

if ~isfield(param,'dataPath')
    % Default data path - can be customized in parameter file
    param.dataPath = fullfile(param.rootDir,param.animal,'Iconeus','FilteredData');
end

if ~ischar(param.session)
    param.session = num2str(param.session);
end
fileName = sprintf('param_%s_%s.mat',param.animal,param.session);

if ~isfield(param,'savePath')
    % Default save path
    param.savePath = fullfile(param.rootDir,param.animal,'processedData');
end

switch param.fUS_recording_type
    case '3D'
        icoTag = '_fus3D.source.scan';
    case '2D'
        icoTag = '_fus2D.source.scan';
    otherwise
        error('Unknown recording type')
end
if param.addSlice == 0
    param.fileList = dir(fullfile(param.dataPath,['*' icoTag]));
    % Keep only file names that identify a slice (ex: 01a)
    datafileIdx = cell2mat(arrayfun(@(x)(~isempty(regexp(x.name,['(' param.session '[a-z]+_)'],'ONCE'))),param.fileList,'UniformOutput',false));
    param.fileList = {param.fileList(datafileIdx).name}';
    fileListToAnalyze = param.fileList;
    param.fileIdxOnScan = [];
else
    existingParam = load(fullfile(savePath,fileName));
    f = fieldnames(existingParam.param);
    for i = 1:length(f)
        param.(f{i}) = existingParam.param.(f{i});
    end
    param.fileList = dir(fullfile(param.dataPath,['*' icoTag]));
    % Keep only file names that identify a slice (ex: 01a)
    datafileIdx = cell2mat(arrayfun(@(x)(~isempty(regexp(x.name,['(' param.session '[a-z]+_)'],'ONCE'))),param.fileList,'UniformOutput',false));
    param.fileList = {param.fileList(datafileIdx).name}';
    param.addSlice = 1;
    fileListToAnalyze = setdiff(param.fileList,existingParam.param.fileList);

end

% save anatomy images for each slice
for i = 1:length(fileListToAnalyze)
    fprintf('Saving anatomy for slice %i / %i\n',i,length(fileListToAnalyze))
    sliceName = strrep(fileListToAnalyze{i},'sub-','');
    sliceName = strrep(sliceName,'_ses-Session','');
    sliceName = strrep(sliceName,icoTag,'');
    sliceName = strrep(sliceName,'-','_');
    [data, nfo] = loadIconeusData(fullfile(param.dataPath,fileListToAnalyze{i}),100);
    metadata = loadIconeusMetaData(fullfile(param.dataPath,fileListToAnalyze{i}));
%     dat = squeeze(data);
%     dat = permute(dat,[2 1 3:ndims(dat)]);
    dat = permute(data,[3 1 5 2 4 6]);
    dat = mean(dat,ndims(dat));
%     dat = squeeze(dat);
    param.anatomyImages.(sliceName) = dat;
%     anatomyImages.(sliceName) = dat;
end

%% Pick a reference scan

if param.addSlice == 0
    % scan = param.scanLoadFuntion(param.dataPath,param.scanList);
    scan = param.scanLoadFuntion(param.dataPath);
    
    param.scans = scan;
    scanNames = fieldnames(param.scans);
    for i = 1:length(scanNames)
        figure
        sliceViewer(param.scans.(scanNames{i}))
        title(scanNames{i})
    end
    if length(scanNames) > 1
        r = input(sprintf('pick a ref scan (1-%i) :',length(scanNames)));
    else
        r = 1;
    end
    param.referenceScanner = scanNames{r};
    param.referenceScannerIdx = r;
    fprintf('New reference scan : %s\n', scanNames{r})
    close all
else
    scanNames = fieldnames(param.scans);
end

if ~param.automatic_slice_alignment
    % Align slices on ref - Manual
    figure
    sliceViewer(sqrt(param.scans.(scanNames{param.referenceScannerIdx})))
    sliceList = fieldnames(param.anatomyImages);
    h = figure;
    for i = 1:length(sliceList) % Loop through slices
        for j = 1:size(param.anatomyImages.(sliceList{i}),3) % loop through slices within a 3d stack
            for k = 1:size(param.anatomyImages.(sliceList{i}),4) % loop through slices within the 4 plane probe
                figure(h);
                imagesc(sqrt(param.anatomyImages.(sliceList{i})(:,:,j,k)))
                colormap(gray)
                title(sliceList{i},'interpreter','none')
                r = input(sprintf('pick corresponding ref image (1-%i) :',size(param.scans.(scanNames{param.referenceScannerIdx}),3)));
                param.fileIdxOnScan(j,i,k) = r;
            end
        end
    end
else
    % Align slices on ref - Auto
%     figure
%     sliceViewer(sqrt(param.scans.(scanNames{param.referenceScannerIdx})))
    sliceList = fieldnames(param.anatomyImages);
%     h = figure;
    for i = 1:length(sliceList) % Loop through slices
        for j = 1:size(param.anatomyImages.(sliceList{i}),3) % loop through slices within a 3d stack
            for k = 1:size(param.anatomyImages.(sliceList{i}),4) % loop through slices within the 4 plane probe
                for l = 1:size(param.scans.(scanNames{param.referenceScannerIdx}),3)
                    cc = corrcoef(squeeze(param.scans.(scanNames{param.referenceScannerIdx})(:,:,l)),squeeze(param.anatomyImages.(sliceList{i})(:,:,j,k)));
                    ccList(l) = cc(1,2);
                end
                [ccVal, r] = max(ccList);
                fprintf('slice %s idx on scan : %d - cc value : %1.3f\n',sliceList{i},r,ccVal);
    %             r = input(sprintf('pick corresponding ref image (1-%i) :',size(param.scans.(scanNames{param.referenceScannerIdx}),3)));
                param.fileIdxOnScan(j,i,k) = r;
                if param.do_plots
                    figure;
                    subplot(1,2,1)
                    imagesc(sqrt(squeeze(param.anatomyImages.(sliceList{i})(:,:,j,k))))
                    colormap(gray)
                    title(sliceList{i},'interpreter','none')
                    subplot(1,2,2)
                    imagesc(sqrt(squeeze(param.scans.(scanNames{param.referenceScannerIdx})(:,:,r))))
                    colormap(gray)
                    title(sprintf('match in scan (r=%1.2f)',ccVal))
                end
            end
        end
    end
end
%% save param file
if ~exist(param.savePath,'dir')
    mkdir(param.savePath)
end
save(fullfile(param.savePath,fileName),'param');

disp('Done.')