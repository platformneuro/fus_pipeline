function exportFig(handles,figFormat,saveFolder)
% exportFig(handles,figFormat,saveFolder) exports matlab figures as images
% handles : Handle list. Default : export all current figures. 
% figFormat : Any image format accepted by saveas. Default : 'png'. 
% saveFolder : path to save the export. Default : pwd

if ~exist('handles','var') || isempty(handles)
    handles = get(groot, 'Children');
end
if ~exist('figFormat','var') || isempty(figFormat)
    % Default format : 'png'. Any format accepted by saveas.
    figFormat = 'png';
end
if ~exist('saveFolder','var') || isempty(saveFolder)
    saveFolder = pwd;
end

for i = 1:length(handles)
    if ~isempty(handles(i).Name)
        fileName = handles(i).Name;
    else
        fileName = ['Figure' num2str(handles(i).Number)];
    end
    saveas(handles(i),fullfile(saveFolder,fileName),figFormat)
end
    