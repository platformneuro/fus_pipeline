function masks = drawManualMask(b0)

Im = nanmean(b0,3:ndims(b0));
Im = Im ./ max(Im(:));
Im = sqrt(Im);
h = figure;
imagesc(Im)
colormap(gray)
disp('select cortex region')
masks.cortex = roipoly;
% disp('select sub cortical region')
% masks.subcortical = roipoly;
disp('select out region (CCA)')
masks.Out = roipoly;
% masks.Out = ~masks.subcortical | ~masks.cortex;
close(h)