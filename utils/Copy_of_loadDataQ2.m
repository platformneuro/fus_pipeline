function [data,param] = loadDataQ2(param)
% [data,param] = loadDataQ2(param)
% Load a series of slices and order them according to their anatomical
% order. Works with 2D and 3D slices, CCA corrected or not.
%
% Output :
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : (x,y,t)
% data.Anat : Image of slices (x,y,slice)

mainParam = load(fullfile(param.DataPath,['param_' param.Animal '_' param.Session '.mat']));
mainParam = mainParam.param;
param.dataParam = mainParam;

fileList = dir(fullfile(param.DataPath,[param.Animal '_' param.Session '*_' param.Type '.mat']));

if isempty(param.Filter) % If filter is empty, load all available slices
    a = arrayfun(@(x)(regexp(x.name, ['_' param.Session '([a-z]+)_'], 'Tokens')),fileList);
%     param.Filter = cell2mat(vertcat(a{:})');
    param.Filter = cellflat(a);
end

if ischar(param.Filter)
    % Legacy for Agnes data: case where slices are poited as 'abcfrs'
    param.Filter = num2cell(param.Filter);
end

% Re-order slices
% Need to flatten 3d slices here, to have all orders as one vector and
% merge individual 3d slices and other slices. Then loop over all of them
% and fill the final matrices at the correct position indexes.

c = 1;
refIdx = [];
Filters = {};
sliceIdx = [];
for s = 1:length(param.Filter)
    iFile = cellfun(@(x)(~isempty(regexp(x, [param.Type '_' param.Session param.Filter{s} '_'],'Once'))),mainParam.fileList);
    tmp = mainParam.fileIdxOnScan(:,s);
    refIdx = [refIdx; tmp(:)];
    Filters = [Filters repmat(param.Filter(s),[1 length(tmp)])];
    sliceIdx = [sliceIdx 1:length(tmp)];
    c = c + 1;
end
[~, refIdxSrtI] = sort(refIdx);
% param.Filter = param.Filter(refIdxSrtI);
Filters = Filters(refIdxSrtI);
sliceIdx = sliceIdx(refIdxSrtI);
for s = 1:length(param.Filter)
    idx = find(strcmp(Filters,param.Filter{s}));
    [~, idx2] = sort(sliceIdx(idx));
    finalOrder(s,:) = idx(idx2); % Indices of the linearized position of each slice per 3d volume.
end
param.MotorStepList = param.Filter;

% Try to order the stimulus list if they are numeric
% iNewList = 1:length(param.stimList);
% if iscell(param.stimList)
%     if prod(cellfun(@isstr,param.stimList))
%         newList = str2double(param.stimList);
%         if prod(~isnan(newList))
%             [newList, iNewList] = sort(newList);
%             param.stimList_loaded = param.stimList;
%             param.stimList = arrayfun(@num2str,newList,'UniformOutput',false);
%         end
%     end
% end

% Load the first slice of the data
if param.CCAcorr == 0
    % non CCA
    % First data point
    d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{1},param.Type)));
     minResponseLength = size(d.b0_r,3);
    % Stim info
    [param.FreqListFinal, stimOrderIdx] = sort(str2double(d.stims.stimList));
    param.stimList = d.stims.stimList;
    if isnan(param.FreqListFinal(1))
        param.FreqListFinal = 1:length(param.FreqListFinal);
    end
    
    if ~isfield(param,'dv_coo') || isempty(param.dv_coo)
        param.dv_coo = 1:size(d.b0_r,1);
    end
    if ~isfield(param,'pa_coo') || isempty(param.pa_coo)
        param.pa_coo = 1:size(d.b0_r,2);
    end

    d.b0_r = d.b0_r(param.dv_coo,param.pa_coo,:,:,:);
    
    data.Resp = nan(size(d.b0_r,1),size(d.b0_r,2),size(d.b0_r,3),size(d.b0_r,5),length(param.Filter),size(d.b0_r,4));
    data.Resp(:,:,:,:,1,:) = permute(d.b0_r(:,:,:,stimOrderIdx,:),[1 2 3 5 6 4]);
    if ~isempty(d.param.baseline_pre)
        data.baseline_pre(:,:,1) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_pre = [];
    end
    if ~isempty(d.param.baseline_post)
        data.baseline_post(:,:,1) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_post = [];
    end
    
    data.Resp_corr = [];
    data.baseline_pre_c = [];
    data.baseline_post_c = [];
else
    % CCA
    % Load info present in non CCA data files
    d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{1},param.Type)),'Anat','param','stims');
    % Stim info
    [param.FreqListFinal, stimOrderIdx] = sort(str2double(d.stims.stimList));
    param.stimList = d.stims.stimList;
    if isnan(param.FreqListFinal(1))
        param.FreqListFinal = 1:length(param.FreqListFinal);
    end
    
    % First data point
    dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{1},param.Type)));
    
    if ~isfield(param,'dv_coo') || isempty(param.dv_coo)
        param.dv_coo = 1:size(dCCA.Din_c,1);
    end
    if ~isfield(param,'pa_coo') || isempty(param.pa_coo)
        param.pa_coo = 1:size(dCCA.Din_c,2);
    end
    
    dCCA.Din_c = dCCA.Din_c(param.dv_coo,param.pa_coo,:,:,:,:);
%     data.Resp_corr = nan(size(dCCA.Din_c,1),size(dCCA.Din_c,2),size(dCCA.Din_c,3),size(dCCA.Din_c,5),length(param.Filter),size(dCCA.Din_c,4));
    data.Resp_corr = nan(size(dCCA.Din_c,1),size(dCCA.Din_c,2),size(dCCA.Din_c,3),size(dCCA.Din_c,5),length(Filters),size(dCCA.Din_c,4));
    
    data.Resp_corr(:,:,:,:,finalOrder(1,:),:) = permute(dCCA.Din_c(:,:,:,stimOrderIdx,:,:),[1 2 3 5 6 4]);
    minResponseLength = size(data.Resp_corr,3);
    if ~isempty(dCCA.baseline_pre_c)
        data.baseline_pre_c(:,:,1) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_pre_c = [];
    end
    if ~isempty(dCCA.baseline_post_c)
        data.baseline_post_c(:,:,1) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_post_c = [];
    end
    
    data.Resp = [];
    data.baseline_pre = [];
    data.baseline_post = [];
end

% Mask
for i = 1:length(d.param.msk)
    param.ManualMask(:,:,finalOrder(1,i)) = d.param.msk(i).cortex(param.dv_coo,param.pa_coo);
end
% Anatomy images
data.Anat = nan(length(param.dv_coo),length(param.pa_coo),length(Filters));
data.Anat(:,:,finalOrder(1,:)) = d.Anat(param.dv_coo,param.pa_coo,:);

% Load the rest of the data
if length(param.Filter) > 1
    for s = 2:length(param.Filter)
        
        if param.CCAcorr == 0
            % Non CCA data
            d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
            minResponseLength = min(size(d.b0_r,3), minResponseLength); % Handle cases where response duration was inconsistent.
            data.Resp(:,:,1:minResponseLength,:,s,:) = permute(d.b0_r(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:),[1 2 3 5 6 4]);
            data.Resp_corr = [];
            data.baseline_pre_c = [];
            data.baseline_post_c = [];
            if ~isempty(d.param.baseline_pre)
                data.baseline_pre(:,:,s) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
            end
            if ~isempty(d.param.baseline_post)
                data.baseline_post(:,:,s) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
            end
        else
            % CCA data
            dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
            minResponseLength = min(size(dCCA.Din_c,3), minResponseLength); % Handle cases where response duration was inconsistent.
            data.Resp_corr(:,:,1:minResponseLength,:,finalOrder(s,:),:) = permute(dCCA.Din_c(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:,:),[1 2 3 5 6 4]);
                        
            if ~isempty(dCCA.baseline_pre_c)
                data.baseline_pre_c(:,:,finalOrder(s,:)) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:,:),3);
            end
            if ~isempty(dCCA.baseline_post_c)
                data.baseline_post_c(:,:,finalOrder(s,:)) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:,:),3);
            end
            data.Resp = [];
            data.baseline_pre = [];
            data.baseline_post = [];
            d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{s},param.Type)),'Anat','param');

            % figure; imagesc(squeeze(mean(dCCA.Din_c,[3 4 5])));
        end
        data.Anat(:,:,finalOrder(s,:)) = d.Anat(param.dv_coo,param.pa_coo,:);
        
        for i = 1:length(d.param.msk)
            param.ManualMask(:,:,finalOrder(s,i)) = d.param.msk(i).cortex(param.dv_coo,param.pa_coo);
        end
    end
    
    % Cut off extra bins
    if param.CCAcorr == 0
        data.Resp = data.Resp(:,:,1:minResponseLength,:,:,:);
    else
        data.Resp_corr = data.Resp_corr(:,:,1:minResponseLength,:,:,:);
    end
end
