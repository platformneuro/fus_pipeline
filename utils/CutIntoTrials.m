function [fUSbytrial, baseline_before, baseline_after, stimulus_vector] = CutIntoTrials(fUS_file, trig_file, param, plt)
% CutIntoTrials reshapes the fUS raw signal into trials aligned with Baphy trials. 
%
%    [fUSbytrial, baseline_before, baseline_after] = CutIntoTrials(fUS_file, trig_file, param, plt)
%
% Outputs :
%    fUSbytrial : matrix of fUS signal (x,y,time,trial)
%    baseline_before : fUS signal recorded before the first Baphy trial
%    baseline_after : fUS signal recorded after the last Baphy trial
%    stimulus_vector : time vector (at fUS sampling rate) with stimulus (1) and
%    silent periods (0)
%
% Inputs :
%    fUS_file : path to raw fUS data or matrix of raw fUS data
%    trig_file : path to Baphy triggers csv file
%    param : optional parameters :
%        param.fUS_ITI : fUS inter trial intervals (400 msec);  
%        param.baphy_ITI = Baphy inter trial intervals (1050 msec);
%        param.strict : be strict when cutting into trials or not (0/1)
%        param.offset : Shifts the trial onset relative to the baphy trigger (0  msec).
%    plt : plot triggers (0/1)

if nargin < 4
    plt = 0;
end
if nargin < 3
    % time between 2 fUS trigs (inter-trig interval) in ms
    param.fUS_ITI = 400;  
    param.baphy_ITI = 1050;
    param.strict = 1; % be strict when cutting into trials or not
    param.offset = 0; % Shift the trial onset relative to the baphy trigger.
    param.trigStr = 'B-';
    param.stim_duration = 1;
    param.last_trial_index = [];
end

% Load trigger information file
file = readtable(trig_file,'ReadVariableNames',false);

% fUS trigs
trig_info = table2cell(file(:,2));
f_trigs = trig_info(cellfun(@(x) isequal(x,1),strfind(trig_info,'F-')));
f_trigs = cell2mat(cellfun(@(x) str2double(x(3:end)),f_trigs,'UniformOutput',false));

% baphy trigs
b_trigs = trig_info(cellfun(@(x) isequal(x,1),strfind(trig_info,param.trigStr)));
b_trigs = cell2mat(cellfun(@(x) str2double(x(length(param.trigStr)+1:end)),b_trigs,'UniformOutput',false));

% Adjust case where the first fUS trig did not happen at time 0
% This is setup-dependant
fUS_first_trigger_time = f_trigs(1);
if fUS_first_trigger_time ~= 0
    f_trigs = f_trigs - fUS_first_trigger_time;
    b_trigs = b_trigs - fUS_first_trigger_time;
end

if sum(b_trigs < 0) > 0
    warning(sprintf(['stimulus triggers happening before the first fUS trigger were detected.\n'...
        'These triggers are considered a bug and discarded.\n'... '
        'You may want to double check the integrity of your trigger .csv file.']))
    b_trigs = b_trigs(b_trigs>=0);
end

% Detect new trial when more than 1 sec between trigs
BetweenTrialsBreak = find(diff(b_trigs) > param.baphy_ITI);
NewTrialsBaphy = [b_trigs(1); b_trigs(BetweenTrialsBreak+1)];
if isempty(param.last_trial_index)
    n_trials = length(NewTrialsBaphy);
else
    n_trials = param.last_trial_index;
    NewTrialsBaphy = NewTrialsBaphy(1:n_trials);
    
end
lag = nan(n_trials,1);
NewTrialsfUS_idx = nan(n_trials,1);
for baphy_trial = 1:n_trials
    
    lags = NewTrialsBaphy(baphy_trial) - f_trigs;
    lags(lags < 0) = []; % keep only fUS trigs that happened BEFORE baphy
    
%     [lag(baphy_trial),NewTrialsfUS_idx(baphy_trial)] = min(lags);
    if isempty(lags) && baphy_trial == 1
        lag(baphy_trial) = 0;
        NewTrialsfUS_idx(baphy_trial) = 1;
    else
        [lag(baphy_trial),NewTrialsfUS_idx(baphy_trial)] = min(lags);
    end
    
end

NewTrialsfUS = f_trigs(NewTrialsfUS_idx);
% Trig idx by assuming that they happen every 400ms, to compensate for
% missing fUS trigs
% NewTrialsfUS_corrIdx = round((NewTrialsfUS+param.fUS_ITI)/param.fUS_ITI);



%% Load fUS data

if ischar(fUS_file)
    nfo = h5info(fUS_file); % Get HDF5 info, including data set names
    data = squeeze(h5read(nfo.Filename,'/Data')); % Load raw data
else
    data = fUS_file;
end
n_slice = size(data,4);
n_slice2 = n_slice;
n_plane = size(data,5);
% 
if param.interpolate && n_slice > 1
    n_slice2 = 1; % Get back to the fUS framerate after interpolation.
end

NewTrialsfUS_corrIdx = round(NewTrialsfUS/(param.fUS_ITI*n_slice2));


if plt
    t = 0:param.fUS_ITI:(size(data,3)*n_slice2-1)*param.fUS_ITI;
    h = figure;
    hold on
    for i =1:n_slice
        dataPlt = squeeze(nanmean(data(:,:,:,i)-nanmean(data(:,:,:,i),[1 2 3 4]),[1 2]));
        dataPlt = (dataPlt ./ max(dataPlt)) * 0.5;
%         plot(t(i:n_slice2:end), dataPlt+1.1+(0.3*i))
        plot(t(1:n_slice2:end), dataPlt+1.1+(0.3*i))
        legend_txt{i} = sprintf('CBV slice %i',i);
    end
    scatter(f_trigs,ones(length(f_trigs),1),'+');
    scatter(b_trigs,1.2*ones(length(b_trigs),1),'+');
    missing_trigs = find(diff(f_trigs)>param.fUS_ITI+50);
    scatter(f_trigs(missing_trigs)+param.fUS_ITI,0.9*ones(length(missing_trigs),1),'g+');
    scatter(NewTrialsBaphy,1.4*ones(n_trials,1),'o');
    scatter(NewTrialsfUS,1.4*ones(n_trials,1),'o');
    ylim([0 3])
    legend([legend_txt {'fUS','Baphy','missing fUS trigs','Baphy trials start','fUS trials starts'}])
    
end

stimulus_vector = zeros(1,size(data,3));
stimulus_vector(round(NewTrialsBaphy ./ param.fUS_ITI)+1) = 1;
stimulus_vector = conv(stimulus_vector,[ones(1,ceil(param.stim_duration/(param.fUS_ITI/1000)))]);
stimulus_vector = stimulus_vector(1:end-1);

%% Segment by trial

if param.strict
    min_ITI = min(diff(NewTrialsfUS));
    length_trial_fUS = floor((min_ITI/(param.fUS_ITI*n_slice2)));
else
    max_ITI = max(diff(NewTrialsfUS));
    length_trial_fUS = floor((max_ITI/(param.fUS_ITI*n_slice2)));
end
fUSbytrial = nan(size(data,2),size(data,1),length_trial_fUS,n_trials,n_slice,n_plane);

for trial = 1:n_trials
    if trial == n_trials
        trial_ITI =  size(data,3) - NewTrialsfUS_corrIdx(trial) - 1;
    else
        trial_ITI = (NewTrialsfUS_corrIdx(trial+1)- NewTrialsfUS_corrIdx(trial))-1;
    end
    
%     interval = NewTrialsfUS_corrIdx(trial)+(1:min(trial_ITI,length_trial_fUS));
    interval = (NewTrialsfUS_corrIdx(trial)+(1:min(trial_ITI,length_trial_fUS))) - round(param.offset/(param.fUS_ITI*n_slice2/1000));
    if interval(end) > size(data,3)
        break
    elseif interval(1) < 1
        i = find(interval>=0,1,'first');
        interval = [(interval(end)+1):(interval(end)+i) interval(i+1:end)];
        warning('Offset value is out of range for trial %i. The begining of the following trial will be used.',trial)
    end
    fUSbytrial(:,:,1:min(trial_ITI,length_trial_fUS),trial,:,:) = permute(data(:,:,interval,:,:),[2 1 3 4 5]);

    % --- Debug plots ---
%     if trial == 1, h = figure; else, figure(h); end
%     nanmat = isnan(squeeze(fUSbytrial(1,1,:,:)));
%     imagesc(nanmat)
%     pause(0.2)
    % --- /Debug plots ---
end


% keep time before first trial as baseline, only if lasts more than 10s
if NewTrialsfUS_corrIdx(1) > 10000/(param.fUS_ITI*n_slice)
    baseline_before = permute(data(:,:,1:NewTrialsfUS_corrIdx(1),:,:),[2 1 3 4 5]);
else 
    baseline_before = [];
end

% keep time after last trial as baseline (5 s after last trig), only if lasts more than 10s
LastBaphyTrig = ceil((b_trigs(end)+5000)/(param.fUS_ITI*n_slice));
if (size(data,3) - LastBaphyTrig) > 10000/(param.fUS_ITI*n_slice)
    baseline_after = permute(data(:,:,LastBaphyTrig:end,:,:),[2 1 3 4 5]);
else
    baseline_after = [];
end


