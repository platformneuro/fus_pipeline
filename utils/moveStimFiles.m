% Dirty script to move visual stimulus log from DATA to the right folders

fileList = uigetfile_n_dir(pwd,'Select visual stim folders');
rootDir = fileList{1}(1:strfind(fileList{1},'DATA')-1);
for f = 1:length(fileList)
    %log file
    logFile = dir(fullfile(fileList{f},'*_log.csv'));
    if isempty(logFile)
        warning('Could not find %s... Skip',fileList{f})
    elseif length(logFile) > 1
        warning('Found more than 1 file for %s',fileList{f})
    else
        [status,msg,msgID] = copyfile(fullfile(logFile.folder,logFile.name),fullfile(rootDir,'MatFiles',logFile.name));
    end
    %stim file
    stimFile = dir(fullfile(fileList{f},'*_triggerFUS.csv'));
    if isempty(stimFile)
        warning('Could not find %s... Skip',fileList{f})
    elseif length(stimFile) > 1
        warning('Found more than 1 file for %s',fileList{f})
    else
        [status,msg,msgID] = copyfile(fullfile(stimFile.folder,stimFile.name),fullfile(rootDir,'TrigFiles',stimFile.name));
    end
end