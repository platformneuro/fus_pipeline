function [SignalPixNorm,data,MeanNaNPerPix] = NormalizeSignal(data,param)
    
%Base_mean = mean(data(:,:,(param.PreStimSilence-2)*param.SR:(param.PreStimSilence)*param.SR,:,:,:),3);

   Base_mean = mean(data(:,:,1:(param.PreStimSilence)*param.SR,:,:,:),3);

    %SignalPixNorm = bsxfun(@minus, data, Base_mean);
    SignalPixNorm = bsxfun(@minus, data, mean(Base_mean,[4 6]));
    
    SignalPixNorm = bsxfun(@rdivide,SignalPixNorm, mean(Base_mean,[4 6])); disp('WATCH OUT: Dividing by CONSTANT baseline')
%     SignalPixNorm = bsxfun(@rdivide,SignalPixNorm, Base_mean); disp('WATCH OUT: Dividing by FLUCTUATING baseline')
    %remove trials with artefact
    M = max(SignalPixNorm,[],3);
    tmp = (M > param.Artefact); 
    s = size(M); s(length(s)+1)=1;
    MeanNaNPerPix = sum(tmp(:))/(prod(s)); %Check if many trials that screwed up...
    SignalPixNorm(repmat(tmp, [1 1 size(SignalPixNorm,3) 1 1 1])) = NaN;    
end 
