function r = PearsonCorr(a,b,dim)
    % Computes the Pearson correlation coefficient along a certain dimension
%     az = bsxfun(@minus, a, mean(a,dim));
%     bz = bsxfun(@minus, b, mean(b,dim));
%     a2 = az .^ 2;
%     b2 = bz .^ 2;
%     ab = az .* bz;
%     r = sum(ab, dim) ./ sqrt(sum(a2, dim) .* sum(b2, dim));% * (size(az,3)-1)/size(az,3);
%     r(isnan(r)) = 1;
%     
%     a2 = az .^ 2;
%     b2 = bz .^ 2;
%     ab = az .* bz;
%     r = sum(az .* bz, dim) ./ sqrt(sum(az .^ 2, dim) .* sum(bz .^ 2, dim));% * (size(az,3)-1)/size(az,3);
    r = sum(bsxfun(@minus, a, mean(a,dim)) .* bsxfun(@minus, b, mean(b,dim)), dim) ./ sqrt(sum(bsxfun(@minus, a, mean(a,dim)) .^ 2, dim) .* sum(bsxfun(@minus, b, mean(b,dim)) .^ 2, dim));
    r(isnan(r)) = 1;
end