function transfer_mask(rootDir,Animal,stimType,slices,sessionSource,sessionDest)
    
if isempty(slices) % If filter is empty, load all available slices
    load(fullfile(rootDir,Animal,'ProcessedData',sprintf('param_%s_%s.mat',Animal,sessionSource)));
    a = arrayfun(@(x)(regexp(x, [stimType '_' sessionSource '([a-z]+)_'], 'Tokens')),param.fileList);
    slices = cellflat(a);
end

if ischar(slices)
    % Legacy for Agnes data: case where slices are poited as 'abcfrs'
    slices = num2cell(slices);
end

if ~exist(rootDir,'dir')
    error('Can''t find path to root directory')
end

for ii = 1:length(slices)
    fprintf('Slice %s (%i/%i)\n',slices{ii},ii,length(slices))
    Ref = [sessionSource slices{ii}];
    a = load(fullfile(rootDir,Animal,'ProcessedData',sprintf('%s_%s_%s.mat',Animal,Ref,stimType)),'param');
    load(fullfile(rootDir,Animal,'ProcessedData',sprintf('%s_%s_%s.mat',Animal,[sessionDest slices{ii}],stimType)),'param')
    param.msk = a.param.msk;
    save(fullfile(rootDir,Animal,'ProcessedData',sprintf('%s_%s_%s.mat',Animal,[sessionDest slices{ii}],stimType)),'param','-append')
end