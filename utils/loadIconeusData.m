function [data, nfo] = loadIconeusData(filepath, maxT)
% [data, nfo] = loadIconeusData(filepath, maxT)
% Load fUS data from Iconeus (hdf5 format)
% filepath : path to .scan file
% maxT : int, index of last data point to load. Default : all data.

nfo = h5info(filepath); % Get HDF5 info, including data set names
dim = nfo.Datasets(1).Dataspace.Size;
start = ones(size(dim));

if exist('maxT','var') && ~isempty(maxT)
    dim(end) = maxT;
end

data = h5read(nfo.Filename,'/Data',start,dim); % Load raw data