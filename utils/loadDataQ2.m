function [data,param] = loadDataQ2(param)
% [data,param] = loadDataQ2(param)
% Load a series of slices and order them according to their anatomical
% order. Works with 2D and 3D slices, CCA corrected or not.
%
% Output :
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : (x,y,t)
% data.Anat : Image of slices (x,y,slice)

mainParam = load(fullfile(param.DataPath,['param_' param.Animal '_' param.Session '.mat']));
mainParam = mainParam.param;
param.dataParam = mainParam;

if ~isfield(param,'stim_to_load')
    param.stim_to_load = [];
end

fileList = dir(fullfile(param.DataPath,[param.Animal '_' param.Session '*_' param.Type '.mat']));

if isempty(param.Filter) % If filter is empty, load all available slices
    a = arrayfun(@(x)(regexp(x.name, ['_' param.Session '([a-z]+)_'], 'Tokens')),fileList);
%     param.Filter = cell2mat(vertcat(a{:})');
    param.Filter = cellflat(a);
end

if ischar(param.Filter)
    % Legacy for Agnes data: case where slices are poited as 'abcfrs'
    param.Filter = num2cell(param.Filter);
end

% Re-order slices
% Need to flatten 3d slices here, to have all orders as one vector and
% merge individual 3d slices and other slices. Then loop over all of them
% and fill the final matrices at the correct position indexes.

% c = 1;
refIdx = [];
Filters = {};
sliceIdx = [];
planeIdx = [];
[n_slice, n_recording, n_plane] = size(mainParam.fileIdxOnScan);

for s = 1:length(param.Filter)
    iFile = cellfun(@(x)(~isempty(regexp(x, [param.Type '_' param.Session param.Filter{s} '_'],'Once'))),mainParam.fileList);
    for ss = 1:n_slice
        tmp = mainParam.fileIdxOnScan(ss,find(iFile),:); % mainParam.fileIdxOnScan(3D slice, slice , probe plane)
        tmp = tmp(:);
        refIdx = [refIdx; tmp];
        Filters = [Filters repmat(param.Filter(s),[1 length(tmp)])];
        sliceIdx = [sliceIdx repmat(ss,[1 length(tmp)])];
        planeIdx = [planeIdx 1:length(tmp)];
    end
%     c = c + 1;
end
[~, refIdxSrtI] = sort(refIdx);
% param.Filter = param.Filter(refIdxSrtI);
Filters = Filters(refIdxSrtI);
sliceIdx = sliceIdx(refIdxSrtI);
planeIdx = planeIdx(refIdxSrtI);
for s = 1:length(param.Filter)
    for ss = 1:n_slice
        for p = 1:n_plane
            idx = find(strcmp(Filters,param.Filter{s}) & sliceIdx == ss & planeIdx == p);
    
%     [~, idx2] = sort(sliceIdx(idx)); % I think this sort should not be
%     here.
%     finalOrder(s,:) = idx(idx2); % Indices of the linearized position of each slice per 3d volume.
            finalOrder(s,ss,p) = idx; % Indices of the linearized position of each slice per 3d volume.
        end
    end
end
param.MotorStepList = param.Filter;

% Try to order the stimulus list if they are numeric
% iNewList = 1:length(param.stimList);
% if iscell(param.stimList)
%     if prod(cellfun(@isstr,param.stimList))
%         newList = str2double(param.stimList);
%         if prod(~isnan(newList))
%             [newList, iNewList] = sort(newList);
%             param.stimList_loaded = param.stimList;
%             param.stimList = arrayfun(@num2str,newList,'UniformOutput',false);
%         end
%     end
% end

% Load the first slice of the data
if param.CCAcorr == 0
    % non CCA
    % First data point
    d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{1},param.Type)));
    
    minResponseLength = size(d.b0_r,3);
    
    % Select only desired stimuli
    if ~isempty(param.stim_to_load)
        stim_idx = contains(d.stims.stimList,param.stim_to_load);
        d.stims.stimList = d.stims.stimList(stim_idx);
    else
        stim_idx = 1:length(d.stims.stimList);
    end
    
    % Stim info
    [param.FreqListFinal, stimOrderIdx] = sort(str2double(d.stims.stimList));
    param.stimList = d.stims.stimList;
    if isnan(param.FreqListFinal(1))
        param.FreqListFinal = 1:length(param.FreqListFinal);
    end
    
    if ~isfield(param,'dv_coo') || isempty(param.dv_coo)
        param.dv_coo = 1:size(d.b0_r,1);
    end
    if ~isfield(param,'pa_coo') || isempty(param.pa_coo)
        param.pa_coo = 1:size(d.b0_r,2);
    end

    d.b0_r = d.b0_r(param.dv_coo,param.pa_coo,:,stim_idx,:,:,:);
    
    data.Resp = nan(size(d.b0_r,1),size(d.b0_r,2),size(d.b0_r,3),size(d.b0_r,5),length(param.Filter),size(d.b0_r,4));
    data.Resp(:,:,:,:,1,:) = permute(d.b0_r(:,:,:,stimOrderIdx,:),[1 2 3 5 6 4]);
    if ~isempty(d.param.baseline_pre)
        data.baseline_pre(:,:,1) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_pre = [];
    end
    if ~isempty(d.param.baseline_post)
        data.baseline_post(:,:,1) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_post = [];
    end
    
    data.Resp_corr = [];
    data.baseline_pre_c = [];
    data.baseline_post_c = [];
else
    % CCA
    % Load info present in non CCA data files
    d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{1},param.Type)),'Anat','param','stims');

    % Select only desired stimuli
    if ~isempty(param.stim_to_load)
        stim_idx = contains(d.stims.stimList,param.stim_to_load);
        d.stims.stimList = d.stims.stimList(stim_idx);
    else
        stim_idx = 1:length(d.stims.stimList);
    end
    
    % Stim info
    [param.FreqListFinal, stimOrderIdx] = sort(str2double(d.stims.stimList));
    param.stimList = d.stims.stimList;
    if isnan(param.FreqListFinal(1))
        param.FreqListFinal = 1:length(param.FreqListFinal);
    end
    
    % First data point
    dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{1},param.Type)));
    
    if ~isfield(param,'dv_coo') || isempty(param.dv_coo)
        param.dv_coo = 1:size(dCCA.Din_c,1);
    end
    if ~isfield(param,'pa_coo') || isempty(param.pa_coo)
        param.pa_coo = 1:size(dCCA.Din_c,2);
    end
    
    dCCA.Din_c = dCCA.Din_c(param.dv_coo,param.pa_coo,:,stim_idx,:,:,:);
%     data.Resp_corr = nan(size(dCCA.Din_c,1),size(dCCA.Din_c,2),size(dCCA.Din_c,3),size(dCCA.Din_c,5),length(param.Filter),size(dCCA.Din_c,4));
    data.Resp_corr = nan(size(dCCA.Din_c,1),size(dCCA.Din_c,2),size(dCCA.Din_c,3),size(dCCA.Din_c,5),length(Filters),size(dCCA.Din_c,4));
    
    for ss = 1:n_slice
        for p = 1:n_plane
            data.Resp_corr(:,:,:,:,finalOrder(1,ss,p),:) = permute(dCCA.Din_c(:,:,:,stimOrderIdx,:,ss,p),[1 2 3 5 6 4]);
        end
    end
    minResponseLength = size(data.Resp_corr,3);
    if ~isempty(dCCA.baseline_pre_c)
        data.baseline_pre_c(:,:,1) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_pre_c = [];
    end
    if ~isempty(dCCA.baseline_post_c)
        data.baseline_post_c(:,:,1) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:),3);
    else
        data.baseline_post_c = [];
    end
    
    data.Resp = [];
    data.baseline_pre = [];
    data.baseline_post = [];
end

% Mask
% for i = 1:length(d.param.msk)
%     param.ManualMask(:,:,finalOrder(1,i)) = d.param.msk(i).cortex(param.dv_coo,param.pa_coo);
% end
data.Anat = nan(length(param.dv_coo),length(param.pa_coo),length(Filters));
for ss = 1:n_slice
    for p = 1:n_plane
        param.ManualMask(:,:,finalOrder(1,ss,p)) = d.param.msk(ss,p).cortex(param.dv_coo,param.pa_coo);
        data.Anat(:,:,finalOrder(1,ss,p)) = d.Anat(param.dv_coo,param.pa_coo,ss,p);
    end
end
% Anatomy images
% data.Anat(:,:,finalOrder(1,:)) = d.Anat(param.dv_coo,param.pa_coo,:);

% Load the rest of the data
if length(param.Filter) > 1
    for s = 2:length(param.Filter)
        
        if param.CCAcorr == 0
            % Non CCA data
            d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
            d.b0_r = d.b0_r(:,:,:,stim_idx,:,:,:);
            minResponseLength = min(size(d.b0_r,3), minResponseLength); % Handle cases where response duration was inconsistent.
            data.Resp(:,:,1:minResponseLength,:,s,:) = permute(d.b0_r(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:),[1 2 3 5 6 4]);
            data.Resp_corr = [];
            data.baseline_pre_c = [];
            data.baseline_post_c = [];
            if ~isempty(d.param.baseline_pre)
                data.baseline_pre(:,:,s) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
            end
            if ~isempty(d.param.baseline_post)
                data.baseline_post(:,:,s) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
            end
        else
            % CCA data
            dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
            dCCA.Din_c = dCCA.Din_c(:,:,:,stim_idx,:,:,:);
            minResponseLength = min(size(dCCA.Din_c,3), minResponseLength); % Handle cases where response duration was inconsistent.
            for ss = 1:n_slice
                for p = 1:n_plane
                    data.Resp_corr(:,:,1:minResponseLength,:,finalOrder(s,ss,p),:) = permute(dCCA.Din_c(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:,ss,p),[1 2 3 5 6 4]);
                end
            end
            if ~isempty(dCCA.baseline_pre_c)
                data.baseline_pre_c(:,:,finalOrder(s,:)) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:,:),3);
            end
            if ~isempty(dCCA.baseline_post_c)
                data.baseline_post_c(:,:,finalOrder(s,:)) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:,:),3);
            end
            data.Resp = [];
            data.baseline_pre = [];
            data.baseline_post = [];
            d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{s},param.Type)),'Anat','param');

            % figure; imagesc(squeeze(mean(dCCA.Din_c,[3 4 5])));
        end
        for ss = 1:n_slice
            for p = 1:n_plane
                data.Anat(:,:,finalOrder(s,ss,p)) = d.Anat(param.dv_coo,param.pa_coo,ss,p);
                param.ManualMask(:,:,finalOrder(s,ss,p)) = d.param.msk(ss,p).cortex(param.dv_coo,param.pa_coo);

            end
        end
%         for i = 1:length(d.param.msk)
%             param.ManualMask(:,:,finalOrder(s,ss,p)) = d.param.msk(ss,p).cortex(param.dv_coo,param.pa_coo);
%         end
    end
    
    % Cut off extra bins
    if param.CCAcorr == 0
        data.Resp = data.Resp(:,:,1:minResponseLength,:,:,:);
    else
        data.Resp_corr = data.Resp_corr(:,:,1:minResponseLength,:,:,:);
    end
end
