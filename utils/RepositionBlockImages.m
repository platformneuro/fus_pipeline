function [b0_rp,xoffset,yoffset,xpeak,ypeak,m] = RepositionBlockImages(b0,type,Margin,plt)
    
    %%% TO MERGE WITH THE OTHER RepositionImages...

    % will only be useful for slow drifts... Will be weird if single image,
    % since I take the mean. Be sure of what we're having.
    Anat = sqrt(squeeze(nanmean(b0,3)));
    
    %%% cut the sides that sometimes screw things up
    %%% WATCH OUT VERY SPECIFIC OF NSD EXPERIMENTS IN CORONAL
%     Anat(:,[1:30 size(Anat,2)-30:size(Anat,2)],:) = 0;
    
    xoffset = Margin; yoffset = Margin;
    ypeak = size(Anat,1) + Margin; xpeak = size(Anat,2) + Margin;
    
    for i = 2:size(Anat,3)
        if ~isnan(sum(mat2vec(Anat(:,:,i))))
            switch type
                case 'Consecutive'
                    A = Anat(:,:,i-1);
                    Aprime = zeros(size(Anat(:,:,i-1)) + [2*Margin 2*Margin]);
                    Aprime(Margin+1:size(Anat,1)+Margin,Margin+1:size(Anat,2)+Margin) = Anat(:,:,i-1);
                    
                    if isnan(sum((A(:))))
                        A=lastA;
                    end
                case 'First'
                    A = nanmean(Anat(:,:,1:10),3); % take 10 first images...
                    %                 if mod(i,100) == 2
                    %                     A = mean(Anat(:,:,i-1),3);
                    %                 end
            end
            
        else
            continue;  
        end
       
        x = normxcorr2(Anat(:,:,i),A);
        lastA = Anat(:,:,i);
        [m(i),idx(i)] = max(x(:));

        [ypeak(i),xpeak(i)] = ind2sub(size(x),idx(i)); %here watch out because of orientation & so on.. !
        ypeak(i) = ypeak(i) + Margin; xpeak(i) = xpeak(i) + Margin;
    end
    
    % remove changes that go back and forth + detect outliers
    %%% NOT SURE THIS IS PERFECT / UGLY
    diffy = diff(ypeak);
    for dy = 1:length(diffy)-1
        if (diffy(dy) == - diffy(dy+1)) & ~(diffy(dy) == 0)
            ypeak(dy+1) = ypeak(dy);
            ypeak(dy+2) = ypeak(dy);
            diffy = diff(ypeak);
        elseif (abs(diffy(dy))>10) & (abs(diffy(dy+1))>10) % outlier
            ypeak(dy+1) = ypeak(dy);
            ypeak(dy+2) = ypeak(dy)+diffy(dy)+diffy(dy+1);
            diffy = diff(ypeak);
        end
    end
    diffx = diff(xpeak);
    for dx = 1:length(diffx)-1
        if (diffx(dx) == - diffx(dx+1)) & ~(diffx(dx) == 0)
            xpeak(dx+1) = xpeak(dx);
            xpeak(dx+2) = xpeak(dx);
            diffx = diff(xpeak);
        elseif (abs(diffx(dx))>10) & (abs(diffx(dx+1))>10) % outlier
            xpeak(dx+1) = xpeak(dx);
            xpeak(dx+2) = xpeak(dx)+diffx(dx)+diffx(dx+1);
            diffx = diff(xpeak);
        end
    end
    
    yoffset = ypeak-size(Anat,1);
    xoffset = xpeak-size(Anat,2);
    
    % reposition the BF, masks, etc...
    b0_rp = NaN(size(b0) + [2*Margin 2*Margin 0 0]);
      
    %reposition compared to the first one...
    switch type
        case 'Consecutive'
            Xoffset = cumsum(xoffset-Margin)+Margin; Yoffset = cumsum(yoffset-Margin)+Margin;
            Xpeak = cumsum(xpeak-xpeak(1))+xpeak(1); Ypeak = cumsum(ypeak-ypeak(1))+ypeak(1);
        case 'First'
            Xoffset = xoffset; Yoffset = yoffset;
            Xpeak = xpeak; Ypeak = ypeak;
    end
        
    for i = 1:size(Anat,3)
        if ~isnan(sum(mat2vec(Anat(:,:,i))))
            b0_rp(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),:,i) = b0(:,:,:,i);
        end
    end
    
    % recut the margins
    b0_rp = b0_rp(Margin+1:end-Margin,Margin+1:end-Margin,:,:);
    if plt
        Anat_rp = sqrt(squeeze(mean(b0_rp,3)));
        imtool3D(Anat_rp)
    end
end


% BU single second
%     %%% TO MERGE WITH THE OTHER RepositionImages...
% 
%     % will only be useful for slow drifts... Will be weird if single image,
%     % since I take the mean. Be sure of what we're having.
%     Anat = b0(:,:,:);
%     
%     %%% cut the sides that something screw things up
%     %%% WATCH OUT VERY SPECIFIC OF NSD EXPERIMENTS IN CORONAL
% %     Anat(:,[1:30 size(Anat,2)-30:size(Anat,2)],:) = 0;
%     
%     xoffset = Margin; yoffset = Margin;
%     ypeak = size(Anat,1) + Margin; xpeak = size(Anat,2) + Margin;
%     
%     for i = 2:size(Anat,3)
%         if ~isnan(sum(mat2vec(Anat(:,:,i))))
%             switch type
%                 case 'Consecutive'
%                     A = Anat(:,:,i-1);
%                     Aprime = zeros(size(Anat(:,:,i-1)) + [2*Margin 2*Margin]);
%                     Aprime(Margin+1:size(Anat,1)+Margin,Margin+1:size(Anat,2)+Margin) = Anat(:,:,i-1);
%                     
%                     if isnan(sum((A(:))))
%                         A=lastA;
%                     end
%                 case 'First'
%                     A = nanmean(Anat(:,:,1:10),3); % take 10 first images...
%                     %                 if mod(i,100) == 2
%                     %                     A = mean(Anat(:,:,i-1),3);
%                     %                 end
%             end
%             
%         else
%             continue;  
%         end
%        
%         x = normxcorr2(Anat(:,:,i),A);
%         lastA = Anat(:,:,i);
%         [m(i),idx(i)] = max(x(:));
% 
%         [ypeak(i),xpeak(i)] = ind2sub(size(x),idx(i)); %here watch out because of orientation & so on.. !
%         ypeak(i) = ypeak(i) + Margin; xpeak(i) = xpeak(i) + Margin;
%     end
%     
%     % remove changes that go back and forth + detect outliers
%     %%% NOT SURE THIS IS PERFECT / UGLY
%     diffy = diff(ypeak);
%     for dy = 1:length(diffy)-1
%         if (diffy(dy) == - diffy(dy+1)) & ~(diffy(dy) == 0)
%             ypeak(dy+1) = ypeak(dy);
%             ypeak(dy+2) = ypeak(dy);
%             diffy = diff(ypeak);
%         elseif (abs(diffy(dy))>10) & (abs(diffy(dy+1))>10) % outlier
%             ypeak(dy+1) = ypeak(dy);
%             ypeak(dy+2) = ypeak(dy)+diffy(dy)+diffy(dy+1);
%             diffy = diff(ypeak);
%         end
%     end
%     diffx = diff(xpeak);
%     for dx = 1:length(diffx)-1
%         if (diffx(dx) == - diffx(dx+1)) & ~(diffx(dx) == 0)
%             xpeak(dx+1) = xpeak(dx);
%             xpeak(dx+2) = xpeak(dx);
%             diffx = diff(xpeak);
%         elseif (abs(diffx(dx))>10) & (abs(diffx(dx+1))>10) % outlier
%             xpeak(dx+1) = xpeak(dx);
%             xpeak(dx+2) = xpeak(dx)+diffx(dx)+diffx(dx+1);
%             diffx = diff(xpeak);
%         end
%     end
%     
%     yoffset = ypeak-size(Anat,1);
%     xoffset = xpeak-size(Anat,2);
%     
%     % reposition the BF, masks, etc...
%     b0_rp = NaN(size(Anat) + [2*Margin 2*Margin 0]);
%       
%     %reposition compared to the first one...
%     switch type
%         case 'Consecutive'
%             Xoffset = cumsum(xoffset-Margin)+Margin; Yoffset = cumsum(yoffset-Margin)+Margin;
%             Xpeak = cumsum(xpeak-xpeak(1))+xpeak(1); Ypeak = cumsum(ypeak-ypeak(1))+ypeak(1);
%         case 'First'
%             Xoffset = xoffset; Yoffset = yoffset;
%             Xpeak = xpeak; Ypeak = ypeak;
%     end
%         
%     for i = 1:size(Anat,3)
%         if ~isnan(sum(mat2vec(Anat(:,:,i))))
%             b0_rp(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),i) = b0(:,:,i);
%         end
%     end
%     
%     % recut the margins
%     b0_rp = b0_rp(Margin+1:end-Margin,Margin+1:end-Margin,:,:);
%         
%     % reshape
%     b0_rp = reshape(b0_rp,size(b0));
%     
%     if plt
%         Anat_rp = sqrt(squeeze(mean(b0_rp,3)));
%         imtool3D(Anat_rp)
%     end
% end

