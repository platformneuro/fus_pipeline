function [NewResp,NewAnat,NewMask] = RepositionImages_new(Resp,Anat,Mask,Margin,plt)
% Anat and Mask should be x * y * z
% Resp should be x * y * z * anything
s = size(Resp);
adjust = 1; % for displays, will keep only the minimum necessary

% Check that sizes match
assert(isequal(size(Anat),size(Mask)))
assert(isequal(size(Mask),s(1:3)))
n_slices = s(3);

xoffset = Margin; yoffset = Margin;
ypeak = size(Anat,1) + Margin; xpeak = size(Anat,2) + Margin;

m = nan(n_slices,1);
idx = nan(n_slices,1);

find_nans = isnan(Anat(:));
if any(find_nans)
    are_there_nans = 1;
    % replace by zeros to make cross-correlation work
    Anat(find_nans) = 0;
    disp('Found NaNs in Anat, replacing by 0 for this step')   
end
    
for i = 2 : n_slices
    
    A = Anat(:,:,i-1);
    Aprime = zeros(size(Anat(:,:,i-1)) + [2*Margin 2*Margin]);
    Aprime(Margin+1:size(Anat,1)+Margin,Margin+1:size(Anat,2)+Margin) = Anat(:,:,i-1);
    
    
    x = normxcorr2(Anat(:,:,i),A);
    [m(i),idx(i)] = max(x(:));
    
    [ypeak(i),xpeak(i)] = ind2sub(size(x),idx(i)); %here watch out because of orientation & so on.. !
    ypeak(i) = ypeak(i) + Margin; xpeak(i) = xpeak(i) + Margin;
    yoffset(i) = ypeak(i)-size(Anat,1);
    xoffset(i) = xpeak(i)-size(Anat,2);
       
    % overlay images
    if plt
        hAx = subplot(ceil(sqrt(size(Anat,3))), round(sqrt(size(Anat,3))), i);
        B = zeros(size(Aprime)); B(yoffset(i)+1:ypeak(i),xoffset(i)+1:xpeak(i)) = Anat(:,:,i);
        C = imfuse(Aprime/mean(Aprime(:)),B/mean(B(:)),'falsecolor','Scaling','joint','ColorChannels',[1 2 0]);
        imshow(C)
        imrect(hAx, [xoffset(i)+1, yoffset(i)+1, size(Anat,2), size(Anat,1)]);
    end
end

if are_there_nans
    % put back Nans 
    Anat(find_nans) = nan;
end

% Reposition everything

Xoffset = cumsum(xoffset-Margin)+Margin; Yoffset = cumsum(yoffset-Margin)+Margin;
Xpeak = cumsum(xpeak-xpeak(1))+xpeak(1); Ypeak = cumsum(ypeak-ypeak(1))+ypeak(1);


NewMask = zeros(size(Anat) + [2*Margin 2*Margin 0]);

if adjust
    
    for i = 1 : n_slices
        NewMask(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),i) = Mask(:,:,i);
    end
    
    % find optimal margins to cut
    ym = [arrayfun(@(x) find(sum(NewMask(:,:,x),2),1),1:size(NewMask,3)); arrayfun(@(x) find(sum(NewMask(:,:,x),2),1,'last'),1:size(NewMask,3))];
    xm = [arrayfun(@(x) find(sum(NewMask(:,:,x),1),1),1:size(NewMask,3)); arrayfun(@(x) find(sum(NewMask(:,:,x),1),1,'last'),1:size(NewMask,3))];
    
    Xkeep = min(xm(1,:)):max(xm(2,:));
    Ykeep = min(ym(1,:)):max(ym(2,:));
    NewMask = NewMask(Ykeep,Xkeep,:);
 
    NewAnat = zeros(length(Ykeep),length(Xkeep),n_slices);
    NewResp = zeros([length(Ykeep) length(Xkeep) s(3:end)]);
    
    % recut the margins
    for i = 1 : n_slices
        
        [~,YIndsBigPic,YIndsSmallPic] = intersect(Ykeep,Yoffset(i)+1:Ypeak(i));
        [~,XIndsBigPic,XIndsSmallPic] = intersect(Xkeep,Xoffset(i)+1:Xpeak(i));
        
        NewAnat(YIndsBigPic,XIndsBigPic,i) = Anat(YIndsSmallPic,XIndsSmallPic,i);
        NewResp(YIndsBigPic,XIndsBigPic,i,:) = Resp(YIndsSmallPic,XIndsSmallPic,i,:);
    end
    
else
    
    NewAnat = zeros(size(Anat) + [2*Margin 2*Margin 0]);
    NewResp = zeros([size(Anat) + [2*Margin 2*Margin 0] s(4:end)]);

    for i = 1 : n_slices
        NewMask(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),i) = Mask(:,:,i);
        NewAnat(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),i) = Anat(:,:,i);
        NewResp(Yoffset(i)+1:Ypeak(i),Xoffset(i)+1:Xpeak(i),i,:) = Resp(:,:,i,:);
    end
    
    % recut the margins
    NewAnat = NewAnat(Margin+1:end-Margin,Margin+1:end-Margin,:);
    NewResp = NewResp(Margin+1:end-Margin,Margin+1:end-Margin,:,:)
    NewMask = NewMask(Margin+1:end-Margin,Margin+1:end-Margin,:);
end

NewResp = reshape(NewResp, [size(NewResp,1) size(NewResp,2) n_slices s(4:end)]);
%NewMask(NewAnat == 0) = 0; % if some dead voxels are included in the mask
NewMask = logical(NewMask);


end
