function FromMtoMat(folder_path)
%%% Convert baphy .m files to .mat in folder_path specified

path = [folder_path '/MatFiles/'];
Mfiles = [path '*.m'];
ListFile = dir(Mfiles);
for ii = 1:length(ListFile)
    
    file = ListFile(ii).name;
    run([path file])
    save([path file(1:end-1) 'mat'],'globalparams','exptparams','exptevents');
    delete([path file])
    clear globalparams exptparams exptevents
end

end