function throwAllVariablesToMain()
% throwAllVariablesToMain() forwards all variables from the caller workspace to the base worspace.
% Dirty, for debug or exploration only.
% Use with caution, can cause memory issues.

vars = evalin('caller','who');
for i = 1:length(vars)
    evalin('caller',['outVar.' vars{i} '=' vars{i} ';']);
end
outVar = evalin('caller','outVar');
assignin('base','outVar',outVar)

