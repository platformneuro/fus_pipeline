function metadata = loadIconeusMetaData(filePath)
%  metadata = loadIconeusMetaData(filePath)
% Load metadate from iconeus .scan file in a matlab structure
% filePath : path to .scan file
% metadata : structure with metadate. Hierachy between groups in HDF5 is
% preserved.

nfo = h5info(filePath);
fid = H5F.open(filePath);

metadata = [];    
paths = retrieveDSpaths(nfo,{});

% Skip loading the actual fUS data
paths = paths(~strcmp(paths,'//Data'));

% Load every datasets from every groups in the fUS HDF5 data
for p = 1:length(paths)
    dsPath = paths{p};
    Fields  = regexp(dsPath,'[/ //]','split');
    Fields = Fields(~cellfun(@isempty,Fields));
    dsID = H5D.open(fid,dsPath);
    metadata = setfield(metadata, Fields{1:end},  H5D.read(dsID));
    H5D.close(dsID);
end

H5F.close(fid);

function [paths] = retrieveDSpaths(infoStruct,paths)
    % Recursive function to get datasets from HDF5 group n and apply istelf
	% to HDF5 group n+1 if it exists.
    for i = 1:length(infoStruct)
        paths_tmp = {infoStruct(i).Datasets.Name};
        paths_tmp = cellfun(@(x)([infoStruct(i).Name '/' x]),paths_tmp,'UniformOutput',false);
        paths = [paths paths_tmp];
        if ~isempty(infoStruct(i).Groups)
            paths = retrieveDSpaths(infoStruct(i).Groups,paths);
        end
    end
    