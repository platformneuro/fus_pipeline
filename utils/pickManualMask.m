function masks = pickManualMask(b0)

global filePath folderPath
if isempty(filePath)
    [filePath,folderPath] = uigetfile('.mat','Pick data file with existing mask');
end
load(fullfile(folderPath,filePath),'param');

masks = param.msk;