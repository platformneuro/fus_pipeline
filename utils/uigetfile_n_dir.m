function [pathname] = uigetfile_n_dir(varargin)
% [pathname] = uigetfile_n_dir()
% [pathname] = uigetfile_n_dir(start_path)
% [pathname] = uigetfile_n_dir(start_path, dialog_title)
%
%    Pick multiple directories and/or files

import javax.swing.JFileChooser;

p = inputParser;
addOptional(p,'start_path',pwd,@ischar)
addOptional(p,'dialog_title','',@ischar)
parse(p, varargin{:})

start_path = p.Results.start_path;
dialog_title = p.Results.dialog_title;

% if nargin == 0 | start_path == '' | start_path == 0 % Allow a null argument.
%     start_path = pwd;
% end

jchooser = javaObjectEDT('javax.swing.JFileChooser', start_path);

jchooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
if nargin > 1
    jchooser.setDialogTitle(dialog_title);
end

jchooser.setMultiSelectionEnabled(true);

status = jchooser.showOpenDialog([]);

if status == JFileChooser.APPROVE_OPTION
    jFile = jchooser.getSelectedFiles();
	pathname{size(jFile, 1)}=[];
    for i=1:size(jFile, 1)
		pathname{i} = char(jFile(i).getAbsolutePath);
	end
	
elseif status == JFileChooser.CANCEL_OPTION
    pathname = [];
else
    error('Error occured while picking file.');
end
