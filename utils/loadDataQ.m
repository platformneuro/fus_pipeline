function [data,param] = loadDataQ(param)
% data.Resp_corr : CCA corrected data (x,y,t,rep,slice,stim)
% data.Resp : non CCA data (x,y,t,rep,slice,stim)
% data.Base : (x,y,t)
% data.Anat : Image of slices (x,y,slice)

mainParam = load(fullfile(param.DataPath,['param_' param.Animal '_' param.Session '.mat']));
mainParam = mainParam.param;
param.dataParam = mainParam;

if ~isfield(param,'stim_to_load')
    param.stim_to_load = [];
end

fileList = dir(fullfile(param.DataPath,[param.Animal '_' param.Session '*_' param.Type '.mat']));

if isempty(param.Filter) % If filter is empty, load all available slices
    a = arrayfun(@(x)(regexp(x.name, ['_' param.Session '([a-z]+)_'], 'Tokens')),fileList);
%     param.Filter = cell2mat(vertcat(a{:})');
    param.Filter = cellflat(a);
end

if ischar(param.Filter)
    % Legacy for Agnes data: case where slices are poited as 'abcfrs'
    param.Filter = num2cell(param.Filter);
end
% Re-order slices
c = 1;
for s = 1:length(param.Filter)
    iFile = cellfun(@(x)(~isempty(regexp(x, [param.Type '_' param.Session param.Filter{s} '_'],'Once'))),mainParam.fileList);
    refIdx(c) = mainParam.fileIdxOnScan(iFile);
    c = c + 1;
end
[~, refIdxSrtI] = sort(refIdx);
param.Filter = param.Filter(refIdxSrtI);
param.MotorStepList = param.Filter;
% Load data
% non CCA
% First data point
d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{1},param.Type)));

if ~isfield(param,'dv_coo') || isempty(param.dv_coo)
    param.dv_coo = 1:size(d.b0_r,1);
end
if ~isfield(param,'pa_coo') || isempty(param.pa_coo)
    param.pa_coo = 1:size(d.b0_r,2);
end

param.ManualMask(:,:,1) = d.param.msk.cortex(param.dv_coo,param.pa_coo);

% Select only desired stimuli
if ~isempty(param.stim_to_load)
    stim_idx = contains(d.stims.stimList,param.stim_to_load);
    d.stims.stimList = d.stims.stimList(stim_idx);
end
    
[param.FreqListFinal, stimOrderIdx] = sort(str2double(d.stims.stimList));
param.stimList = d.stims.stimList;
if isnan(param.FreqListFinal(1))
    param.FreqListFinal = 1:length(param.FreqListFinal);
end

d.b0_r = d.b0_r(param.dv_coo,param.pa_coo,:,stim_idx,:);

data.Resp = nan(size(d.b0_r,1),size(d.b0_r,2),size(d.b0_r,3),size(d.b0_r,5),length(param.Filter),size(d.b0_r,4));
data.Resp(:,:,:,:,1,:) = permute(d.b0_r(:,:,:,stimOrderIdx,:),[1 2 3 5 6 4]);
if ~isempty(d.param.baseline_pre)
    data.baseline_pre(:,:,1) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
else
    data.baseline_pre = [];
end
if ~isempty(d.param.baseline_post)
    data.baseline_post(:,:,1) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
else
    data.baseline_post = [];
end

data.Anat = nan(length(param.dv_coo),length(param.pa_coo),length(param.Filter));
data.Anat(:,:,1) = d.Anat(param.dv_coo,param.pa_coo);

% CCA
% First data point
dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{1},param.Type)));
dCCA.Din_c = dCCA.Din_c(param.dv_coo,param.pa_coo,:,stim_idx,:);
data.Resp_corr = nan(size(dCCA.Din_c,1),size(dCCA.Din_c,2),size(dCCA.Din_c,3),size(dCCA.Din_c,5),length(param.Filter),size(dCCA.Din_c,4));
data.Resp_corr(:,:,:,:,1,:) = permute(dCCA.Din_c(:,:,:,stimOrderIdx,:),[1 2 3 5 6 4]);
minResponseLength = size(data.Resp_corr,3);
if ~isempty(dCCA.baseline_pre_c)
    data.baseline_pre_c(:,:,1) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:),3);
else
    data.baseline_pre_c = [];
end
if ~isempty(dCCA.baseline_post_c)
    data.baseline_post_c(:,:,1) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:),3);
else
    data.baseline_post_c = [];
end
% Rest
if length(param.Filter) > 1
    for s = 2:length(param.Filter)
        d = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
        
        minResponseLength = min(size(d.b0_r,3), minResponseLength); % Handle cases where response duration was inconsistent.
        
        data.Resp(:,:,1:minResponseLength,:,s,:) = permute(d.b0_r(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:),[1 2 3 5 6 4]);
        
        dCCA = load(fullfile(param.DataPath,sprintf('%s_%s%s_%s_corr2.mat',param.Animal,param.Session,param.Filter{s},param.Type)));
        data.Resp_corr(:,:,1:minResponseLength,:,s,:) = permute(dCCA.Din_c(param.dv_coo,param.pa_coo,1:minResponseLength,stimOrderIdx,:),[1 2 3 5 6 4]);
        
        data.Anat(:,:,s) = d.Anat(param.dv_coo,param.pa_coo);
        param.ManualMask(:,:,s) = d.param.msk.cortex(param.dv_coo,param.pa_coo);
        if ~isempty(d.param.baseline_pre)
            data.baseline_pre(:,:,s) = mean(d.param.baseline_pre(param.dv_coo,param.pa_coo,:),3);
        else
            data.baseline_pre(:,:,s) = NaN;
        end
        if ~isempty(d.param.baseline_post)
            data.baseline_post(:,:,s) = mean(d.param.baseline_post(param.dv_coo,param.pa_coo,:),3);
        else
            data.baseline_post(:,:,s) = NaN;
        end
        if ~isempty(dCCA.baseline_pre_c)
            data.baseline_pre_c(:,:,s) = mean(dCCA.baseline_pre_c(param.dv_coo,param.pa_coo,:),3);
        else
            data.baseline_pre_c(:,:,s) = NaN;
        end
        if ~isempty(dCCA.baseline_post_c)
            data.baseline_post_c(:,:,s) = mean(dCCA.baseline_post_c(param.dv_coo,param.pa_coo,:),3);
        else
            data.baseline_pre_c(:,:,s) = NaN;
        end
%         figure; imagesc(squeeze(mean(dCCA.Din_c,[3 4 5])));
    end
    
    % Cut off extra bins
    data.Resp = data.Resp(:,:,1:minResponseLength,:,:,:);
    data.Resp_corr = data.Resp_corr(:,:,1:minResponseLength,:,:,:);

end
