function masks = drawManualMask_custom(b0)

Im = nanmean(b0,3:ndims(b0));
Im = Im ./ max(Im(:));
Im = sqrt(Im);
h = figure;
imagesc(Im)
colormap(gray)

name = input('mask name (leave empty to exit): ','s');
while ~isempty(name)
    fprintf('select %s region',name)
    masks.(name) = roipoly;
    name = input('mask name (leave empty to exit): ','s');
end
close(h)