function [behavioralData, exptevents, exptparams, globalparams] = read_baphy_file(filePath)
% [behavioralData, exptevents, exptparams, globalparams] = read_baphy_file(filePath)
% [behavioralData, exptevents, exptparams, globalparams] = read_baphy_file()
% Returns all information from the baphy files. 

if nargin < 1
    [file, path] = uigetfile('.m','Select Baphy .m file :');
    filePath = fullfile(path,file);
end

disp(filePath);
run(filePath);

nameStr = {'Reference', 'Target'};
trialsID = [exptevents.Trial];
trialsNotes = {exptevents.Note};

for t = 1:exptparams.TotalTrials
    behavioralData(t).HitFromBaphy = exptparams.Performance(t).ThisTrial;
    behavioralData(t).LickRate = exptparams.Performance(t).LickRate;
%     behavioralData(t).CumHitRate = exptparams.Performance(t).HitRate;
%     behavioralData(t).CumMissRate = exptparams.Performance(t).MissRate;
    behavioralData(t).LickTimeRef = exptparams.FirstLick.Ref(t);
    behavioralData(t).LickTimeTar = exptparams.FirstLick.Tar(t);
    
    thisNotes = trialsNotes(trialsID == t);
    stimName = cellflat(regexp(thisNotes,'Stim , (\w+) , ','Tokens'));
    behavioralData(t).stimName = stimName{1};
    
    BaphyStimName = cellflat(regexp(thisNotes,'Stim , \w+ , (\w+)','Tokens'));
    behavioralData(t).stimNameBaphy = BaphyStimName{1};
    
    % Compute hit/miss/falsa alarm
    [~, responseDirection] = nanmin([behavioralData(t).LickTimeRef behavioralData(t).LickTimeTar]);
    if isnan(responseDirection) % No lick registered
        behavioralData(t).HitRecalc = 'Miss';
    else
        if strcmp(behavioralData(t).stimNameBaphy, nameStr{responseDirection})
            behavioralData(t).HitRecalc = 'Hit';
        else
            behavioralData(t).HitRecalc = 'False Alarm';
        end
    end
end