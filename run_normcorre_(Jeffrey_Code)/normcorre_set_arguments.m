%% normcorre_set_arguments
  %{
    Defines various arguments used for various kinds of batch normcorre scripts
    I run. Helps to keep them consistent. 
  %}

  max_shift = [10 10]; % Allow much rigid shifting !
  max_dev = [3 1];
  mot_uf = 4;
  vary_grid_size = 1;
  if ~vary_grid_size
    error('Don''t it''s a bad idea'); % I have this here because if I change my mind I want to avoid forgetting I reset grid_size each loop. But I can't imagine changing my mind right now. 
  else
    %grid_size = [size(b0_in,1),6]; %%% need to set this within each loop
  end
  overlap_pre = [48 24];
  overlap_post = [48 24];
  shifts_method = 'linear';
  init_batch = 2; % just use two frames. Image is clean, and this way I don't mean over movement. Won't let me use one frame, but 2 should be fine. 
  min_diff = [16,16];
  correct_bidir = false;
  %% set a constant
  minFramesForGoodRecording = 9; % less than 9, recording is unusable. 
  %% set a very important argument!!!
    framesIncluded = 2:9; % decides which frames will be included in everything going forward. At time of writing, 1:9 is the best option. Because of a weird effect with the first frame I've tried excluding it from the CCA correction. Doing this, I use frame 2:9, though I doubt this will be permemant. 

  
  
  
  
  
  
  