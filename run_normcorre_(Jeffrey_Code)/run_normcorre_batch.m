%% run_normcorre_batch
  %{
    In my "FilterAllData" function I save each trial to each trial folder.
    This had been fine and correct and good, but I need to concatenate all
    trials in order to make use of "run_normcorre_batch" which unfortunately
    means I need to retool my code to concatenate and load each trial.

    After doing this, I input the result into "run_normcorre_batch" and
    save the output for use in the CCA correction. 

    "run_normcorre_batch" is one of many functions in the NoRMCorre package.
    Here is the citation:
      Eftychios A. Pnevmatikakis and Andrea Giovannucci, *NoRMCorre: An online algorithm for piecewise rigid motion correction of calcium imaging data*, Journal of Neuroscience Methods, vol. 291, pp 83-94, 2017; doi: [https://doi.org/10.1016/j.jneumeth.2017.07.031](https://doi.org/10.1016/j.jneumeth.2017.07.031)

    
  %}


%% debug variables
  dontsaveanything = 0;
  showMovie = 0; % just keep this at 0 and set a breakpoint at the "if" statement. 
  saveCroppedDimensions = 0;
%% load basic addresses into workspace and add the address of the CCAcorrectionfUS function
  ferretToAnalyze = 'Cabecou';
  loadWorkspaceModule;
  %addpath(genpath('/Users/agnes.land/Dcu/PHD_Code/DownloadedCode/NoRMCorre-master'))
%% set the arguments
  normcorre_set_arguments

for slice = 1:length(sliceFolders)
  %% count number of sessions, based on number of session folders
    sessionFolders = dir([BehaveAddress ferretToAnalyze '/' sliceFolders(slice).name]);
    sessionFolders = sessionFolders(~cellfun(@isempty,regexp({sessionFolders.name},'session')));
    sessionFolders = sessionFolders(vertcat(sessionFolders.isdir)); % only want directories here
    numberOfSessions = length(sessionFolders); % number of fUS sessions available.
    for session = 1:numberOfSessions
     %% count number of performanceBlocks in this session, based on number of trialInfo files in  
        trialInfoFiles = dir([sessionFolders(session).folder '/' sessionFolders(session).name]);
        trialInfoFiles = trialInfoFiles(~cellfun(@isempty,regexp({trialInfoFiles.name},'trialinfo')));
        trialInfoFileNumber = length(trialInfoFiles); % number of fUS sessions available.
        %% load file containing performanceBlock labels, which are unique among sessions for each slice
          load([trialInfoFiles(1).folder '/specificPerformanceBlockStrings.mat'],'specificPerformanceBlockStrings');

        for performanceBlock = 1:trialInfoFileNumber
          %% setup variablesToKeep
            variablesToKeep = [];
            variablesToKeep = who;
          %% get the trialInfoFile loaded and confirm current performanceBlock type. 
            load([trialInfoFiles(performanceBlock).folder '/' trialInfoFiles(performanceBlock).name]); % load trialinfo file 

            currentPerformanceBlockString = specificPerformanceBlockStrings{performanceBlock};
            sessionNameString = sessionFolders(session).name;

            %% find all fUS trial data and make sure you only keep those you have PerformanceData for. 
            if contains(currentPerformanceBlockString,'active')
              workingDirfUS = dir([basefUSAddressWORKDRIVE ferretToAnalyze '/' ferretToAnalyze '_' sliceFolders(slice).name '/' sessionNameString '/' ferretToAnalyze '_' sliceFolders(slice).name '_' currentPerformanceBlockString]);
            elseif contains(currentPerformanceBlockString,'passive')
              workingDirfUS = dir([basefUSAddressWORKDRIVE ferretToAnalyze '/' ferretToAnalyze '_' sliceFolders(slice).name '/' sessionNameString '/' ferretToAnalyze '_' sliceFolders(slice).name '_' strrep(currentPerformanceBlockString,'passive','postpassive')]);
            end
              trialFolders = workingDirfUS(~cellfun(@isempty,regexp({workingDirfUS.name},'\d\d\d$')));
              trialsWithfUSData = 2:length(trialFolders); % Here I deal with the first trial lack-of-data problem
              if max(trialsWithfUSData) < size(PerformanceData,1)
                  reducedPerformanceData = PerformanceData(trialsWithfUSData,:);
              elseif max(trialsWithfUSData) >= size(PerformanceData,1)
                  reducedPerformanceData = PerformanceData(2:end,:);
              end
            %% gather all trials together
                usableTrials = [0;zeros([(min(length(trialsWithfUSData),size(reducedPerformanceData,1))) 1])]; % first trial always 0;
                for trialCount = 1:(min(length(trialsWithfUSData),size(reducedPerformanceData,1))) % we want to iterate through every trial and load up the fUS data. It will be worthwhile to keep track of n_repetitions and n_stim... 
                  if trialCount == 1
                    b0 = [];
                  end
                  trial = trialsWithfUSData(trialCount);
                  tempTrialData = load([trialFolders(trial).folder ['/'] trialFolders(trial).name '/Filtered_Trial_Data.mat']); % load the filtered Trial Data. It includes the aquisition times and the fUS data, but I will only be grabbing the fUS data
                  tempTrialData = tempTrialData.Doppler_image;
                  if size(tempTrialData,3) >= minFramesForGoodRecording
                    tempTrialData = tempTrialData(:,:,framesIncluded);
                    %% initialize arrays if this is the first loop

                    b0 = cat(4,b0,tempTrialData);
                    usableTrials(trial) = 1;
                  else
                    usableTrials(trial) = 0;
                  end
                  


                end
            %% ascertain that you are inputting a cropped image. Don't want to corrupt the out-region, and normcorre isn't designed for it. Specifically, because I have such large overlaps horizontally, I will corrupt the lateral edges if I include the under-cement here. 
              if exist([trialInfoFiles(1).folder '/pre_normcorre_cropDimensions.mat' ],'file')
                load([trialInfoFiles(1).folder '/pre_normcorre_cropDimensions.mat' ],'pre_normcorre_cropDimensions')
               
              else
                figure;imagesc(b0(:,:,1,1));
                good_crop = 0;
                while ~good_crop
                  left_adjust = input('Enter left adjust: ');
                  right_adjust = input('Enter right adjust: ');
                  bottom_adjust = input('Enter bottom adjust: ');
                  if isempty(left_adjust)
                    left_adjust = 10;
                  end
                  if isempty(right_adjust)
                    right_adjust = 30;
                  end
                  if isempty(bottom_adjust)
                    bottom_adjust = 20;
                  end
                  figure;imagesc(b0(1:(size(b0,1) - bottom_adjust),left_adjust:(size(b0,2)-right_adjust),1,1));
                  good_crop = input('Good crop? Enter 1 for yes, 0 or nothing for no: ');
                  if isempty(good_crop)
                  good_crop = 0;
                  end
                end
                pre_normcorre_cropDimensions = {[1:(size(b0,1) - bottom_adjust)]',[left_adjust:(size(b0,2)-right_adjust)]'};
                save([trialInfoFiles(1).folder '/pre_normcorre_cropDimensions.mat' ],'pre_normcorre_cropDimensions')
                if ~isempty(variablesToKeep)
                  clearvars('-except',variablesToKeep{:})
                end   
                
                continue
              
              end
              if saveCroppedDimensions % just saving cropped dimensions. 
                if ~isempty(variablesToKeep)
                  clearvars('-except',variablesToKeep{:})
                end   
                continue
              end
            %% define b0_in
              b0_in = b0(pre_normcorre_cropDimensions{1},pre_normcorre_cropDimensions{2},:,:);
            %% set loop-variable arguments
              if vary_grid_size
                grid_size = [size(b0_in,1),6];
              end
            %% rehape b0 for use with normcorre
              b0_in_initialSize = size(b0_in);
              Y = reshape(b0_in,[size(b0_in,[1 2]) prod(size(b0_in,[3 4]))]);
            %% apply arguments via NoRMCorreSetParms
              options_nonrigid = NoRMCorreSetParms('d1',size(Y,1),'d2',size(Y,2),'grid_size',grid_size,'mot_uf',mot_uf,'max_shift',max_shift,'max_dev',max_dev,'init_batch',init_batch,'overlap_pre',overlap_pre,'overlap_post',overlap_post,'shifts_method',shifts_method,'min_diff',min_diff,'correct_bidir',correct_bidir);
            %% run run_normcorre_batch
              [M2,shifts2,template2,options_nonrigid] = normcorre_batch(Y,options_nonrigid);
            %% stuff from the normcorre demo, to evaluate successes. 
              %% plot a movie with the results
              if showMovie
                %% compute metrics
                  nnY = quantile(Y(:),0.005);
                  mmY = quantile(Y(:),0.995);

                  [cY,mY,vY] = motion_metrics(Y,10);
                  [cM2,mM2,vM2] = motion_metrics(M2,10);
                  T = length(cY);
                  
                figure; 
                for t = 60:1:T
                    shifts2(t).shifts
                  
                    subplot(131);imagesc(sqrt(Y(:,:,t)),sqrt([nnY,mmY])); xlabel('raw data','fontsize',14,'fontweight','bold'); axis equal; axis tight;
                    title(sprintf('Frame %i out of %i',t,T),'fontweight','bold','fontsize',14); colormap('hot');
                    subplot(132);quiv = quiver(flipud(shifts2(t).shifts_up(:,:,1,2)),flipud(shifts2(t).shifts_up(:,:,1,1)),'AutoScale','off');
                      ylim(quiv.Parent,[-(max_shift(1)+1) (max_shift(1)+1)]); %%% do this if you want to focus on the y axis. 
                      %axis equal; %%% do this if you want the directions to be correct. I only care about the y axis, so no need. 
                      xlabel('shifts_up','fontsize',14,'fontweight','bold');
                    title(sprintf('Frame %i out of %i',t,T),'fontweight','bold','fontsize',14); colormap('hot');                  
                    subplot(133);imagesc(sqrt(M2(:,:,t)),sqrt([nnY,mmY])); xlabel('non-rigid corrected','fontsize',14,'fontweight','bold'); axis equal; axis tight;
                    title(sprintf('Frame %i out of %i',t,T),'fontweight','bold','fontsize',14); colormap('hot');
                    set(gca,'XTick',[],'YTick',[]);
                    drawnow;
                    pause(0.01);
                end
              end
                
                
                
            %% save the results
                saveAddress = [basefUSAddress ferretToAnalyze '/' ferretToAnalyze '_RepositionedData/'];
                if ~exist(saveAddress,'dir')
                   mkdir(saveAddress)
                end
                if ~dontsaveanything
                  save([saveAddress 'Repositioned_' sliceFolders(slice).name '_' currentPerformanceBlockString '.mat'],'Y','M2','shifts2','template2','options_nonrigid','pre_normcorre_cropDimensions','usableTrials','b0_in_initialSize','framesIncluded')
                else
                end
            % /[ferretname]_ProcessedData/[outROI_name]/[inROI_name]/[CCs_removed]
            if ~isempty(variablesToKeep)
              clearvars('-except',variablesToKeep{:})
            end      
        end
    end
end











































