% ocdom preprocessing

% Data
Animal = 'Chachi'; % Animal's name. Must match the path names
Hemis = '01'; % Session name, depending on your organization
slices = {'b'}; % Slice to process. {'a','b',....'n'} or {} for all files in param.fileList.
stimulusSetup = 'VisualStim'; % Which setup has been used to present stimuli. 'Baphy' (default) or 'VisualStim'
param.fUS_recording_type = '3D'; % This may be automatized with the .seq file, or with a check on ico data size
param.interpolate = true; % This interpolation can be needed for 3D recording. Interpolates data in time to emulate the fUS frame rate.

% flags
reload = 1; % Do we overwrite the existing motion corrected files ?
doMask = 0; % Do we do the manual mask ?
doCCA = 0; % Do we do the CCA correction ?
save_motion_corrected_data = false; % Do we save the motion corrected signal (before trial epoching)
param.do_plots = false;

% Path
rootDir = fullfile('E:\','fUS','ocdom');
stimDataPathSubFolderName = '2023-07-26'; % Can be a subfolder name for the trig and files. Empty string if not needed.

% Baphy stimulus type
% Make it a function that the user can change
stimType = 'retinotopy'; % 'oriFlashStim' 'privation' 'fovea' 'retinotopy'

% Mask parameters
inMask = 'cortex';
outMask = 'Out';
maskFunction = @pickManualMask; % Pick the function you want to draw the mask.
% @pickManualMask, @drawManualMask, @skipManualMask, @drawManualMask_custom

% raw data filtering
fUSfilter.type = ''; % Filter type. Empty to apply no filter.
fUSfilter.cutofFrequency = 0.1; %Hz

% CCA correction parameters
cca_params.recenterIn = 1;
cca_params.recenterOut = 1;
cca_params.pc2Keep = 250; %250 defaut
cca_params.cc2Remove = 20; % 20 defaut
cca_params.what2Remove = 'out';
cca_params.baseline_tps = 1:5;
        
% Trial identification
strict = 1; % Cut trial duration from data file if the trials have different length
last_trial_index = []; % Ignore any trial presented after trial n. Usefull to salvage interrupted data collection.

% Image registration
RepositionBlockImagesFlag = 1;
% common registration option
% template_path = 'D:\fUS\ocdom\Pavin\Iconeus\FilteredData_20230424\retinotopy_01a_4Dscan_6_fus3D.source.scan'; % Path to template image for registration. Usefull to register multiple slice to the same template. Empty string ('') if not needed.
template_path = 'E:\fUS\ocdom\Chachi\Iconeus\FilteredData_20230724\oriFlashStim_02b_4Dscan_3_fus3D.source.scan';

%fUS
SR = 1/0.6; % We need to see if this can be retrieved from the metadata. Can from the timestamps.
offset = 2.4; % shift the trial onset by x seconds toward the past.

% Stimuli (Maybe can be retrieved from baphy files automatically)
param.stim_duration = 1;
baphy_ITI = 1050;
