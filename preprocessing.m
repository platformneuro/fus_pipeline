function preprocessing(data_parameters)
% This script pre-process the fUS data :
% - load raw data
% - Rigid motion correction
% - Cut raw data into trials based on Baphy's output
% - Create a mask (manually) on the region of interest
% - Denoise fUS data with CCA
% - Save denoised and motion corrected data
%
% Steps can be toggled on and off with flag.
%
% TO DO
% - Implement common registration
% - Implement splitting 3d into 2 slices -> is it the best thing to do ? Maybe for now ?
% - Save fUS metadat in param structure DONE
% - Find how to read .seq files



if nargin < 1
    [data_parameters,data_parameters_file_path] = uigetfile;
    if data_parameters==0, return; end
    run(fullfile(data_parameters_file_path,data_parameters));
else
    run(data_parameters);
end

fUS_ITI = 1000 * 1/SR; % fUS inter-trial interval (msec).

% Default stimulusSetup
if ~exist('stimulusSetup','var')
	stimulusSetup = 'Baphy';
end

if isempty(slices) % If filter is empty, load all available slices
    param_hemis = load(fullfile(rootDir,Animal,'ProcessedData',sprintf('param_%s_%s.mat',Animal,Hemis)));
    a = arrayfun(@(x)(regexp(x, [stimType '_' Hemis '([a-z]+)_'], 'Tokens')),param_hemis.param.fileList);
    slices = cellflat(a);
%     slices = cell2mat(cellflat(a));
end

if ischar(slices)
    % Legacy for Agnes data: case where slices are pointed as 'abcfrs'
    slices = num2cell(slices);
end

if ~exist(rootDir,'dir')
    error('Can''t find path to root directory')
end

switch param.fUS_recording_type
    case '3D'
        icoTag = '_fus3D.source.scan';
    case '2D'
        icoTag = '_fus2D.source.scan';
    otherwise
        error('Unknown recording type')
end


for ii = 1:length(slices)
    
    disp(['Loading slice '  slices{ii} ' - ' num2str(ii) '/' num2str(length(slices))])
    
    Ref = [Hemis slices{ii}];
    
    %% Preprocessing
    if or(~exist(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']),'file'),reload)
        param.exp.Animal = Animal;
        param.exp.Hemis = Hemis;
        param.exp.Type = stimType;
        param.exp.MotorStepList = Ref;
        param.exp.Path = fullfile(rootDir, param.exp.Animal);
        param.exp.RepositionBlockImages = RepositionBlockImagesFlag;
        % param.exp.bloc_size = 12;
        param.exp.SR = SR;
%         param.exp.sound_length = sound_length;
        % param.exp.ITI = 4;
        % param.exp.bloc_length = floor(((param.exp.bloc_size+1) * param.exp.sound_length + param.exp.ITI) * param.exp.SR);
        % param.exp.n_blocs_by_run = 60;
%         param.nRep = nRep;
%         ReferenceName = dir([param.exp.Path '\Iconeus\FilteredData\sub-' param.exp.Animal '*_' param.exp.Type '_' Ref '_fus2D.source.scan']);
        ReferenceName = dir(fullfile(param.exp.Path, 'Iconeus','FilteredData',['*' param.exp.Type '_' Ref '*' icoTag]));
        
        % Create place holder mat file
        a = struct;
        save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'a')
        
        % Load registration template
        if ~isempty(template_path)
            [template, ~] = loadIconeusData(template_path,2);
%             data = squeeze(data);
%             if strcmp(param.fUS_recording_type,'3D')
%                 data = permute(data,[1 2 4 3]);
%             end
            template = permute(template,[1 3 6 5 2 4]);
            template = squeeze(mean(template,3));
            if param.do_plots
                for s = 1:size(template,3)
                    for p = 1:size(template,4)
                        figure
                        imagesc(template(:,:,s,p))
                        colormap(hot)
                        title(sprintf('reg template V %i P %i',s,p))
                    end
                end
            end
        else
            template = [];
        end
        
        fUS_file = dir(fullfile(param.exp.Path, 'Iconeus', 'FilteredData', ReferenceName.name));
        assert(length(fUS_file)==1)
        
%         trig_file = dir([fullfile(param.exp.Path, 'TrigFiles', param.exp.Animal,'*',stimDataPathSubFolderName) '_*_' param.exp.Type '_' Ref '_triggerFUS.csv']);
%         
%         if isempty(trig_file)
%             trig_file = dir([fullfile(param.exp.Path, 'TrigFiles', param.exp.Animal) '_*_' param.exp.Type '_*_' Ref '_triggerFUS.csv']);
%         end
        trig_file = dir(fullfile(param.exp.Path, 'TrigFiles',stimDataPathSubFolderName, [param.exp.Animal '*_' param.exp.Type '_' Ref '_triggerFUS.csv']));
        
        if isempty(trig_file)
            trig_file = dir(fullfile(param.exp.Path, 'TrigFiles',stimDataPathSubFolderName, [param.exp.Animal '*_' param.exp.Type '_*_' Ref '_triggerFUS.csv']));
        end
        assert(length(trig_file)==1)
        
        
        %% Load fUS data
        [data, ~] = loadIconeusData(fullfile(fUS_file.folder,fUS_file.name));
        if ndims(data) == 4
            % We have to thank Iconeus for that. Their data structure is
            % not consistant between recording configurations. 
            data = permute(data, [1 2 3 5 6 4]);
        end
        data = permute(data,[1 3 6 5 2 4]);
        param.n_slice = size(data,4); % Number of motor steps in 3d recording configuration
        param.n_plane = size(data,5); % Number of planes on 4d probe
        
        % Load fUS metadata
        metadata = loadIconeusMetaData(fullfile(fUS_file.folder,fUS_file.name));
        param.fUS_metadata = metadata;
        
        %% Motion correct
        % Correct drift in images
        if param.exp.RepositionBlockImages
            param.exp.normcorre = 1;
            
            if param.exp.normcorre
                % Use NoRMCorre toolbox
                % https://github.com/flatironinstitute/NoRMCorre
                
                normcorre_set_arguments
                
                % set loop-variable arguments
                if vary_grid_size
                    grid_size = [size(data,1),6];
                end
                % rehape b0 for use with normcorre
%                 sz = size(b0);
%                 Y = reshape(b0,[size(b0,[1 2]) prod(size(b0,[3 4]))]);
%                 Y = data;
%                 nans = isnan(snm(Y,[1 2]));
%                 Y = Y(:,:,~nans);
                % apply arguments via NoRMCorreSetParms
%                 options_nonrigid = NoRMCorreSetParms('d1',size(Y,1),'d2',size(Y,2),'grid_size',grid_size,'mot_uf',mot_uf,'max_shift',max_shift,...
%                     'max_dev',max_dev,'init_batch',init_batch,'overlap_pre',overlap_pre,'overlap_post',...
%                     overlap_post,'shifts_method',shifts_method,'min_diff',min_diff,'correct_bidir',correct_bidir);
                options_nonrigid = NoRMCorreSetParms('d1',size(data,1),'d2',size(data,2),'grid_size',grid_size,'mot_uf',mot_uf,'max_shift',max_shift,...
                                    'max_dev',max_dev,'init_batch',init_batch,'overlap_pre',overlap_pre,'overlap_post',...
                                    overlap_post,'shifts_method',shifts_method,'min_diff',min_diff,'correct_bidir',correct_bidir);
                % run run_normcorre_batch
%                 [b0_tmp,~,~,~] = normcorre_batch(Y,options_nonrigid);
                
                data_c = zeros(size(data));
                for s = 1:size(data,4)
                    for t = 1:size(data,5)
                        if ~isempty(template)
                            [data_c(:,:,:,s,t),~,template_out,~] = normcorre_batch(data(:,:,:,s,t),options_nonrigid,template(:,:,s,t));
                        else
                            [data_c(:,:,:,s,t),~,template_out,~] = normcorre_batch(data(:,:,:,s,t),options_nonrigid);
                        end
                        param.reg_template_out{s,t} = template_out;
                        param.reg_template{s,t} = template;
                    
                    % TO DO : Call motion_metrics to control registration quality
                    end
                end

            else
                %%% CLEAN THIS. Don't know why first doesn't work directly.
                % Code from Agnes, maybe outdated ?
                
%                 b0 = RepositionAllImages(b0,'First',20,0); % ugly
%                 
%                 %%% need here to recut all pixels with nans
%                 % reposition it in the corner
%                 tmp = nansum(b0(:,:,1,:),4); % changed nansum to sum, don't know why it was a nansum
%                 xidx = ~all(isnan(tmp),2);
%                 yidx = ~all(isnan(tmp));
%                 b0tmp = zeros(size(b0)); % can put zeros because will be outside the image. Not perfect though.
%                 b0tmp(1:sum(xidx),1:sum(yidx),:,:) = b0(xidx,yidx,:,:);
%                 b0 = b0tmp;
                
            end
            
        else
            data_c = data;
        end
        clear data
        
        %% Filter fUS data
        % This has not been tested with multi-dimentional data (3D, 4d
        % probe)
        param.fUSfilter = fUSfilter;
        if ~isempty(param.fUSfilter.type)
            if strcmp(param.fUSfilter.type,'highpass') % Basic highpass
%                 data_in = data_c;
                p.ManualMask = true(size(data_c,[1 2]));
%                 [OUT_mat] = Mat2Pixs(data_in,p);
                [OUT_mat] = Mat2Pixs(data_c,p);
                filtData = highpass(OUT_mat,param.fUSfilter.cutofFrequency,(1/fUS_ITI)*1000);
                [data_c] = Pixs2Mat(filtData,p);
                
                if  min(data_c(:)) < 0
                    data_c = data_c + abs(min(data_c(:)));
                end
                
            else % Custom filter - to implement

            end
        end
        
         %% Global baseline substraction - TEST
%         n_bins = size(data_c,3);
%         n_slice = size(data_c,4);
%         idx = false(size(data_c));
%         for slice = 1:n_slice
%     
% %             dat = data_c(:,:,:,slice);
% 
%             figure;
%             imagesc(nanmean(data_c(:,:,:,slice),3));
%             colormap(gray)
%             roi = roipoly();
%             idx(:,:,:,slice) = logical(repmat(roi,[1 1 n_bins]));
%         end
%         v = nan(size(data_c));
%         v(idx) = data_c(idx);
% 
%         baseline_roi = squeeze(nanmean(v,[1 2 4]));
% 
%         figure;
% %         shadedErrorBar(x,baseline_roi,baseline_roi_e{slice});
%         plot(baseline_roi);
%         title('baseline ROI')
%         xlabel('bin')
%         ylabel('CBV')
% 
%             % Baseline correction
% %         v_corr = v - permute(repmat(baseline_roi{slice},[1 size(v,1,2)]),[2 3 1]);
%         data_c = data_c - permute(repmat(baseline_roi,[1 size(v,1,2,4)]),[2 3 1 4]);
% %         roi_activity_corr{slice} = squeeze(nanmean(v_corr,[1 2]));
% %         roi_activity{slice} = squeeze(nanmean(v,[1 2]));
% %         figure;
% %         subplot(2,1,1)
% %         plot(x,roi_activity_corr{slice})
% %         subplot(2,1,2)
% %         plot(x,baseline_roi{slice})

        
        %% Interpolate fUS data
        
        n_slice = param.n_slice;
        n_plane = param.n_plane;
%         n_slice2 = n_slice;
        
        if param.interpolate && n_slice > 1
            fprintf('Interpolating data for 3D recording...')
            data_int = nan([size(data_c,[1 2]),(size(data_c,3) * n_slice)-1 + 2*(n_slice-1)-1, n_slice, n_plane]);
            for s = 1:n_slice
                for p = 1:n_plane
                    for i = 1:size(data_c,1)
                        for j = 1:size(data_c,2)
                            % %                 data_int(i,j,1:s+(size(data_c,3) * n_slice)-1-1,s) =[repmat(data_c(i,j,1,s),[1,s-1]) interp1(1:size(data_c,3),squeeze(data_c(i,j,:,s)),1:1/n_slice:size(data_c,3)) repmat(data_c(i,j,end,s),[1,n_slice-s])];
                            data_int(i,j,:,s,p) =[repmat(data_c(i,j,1,s,p),[1,s-1]) interp1(1:size(data_c,3),squeeze(data_c(i,j,:,s,p)),1:1/n_slice:size(data_c,3)) repmat(data_c(i,j,end,s,p),[1,n_slice-s])];
                        end
                    end
                end
            end
            clear data_c
            data_c = data_int;
            clear data_int
            fprintf(' Done.\n')
%             n_slice2 = 1; % Get back to the fUS framerate after interpolation.
        end

        %% Save corrected data before epoching
        if save_motion_corrected_data
            save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'data_c','-append');
        end
        
        %% Cut into trials
        switch stimulusSetup
            case 'Baphy'
                %prm.baphy_ITI_max = 4400; % Need a better solution for Baphy longer ISI...
                prm.strict = strict;
                prm.baphy_ITI = baphy_ITI;
                prm.fUS_ITI = fUS_ITI;
                prm.offset = offset;
                prm.trigStr = 'B-';
                prm.interpolate = param.interpolate;
                prm.stim_duration = param.stim_duration;
                prm.last_trial_index = last_trial_index;
                %         [b0, param.baseline_pre, param.baseline_post] = CutIntoTrials([fUS_file.folder '/' fUS_file.name], [trig_file.folder '/' trig_file.name],prm,1);
%                 [b0, param.baseline_pre, param.baseline_post] = CutIntoTrials(data_c, [trig_file.folder '/' trig_file.name],prm,1);
            case 'VisualStim'
                prm.strict = strict;
                prm.baphy_ITI = baphy_ITI;
                prm.fUS_ITI = fUS_ITI;
                prm.offset = offset;
                prm.trigStr = 'On-';
                prm.interpolate = param.interpolate;
                prm.stim_duration = param.stim_duration;
                prm.last_trial_index = last_trial_index;
%                 [b0, param.baseline_pre, param.baseline_post] = CutIntoTrials_visualSetup(data_c, [trig_file.folder '/' trig_file.name],prm,1);
%                 [b0, param.baseline_pre, param.baseline_post] = CutIntoTrials(data_c, [trig_file.folder '/' trig_file.name],prm,1);
            otherwise
                error('Unknown setup option.')
        end
        [b0, param.baseline_pre, param.baseline_post, stimulus_vector] = CutIntoTrials(data_c, [trig_file.folder '/' trig_file.name],prm,param.do_plots);
        if param.do_plots
            set(get(gca,'title'),'string',Ref)
        end
        % Remove incomplete time bins and calculate last complete trial in
        % case of interruption
        for i = 1:param.n_slice
            nanmat = isnan(squeeze(b0(1,1,:,:,i,:)));
            lastCompleteTrial = find(sum(nanmat,1) == size(nanmat,1),1,'first') - 1;
            if isempty(lastCompleteTrial)
                lastCompleteTrial = size(b0,4);
            end
            lastCompleteTimeBin = min(sum(~nanmat(:,1:lastCompleteTrial),1));
        end
        lastCompleteTimeBin = min(lastCompleteTimeBin);
        if lastCompleteTimeBin < size(b0,3)
            warning('NaN values in some trials. Shortened trials by %i.',size(b0,3)-lastCompleteTimeBin)
            b0 = b0(:,:,1:lastCompleteTimeBin,:,:,:);
        end
        %         if sum(isnan(b0(1,1,:,end))) > 1
        %             disp('Last trial was aborted. Removed it from b0')
        %             b0 = b0(:,:,:,1:end-1);
        %         end
        %         while sum(isnan(b0(:))) > 0
        %             disp('NaN values in some trials. Shortened trials by 1.')
        %             b0 = b0(:,:,1:end-1,:);
        %         end
        
        if param.do_plots
            clim = [1 14];
            for i = 1:param.n_slice
                for j = 1:param.n_plane
                    figure;
                    imagesc(snm(b0(:,:,:,:,i,j),[1 2]))
                    set(gca,'clim',clim)
                    title(sprintf('%s V %d P %d',Ref,i,j))
                end
            end
        end
        %b0 = b0(:,:,1:param.exp.bloc_length,:);
        
        % Correct experimental troubles like Hemiss stopped early, or split
        % in 2 , adapt to each case
        
        %         b0 = FixExperimentalPb(b0,param); % check this one
        
        Anat_raw = sqrt(snm(b0,[3 4])); % Get anatomy image before motion correction
        
        % To save anat and data before repositionning and cutting frames, and
        % reordering trials too
        %data.AnatOrig(:,:,ii) = sqrt(nanmean(nanmean(b0,3),4));

        
        %% Load baphy data, this depends on type of exp
        % Reshape b0 by stimulus and repeats
        switch stimulusSetup
            case 'Baphy'
                
                ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles',[ '*_' param.exp.Type '_' Ref '.mat']));
                %             ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_PIT_3.mat']);
                if ~isempty(ReferenceName)
                    ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_' param.exp.Type '_' Ref '.mat']);
                    EXPT(ii) = load([param.exp.Path '/MatFiles/' ReferenceName.name]);
                    
                else
                    
                    try
                        FromMtoMat(param.exp.Path);
                        ReferenceName = dir([param.exp.Path '\MatFiles\' param.exp.Animal '*_' param.exp.Type '_' Ref '*.mat']);
                        EXPT(ii) = load([param.exp.Path '\MatFiles\' ReferenceName.name]);
                        
                    catch
                        ReferenceName = dir([param.exp.Path '\MatFiles\' param.exp.Animal '*_' param.exp.Type '_' Ref '*.m']);
                        run([param.exp.Path '/MatFiles/' ReferenceName.name]);
                        EXPT(ii).exptevents = exptevents;
                        EXPT(ii).exptparams = exptparams;
                    end
                end
                
                
                tp = {EXPT(ii).exptevents.Note};
                tp = tp(cellfun(@(x) isequal(x,1),strfind(tp,'Stim')));
                tp = tp(arrayfun(@(x) isequal(x,0),contains(tp,'Silence')));
                %             x = strfind(tp,',');
                
                nTrial = max([EXPT(ii).exptevents.Trial]);
                % Specific case when baphy and fUS don't have same nb of trials
                if nTrial ~= size(b0,4)
                    
                    disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
                    b0(:,:,:,min(nTrial,size(b0,4))+1:nTrial)=nan;
                    stims.lastTrial = size(b0,4);
                end
                %         end
                % Record of sounds presented
                % TO DO : Make a general case, leave option for custom case.
                % Check with Yves to get as much info as possible from baphy files
                switch param.exp.Type
                    case 'PIT'
                        clear stims
                        stims.stimType = regexp(tp,'Pitch2021_([a-zA-z0-9]*)_' ,'Tokens');
                        stims.stimType = cellfun(@(x)(x{1}{1}),stims.stimType,'UniformOutput',false);
                        stims.f0 = regexp(tp,'Pitch2021_[a-zA-z0-9]*_(\d{3,5})Hz' ,'Tokens');
                        stims.f0 = cellfun(@(x)(x{1}{1}),stims.f0,'UniformOutput',false);
                        
                        % Works only if sound is in target !!
                        param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                            EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                            EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
                            EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
                        
                        for i = 1:nTrial
                            stims.stimName{i} = [stims.stimType{i} stims.f0{i}];
                        end
                    case 'FTC'
                        clear stims
                        stims.stimName = regexp(tp,'(\d{3,5})' ,'Tokens');
                        stims.stimName = cellfun(@(x)(x{1}{1}),stims.stimName,'UniformOutput',false);
                        
                        % Works only if sound is in target !!
                        %                 param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                        %                     EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                        %                     EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
                        %                     EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
                        param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                            EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                            EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence;
                        
                    otherwise % General case - Sound in target for Baphy
                        clear stims
                        stims.stimName = regexp(tp,'Stim , ([a-zA-Z_0-9.]+) ,' ,'Tokens');
                        stims.stimName = cellfun(@(x)(x{1}{1}),stims.stimName,'UniformOutput',false);
                        
                        param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
                        param.exp.trial_offset = prm.offset;
                        param.exp.PostStimSilence = EXPT(ii).exptparams.TrialObject.TargetHandle.PostStimSilence;
                        
                        notes = {EXPT(ii).exptevents.Note};
                        stimIdx = cellfun(@(x) isequal(x,1),regexp(notes,'Stim , [a-zA-Z0-9_.]+ , Target'));
                        durations = [EXPT(ii).exptevents(stimIdx).StopTime] - [EXPT(ii).exptevents(stimIdx).StartTime];
                        
                        if length(unique(durations))==1
                            param.exp.Duration = unique(durations);
                        else
                            param.exp.Duration = durations;
                        end
                end
                
            case 'VisualStim'
%                 ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles',[ '*_' param.exp.Type '_' Ref '_stim_log.csv']));
%                 if isempty(ReferenceName)
%                     ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles',[ '*_' param.exp.Type '_*_' Ref '_stim_log.csv']));
%                 end
                ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles', stimDataPathSubFolderName,[ '*_' param.exp.Type '_' Ref '_stim_log.csv']));
                if isempty(ReferenceName)
                    ReferenceName = dir(fullfile(param.exp.Path, 'MatFiles', stimDataPathSubFolderName,[ '*_' param.exp.Type '_*_' Ref '_stim_log.csv']));
                end
                param.exp.trial_offset = prm.offset;
%                 assert(length(ReferenceName)>1)
                if length(ReferenceName) == 1
                    EXPT = readtable(fullfile(ReferenceName.folder,ReferenceName.name),"FileType" ,'Text');
                    if ismember('stimulus',EXPT.Properties.VariableNames) % general case
                        stims.stimName = EXPT.stimulus;
                    elseif ismember('shape',EXPT.Properties.VariableNames)
                        stims.stimName = EXPT.shape;
                    elseif ismember('orientation',EXPT.Properties.VariableNames)
                        stims.stimName = EXPT.orientation;
                    else
                        error('Could not read stimulus type from stimulus log.')
                    end
                else % Case where there is no stim log file.
                    warning('There is no stim log file. Double check if that is expected.')
                    stims.stimName = ones(size(b0,4),1);
                end
                if ~iscell(stims.stimName)
                    % Save stim names as strings for consistency
                    stims.stimName = arrayfun(@(x)(num2str(x)),stims.stimName,'UniformOutput',false);
                end
                if isempty(last_trial_index)
                    nTrial = length(stims.stimName);
                else
                    nTrial = last_trial_index;
                    stims.stimName = stims.stimName(1:nTrial);
                end
                
                % Specific case when baphy and fUS don't have same nb of trials
                if nTrial ~= size(b0,4)
                    disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
                    b0(:,:,:,min(nTrial,size(b0,4))+1:nTrial,:,:) = nan;
                    stims.lastTrial = size(b0,4);
                end
                
            otherwise
                error('Unknown setup option.')
        end
        
        % Chunk b0 in repeats
        stims.stimList = unique(stims.stimName);
        nStim = length(stims.stimList);
        param.nRep = ceil(nTrial / nStim);
        % Ceiling nRep to handle cases where few trials were skipped at the end of the recording
        
        b0_r = nan(size(b0,1),size(b0,2),size(b0,3),nStim,param.nRep,param.n_slice,param.n_plane);
        for i = 1:nStim
            idx = strcmp(stims.stimName,stims.stimList{i});
            b0_r(:,:,:,i,1:sum(idx),:,:) = b0(:,:,:,idx,:,:);
        end
        
        % Format and save
%         data.Anat = sqrt(snm(b0_r,[3 4 5]));
%         Anat = data.Anat;
        Anat = sqrt(nanmean(b0_r,[3 4 5]));
        Anat = reshape(Anat,size(Anat,[1 2 6 7]));
        if param.do_plots
            for i = 1:param.n_slice
                for j = 1:param.n_plane
                    figure
                    imagesc(Anat(:,:,i,j))
                    set(get(gca,'title'),'string',[Ref ' V ' num2str(i) ' P ' num2str(j)])
                    colormap(hot)
                end
            end
        end
%         savefast(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'param','stims','b0_r','Anat','Anat_raw');
        save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'param','stims','b0_r','Anat','Anat_raw','stimulus_vector','-v7.3');

    end
    
    %% Mask loop
    if doMask
        if ~apply_same_mask
            clear global filePath
        end
        load(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']),'param','b0_r');
        %         b0_r = b0;
        % Load mask
        %         if param.msk.loadMask
        %             [param.msk.ManualMask,param.msk.OutMask] = LoadManualMask_WithOut(data,param);
        %         else
        % Add mask if needed
        %             if ~isfield(param.msk,'ManualMask')
        %param.msk = maskFunction(b0_r);
        %             end
        %             param.msk.ManualMask = ones(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
        %             param.msk.OutMask = zeros(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
        %         end

                 
%         add options :
%          - new manual mask -> drawManualMask. Done
%          - use already existing mask -> skipManualMask
%          - get mask from other file -> pickManualMask. Done

        % Backward compatibility
        if ~isfield('n_slice',param)
            param.n_slice = size(b0_r,6);
        end
        if ~isfield('n_plane',param)
            param.n_plane = size(b0_r,7);
        end
% 
%         Din = cell(1,param.n_slice);
%         Dout = cell(1,param.n_slice);

        Din = cell(param.n_slice,param.n_plane);
        Dout = cell(param.n_slice,param.n_plane);

        for s = 1:param.n_slice
            for p = 1:param.n_plane
                msk = maskFunction(b0_r(:,:,:,:,:,s,p));

                if length(msk) > 1
                    param.msk(s,p) = msk(s,p);
                else
                    param.msk(s,p) = msk;
                end
                [x,y, n_tps, n_stims, n_reps]= size(b0_r,1:5);

                D_slice = reshape(b0_r(:,:,:,:,:,s,p), [x*y , n_tps, n_stims, n_reps]);

                Mask_slice = logical(param.msk(s,p).(inMask));
                Out_slice = logical(param.msk(s,p).(outMask));

                Din{s,p} = permute(D_slice(Mask_slice(:),:,:,:),[2 3 4 1]);

                Dout{s,p} = permute(D_slice(Out_slice(:),:,:,:),[2 3 4 1]);
                %    n_timepoints x n_stims x n_repetitions x n_voxels

                %Solve some issues with nans in some voxels due to slice
                %repositionning
                tmp = permute(Din{s,p},[4 1 2 3]);
                tmp = tmp(:,:);
                weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
                if ~isempty(find(weirdVox,1))
                    disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in in'])
                    Din{s,p} = Din{s,p}(:,:,:,~weirdVox);
                    KeptVx = find(Mask_slice(:));
                    Mask_slice(KeptVx(weirdVox)) = 0;
                    param.msk.ManualMask(:,:,ii) = Mask_slice;

                    assert(size(Din{s,p},4)==length(find(mat2vec(param.msk.ManualMask(:,:,ii)))))
                end

                tmp = permute(Dout{s,p},[4 1 2 3]);
                tmp = tmp(:,:);
                weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
                if ~isempty(find(weirdVox,1))
                    disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in out'])
                    Dout{s,p} = Dout{s,p}(:,:,:,~ weirdVox);
                end
            end
        end
    %         savefast(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'Din','Dout','param','stims','b0_r','Anat');
          save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'Din','Dout','param','-append');
        
    end
    
    %% CCA loop
    if doCCA % denoise by rCCA
        load(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']),'Din','Dout','param','stims');
        
        % Backward compatibility
        if ~iscell(Din)
            Din{1} = Din;
            Dout{1} = Dout;
        end
        if ~isfield('n_slice',param)
            param.n_slice = size(Din,1);
        end
        if ~isfield('n_plane',param)
            param.n_plane = size(Din,2);
        end
        
        baseline_cca_params = cca_params;
        baseline_cca_params.recenterIn = 0;
        baseline_cca_params.recenterOut = 0;
        
        Din_c = [];
        baseline_post_c = [];
        baseline_pre_c = [];
        for s = 1:param.n_slice
            for p = 1:param.n_plane
                fprintf('Start CCA - vol %i - plane %i\n',s,p)
    %             Din_c{s} = CCAcorrection(Din{s},Dout{s},cca_params);
                param.ManualMask = param.msk(s,p).(inMask);
                Din_c(:,:,:,:,:,s,p) = Pixs2Mat(permute(CCAcorrection(Din{s,p},Dout{s,p},cca_params),[4 1 2 3]),param);

                if ~isempty(param.baseline_post)
                    [x, y, n_tps]= size(param.baseline_post,1:3);
                    baseline_cca_params.baseline_tps = 1:n_tps;
                    DBase_slice = reshape(param.baseline_post(:,:,:,s), [x*y , n_tps]);
                    Mask_slice = logical(param.msk(s,p).(inMask));
                    Out_slice = logical(param.msk(s,p).(outMask));
                    DBasein = permute(DBase_slice(Mask_slice(:),:,:,:),[2 1]);
                    DBaseout = permute(DBase_slice(Out_slice(:),:,:,:),[2 1]);
    %                 baseline_post_c(:,:,:,s) = CCAcorrection(DBasein,DBaseout,baseline_cca_params);
                    baseline_post_c(:,:,:,s,p) = Pixs2Mat(permute(CCAcorrection(DBasein,DBaseout,baseline_cca_params),[2 1]),param);

                end
                if ~isempty(param.baseline_pre)
                    [x, y, n_tps]= size(param.baseline_pre,1:3);
                    baseline_cca_params.baseline_tps = 1:n_tps;
                    DBase_slice = reshape(param.baseline_pre(:,:,:,s,p), [x*y , n_tps]);
                    Mask_slice = logical(param.msk(s,p).(inMask));
                    Out_slice = logical(param.msk(s,p).(outMask));
                    DBasein = permute(DBase_slice(Mask_slice(:),:,:,:),[2 1]);
                    DBaseout = permute(DBase_slice(Out_slice(:),:,:,:),[2 1]);
    %                 baseline_pre_c = CCAcorrection(DBasein,DBaseout,baseline_cca_params);
                    baseline_pre_c(:,:,:,s,p) = Pixs2Mat(permute(CCAcorrection(DBasein,DBaseout,baseline_cca_params),[2 1]),param);

                end
                %    n_timepoints x n_stims x n_repetitions x n_voxels
            end
        end
%         save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '_corr2_' num2str(cca_params.cc2Remove) '.mat'])),'Din_c','baseline_pre_c','baseline_post_c','param','stims','cca_params');
        save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '_corr2' '.mat'])),'Din_c','baseline_pre_c','baseline_post_c','param','stims','cca_params');
    end
    
end

disp('Done !')

