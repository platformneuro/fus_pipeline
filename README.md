# README #

The codes in here are the minimal pipeline to preprocess fUS data from the Iconeus setup and plot tonotopy.
This code has been mostly written by Celian, Agnes, Jeffrey.

### Main scripts ###
To be used in the following order:

**make_param_file.m** : create a param file for a given animal and recording window.  
This code use the scan from Iconeus to create a reference scan, and makes the user manually replace each slice on the scan.  
This allows volume reconstruction and analysis later.  

**preprocessing.m** : Pre-process the raw data.  
For a given animal and recodrding window, the Iconeus data is:  
- loaded  
- motion corrected  
- cut into trials according to the baphy files  
- the user manually define a cortex region and an out region for the CCA  
- CCA correction is applied  
- motion corrected data and CCA corrected data are saved for each slice.  

It is possible to run only part of the pre-processing with the flags with the following parameters:  
*reload* : 0/1, overwrite motion corrected raw data files  
*doMask* : 0/1, make the user define the masks on the slices  
*doCCA* : 0/1, run the CCA on each slice  

**tonotopy.m** :
Plot the tonotopy from the responses to pure tones. 

### Calling the functions ###
All functions are called with the same phylosophy :  
function(path/to/data_parameters,path/to/analysis_parameters)  
This allows flexible use across different needs. There are template parameter files for these functions.

### setup ###

clone the repository  
cd to the repository  
run setPaths.m  
Enjoy. Maybe. Hopefully.  

### Data organization ###

The code requires the following organization for the data folders (see template folder) :  
  
```
|-root directory  
  |- animal 1  
  |  |- Iconeus  
  |  |  |- FilteredData  
  |  |    |- .scan and .sec files  
  |  |    |- ...  
  |  |- MatFiles  
  |  |  |- baphy data files  
  |  |  |- ...  
  |  |- processedData  
  |  |  |- Processed data and param files will be saved here  
  |  |- TrigFiles  
  |    |- fus and baphy trigger files  
  |    |- ...  
  |- animal 2  
    |- ...  
```

Files from different sessions, slices and stimuli can be save in the same folder.  

### File names ###

The animal name, stimulus name, session name and slice name must match between the fUS data files, baphy files and trigger files.  
Example for animal Racotin, stimulis pitch (PIT), session 01, slice a :   
fUS data file :  
  sub-Racotin_ses-Session_2021-10-20_PIT_01a_fus2D.source  
Baphy file :  
  Racotin_2021_10_20_PIT_01a.mat  
Trigger files :  
  Racotin_2021_10_20_PIT_01a_triggerFUS.csv  
  



