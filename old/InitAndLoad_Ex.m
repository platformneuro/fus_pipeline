%%% Load BGFG data _ copie
clear data
slices = 'ef';
reload = 1;
clear param stim
param.exp.Animal = 'Racotin';

param.exp.Session = '01';
param.exp.RepositionBlockImages = 1;
param.exp.bloc_size = 12;
param.exp.SR = 1/0.4;
param.exp.sound_length = 9.6;
param.exp.ITI = 3;
param.exp.bloc_length = floor(((param.exp.bloc_size+1) * param.exp.sound_length + param.exp.ITI) * param.exp.SR);
param.exp.n_blocs_by_run = 9;

for ii = 1 : length(slices)
    
    param.exp.MotorStepList = slices(ii);
    param.exp.Path = [LoadOS 'data-1/BackgroundInvariance/' param.exp.Animal];
    if ~exist(param.exp.Path,'dir')
        error('Can''t find path')
    end
    
    disp(['Loading slice ' num2str(ii) ])
    
    Ref = [param.exp.Session param.exp.MotorStepList ];
    if or(~exist([LoadOS 'data/Agnes/BGFG/data/BGFG_' param.exp.Animal '_' Ref '.mat'],'file'),reload)
        ReferenceName = dir([param.exp.Path '/Iconeus/FilteredData/sub-' param.exp.Animal '*_NSD_' Ref '_fus2D.source.scan']);
        
        fUS_file = dir([param.exp.Path '/Iconeus/FilteredData/' ReferenceName.name]);
        assert(length(fUS_file)==1)
        
        trig_file = dir([param.exp.Path '/TrigFiles/' param.exp.Animal, '_*_NSD_' Ref '_triggerFUS.csv']);
        assert(length(trig_file)==1)
        
        prm.fUS_ITI = 400;
        prm.baphy_ITI = 3000;
        prm.strict = 0;
        b0 = CutIntoTrials([fUS_file.folder '/' fUS_file.name], [trig_file.folder '/' trig_file.name],prm,1);
        figure;
        imagesc(snm(b0,[1 2]))
        
        
        b0 = b0(:,:,1:param.exp.bloc_length,:);
        
        % Correct experimental troubles like sessions stopped early, or split
        % in 2 , adapt to each case
        b0 = FixExperimentalPb(b0,param); % check this one
        
        % To save anat and data before repositionning and cutting frames, and
        % reordering trials too
        data.AnatOrig(:,:,ii) = sqrt(nanmean(nanmean(b0,3),4));
        
        % Correct drift in images
        if param.exp.RepositionBlockImages
            param.exp.normcorre = 1;
            
            if param.exp.normcorre
                
                normcorre_set_arguments
                
                % set loop-variable arguments
                if vary_grid_size
                    grid_size = [size(b0,1),6];
                end
                % rehape b0 for use with normcorre
                sz = size(b0);
                Y = reshape(b0,[size(b0,[1 2]) prod(size(b0,[3 4]))]);
                nans = isnan(snm(Y,[1 2]));
                Y = Y(:,:,~nans);
                % apply arguments via NoRMCorreSetParms
                options_nonrigid = NoRMCorreSetParms('d1',size(Y,1),'d2',size(Y,2),'grid_size',grid_size,'mot_uf',mot_uf,'max_shift',max_shift,...
                    'max_dev',max_dev,'init_batch',init_batch,'overlap_pre',overlap_pre,'overlap_post',...
                    overlap_post,'shifts_method',shifts_method,'min_diff',min_diff,'correct_bidir',correct_bidir);
                % run run_normcorre_batch
                [b0_tmp,~,~,~] = normcorre_batch(Y,options_nonrigid);
               % b0 = reshape(b0_tmp,sz);
                b0 = nan(sz);
                b0(:,:,~nans) =  b0_tmp;
                
            else
                %%% CLEAN THIS. Don't know why first doesn't work directly.
                
                b0 = RepositionAllImages(b0,'First',20,0); % ugly
                
                %%% need here to recut all pixels with nans
                % reposition it in the corner
                tmp = nansum(b0(:,:,1,:),4); % changed nansum to sum, don't know why it was a nansum
                xidx = ~all(isnan(tmp),2);
                yidx = ~all(isnan(tmp));
                b0tmp = zeros(size(b0)); % can put zeros because will be outside the image. Not perfect though.
                b0tmp(1:sum(xidx),1:sum(yidx),:,:) = b0(xidx,yidx,:,:);
                b0 = b0tmp;
                
            end
        end
        % Load baphy data, this depends on type of exp
        % switch param.exp.Type case 'NSD'
        if strcmp(Ref,'01a') && strcmp(param.exp.Animal,'Banon')
            ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '2.m']);
            run([param.exp.Path '/MatFiles/' ReferenceName.name]);
            EXPT(ii).exptparams = exptparams;
            
            tp = cell(1,45);
            x = cell(1,45);
            blocs = {'Run2bis','Run2','Run1','Run3','Run3bis'};
            kk=0;
            for r = 1 : 5
                for b = 0:8
                    kk = kk+1;
                    tp{kk} = [blocs{r} '_bloc' num2str(b) '.wav'];
                    x{kk} = [1-2  length(tp{kk})+2];
                end
            end
        else
            
            ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '.mat']);
            if ~isempty(ReferenceName)
                ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '.mat']);
                EXPT(ii) = load([param.exp.Path '/MatFiles/' ReferenceName.name]);
                
            else
                
                try
                    FromMtoMat(param.exp.Path);
                    ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '.mat']);
                    EXPT(ii) = load([param.exp.Path '/MatFiles/' ReferenceName.name]);
                    
                catch
                    ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '.m']);
                    run([param.exp.Path '/MatFiles/' ReferenceName.name]);
                    EXPT(ii).exptevents = exptevents;
                    EXPT(ii).exptparams = exptparams;
                end
            end
            
            
            tp = {EXPT(ii).exptevents.Note};
            tp = tp(cellfun(@(x) isequal(x,1),strfind(tp,'Stim')));
            tp = tp(arrayfun(@(x) isequal(x,0),contains(tp,'Silence')));
            x = strfind(tp,',');
            
            
            % Specific case when baphy and fUS don't have same nb of trials
            if ~(length(x) == size(b0,4))
                
                disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
                b0(:,:,:,min(length(x),size(b0,4))+1:end)=nan;
                
            end
        end
        %% Record of sounds presented
        clear stims
        stims.fgs = cell(ceil(length(x)*param.exp.bloc_size/2),1);
        stims.bgs = cell(ceil(length(x)*param.exp.bloc_size/2),1);
        stims.fgs_times = nan(ceil(length(x)*param.exp.bloc_size/2),1);
        stims.bgs_times = nan(length(x)*param.exp.bloc_size/2,1);
        stims.blocs_ids = nan(length(x)*param.exp.bloc_size/2,3);
        stims.reps = nan(length(x)*param.exp.bloc_size/2,1);
        stims.runs = nan(length(x)*param.exp.bloc_size/2,1);
        
        b0_r = nan([size(b0,1:3) ceil(length(x)/2) 2]);
        
        for kk = 1:length(x)
            sd_name = tp{kk}(x{kk}(1)+2:x{kk}(2)-6);
            run_nb = str2double(sd_name(4));
            is_rep = strcmp(sd_name(5:7),'bis');
            strfind(sd_name,'bloc');
            bloc_nb = str2double(sd_name(strfind(sd_name,'bloc')+4));
            
            %  if str2double(bloc_nb) < 8
            bloc_info = load([LoadOS 'data/Lab/AudioFiles/BGFG/v2/' sd_name '_Info.mat']);
            
            b0_r(:,:,:,(run_nb-1)*param.exp.n_blocs_by_run+bloc_nb+1,is_rep + 1) = b0(:,:,:,kk);
            %  Mov_r(:,:,(run_nb-1)*param.exp.n_blocs_by_run+bloc_nb+1,is_rep + 1) = squeeze(MotionPCs(1:size(b0,3),kk,:))';
            
            %stims.reps((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb) = cat(1,stims.reps,(is_rep+1)*ones(param.exp.bloc_size,1));
            stims.runs(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size))= run_nb;
            
            if iscellstr(bloc_info.sounds_ids(1,1))
                stims.fgs(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = repmat(regexprep(bloc_info.sounds_ids(1,1),'.wav',''),param.exp.bloc_size,1);
            else
                stims.fgs(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = repmat({''},param.exp.bloc_size,1);
            end
            stims.fgs_times(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = bloc_info.sounds_starts(:,1);
            if all(isnan(bloc_info.sounds_starts(2:end,1)))
                stims.fgs_times(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size))=0:9.6:9.6*11;
            end
            
            if ~is_rep%% Record of sounds presented
                stims.blocs_ids(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size),:) = repmat([bloc_info.fg_id bloc_info.bg_id (run_nb-1)*param.exp.n_blocs_by_run+bloc_nb+1],param.exp.bloc_size,1);
            end
            if iscellstr(bloc_info.sounds_ids(1,2))
                stims.bgs(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = regexprep(bloc_info.sounds_ids(:,2),'.wav','');
            else
                stims.bgs(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = repmat({''},param.exp.bloc_size,1);
            end
            stims.bgs_times(((run_nb-1)*param.exp.n_blocs_by_run+bloc_nb)*param.exp.bloc_size+(1:param.exp.bloc_size)) = bloc_info.sounds_starts(:,2);
            
            
            
        end
        stims.fgs_list = unique(stims.fgs);
        stims.bgs_list = unique(stims.bgs);
        
        % Works only if sound is in target !!
        param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
            EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
            EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
            EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
        
        
        %% Format and save
        param.exp.Path = [LoadOS 'data/Agnes/' param.exp.Animal];
        data.Anat = sqrt(snm(b0_r,[3 4  5]));
        param.exp.Type = 'NSD';
        param.msk.loadMask = 1;
        % Load mask
        if param.msk.loadMask
            [param.msk.ManualMask,param.msk.OutMask] = LoadManualMask_WithOut(data,param);
        else
            param.msk.ManualMask = ones(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
            param.msk.OutMask = zeros(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
        end
        
        [x,y, n_tps, n_stims, n_reps]= size(b0_r);
        
        D_slice = reshape(b0_r, [x*y , n_tps, n_stims, n_reps]);
        
        Mask_slice = logical(param.msk.ManualMask);
        Out_slice = logical(param.msk.OutMask);
        
        Din = permute(D_slice(Mask_slice(:),:,:,:),[2 3 4 1]);
        
        Dout = permute(D_slice(Out_slice(:),:,:,:),[2 3 4 1]);
        %    n_timepoints x n_stims x n_repetitions x n_voxels
        
        %Solve some issues with nans in some voxels due to slice
        %repositionning
        tmp = permute(Din,[4 1 2 3]);
        tmp = tmp(:,:);
        weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
        if ~isempty(find(weirdVox,1))
            disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in in'])
            Din = Din(:,:,:,~weirdVox);
            KeptVx = find(Mask_slice(:));
            Mask_slice(KeptVx(weirdVox)) = 0;
            param.msk.ManualMask(:,:,ii) = Mask_slice;
            
            assert(size(Din,4)==length(find(mat2vec(param.msk.ManualMask(:,:,ii)))))
        end
        
        
        tmp = permute(Dout,[4 1 2 3]);
        tmp = tmp(:,:);
        weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
        if ~isempty(find(weirdVox,1))
            disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in out'])
            Dout = Dout(:,:,:,~ weirdVox);
        end
        
        Anat = data.Anat;
        save(mkpdir([LoadOS 'data/Agnes/BGFG/data/BGFG_' param.exp.Animal '_' Ref '.mat']),'Din','Dout','param','stims','b0','Anat');
    %else
     %   load([LoadOS 'data/Agnes/BGFG/data/BGFG_' param.exp.Animal '_' Ref '.mat'],'Din','Dout','param','stims');
        
        % denoise by rCCA
        
        cca_params.recenterIn = 0;
        cca_params.recenterOut = 0;
        cca_params.pc2Keep = 250;
        cca_params.cc2Remove = 20;
        cca_params.what2Remove = 'out';
        cca_params.baseline_tps = 1:size(Din,1);
        disp('Start CCA')
        Din_c = CCAcorrection(Din,Dout,cca_params);
        
        Din_c = Pixs2Mat(permute(Din_c,[4 1 2 3]),param.msk);
        
        save(mkpdir([LoadOS 'data/Agnes/BGFG/data/BGFG_' param.exp.Animal '_' Ref '_corr2.mat']),'Din_c','param','stims','cca_params');
        % corr for recentered, corr2 for non-recentered
    end
end



