function preprocessing(data_parameters)
% This script pre-process the fUS data :
% - load raw data
% - Rigid motion correction
% - Cut raw data into trials based on Baphy's output
% - Create a mask (manually) on the region of interest
% - Denoise fUS data with CCA
% - Save denoised and motion corrected data
%
% Steps can be toggled on and off with flag.

if nargin < 1
    [data_parameters,data_parameters_file_path] = uigetfile;
    run(fullfile(data_parameters_file_path,data_parameters));
else
    run(data_parameters);
end

% clear
% close all
% 
% Animal = 'Banon'; % Animal's name. Must match the path names
% Session = '02'; % Session name, depending on your organization
% slices = 'bcdefghijklmnopq'; % Slice to process. 'abcd...n'
% 
% % flags
% reload = 1; % Do we overwrite the existing motion corrected files ?
% doMask = 0; % Do we do the manual mask ?
% doCCA = 0; % Do we do the CCA correction ?
% 
% % Path
% % rootDir = 'Y:\pitchfus\';
% rootDir = 'F:\fUS\pitchfus\';
% 
% % Baphy stimulus type
% % Make it a function that the user can change
% stimType = 'FTC';
% 
% inMask = 'cortex';
% outMask = 'Out';
% maskFunction = @drawManualMask; % Pick the function you want to draw the mask

%clear param stim

%param.msk.loadMask = 0;

if isempty(slices) % If filter is empty, load all available slices
    load(fullfile(rootDir,Animal,'ProcessedData',sprintf('param_%s_%s.mat',Animal,Session)));
    a = arrayfun(@(x)(regexp(x, [Session '([a-z])_'], 'Tokens')),param.fileList);
    slices = cell2mat(cellflat(a));
end

for ii = 1 : length(slices)
    
    if ~exist(rootDir,'dir')
        error('Can''t find path to root directory')
    end
    
    disp(['Loading slice '  slices(ii) ' - ' num2str(ii) '/' num2str(length(slices))])
    
    Ref = [Session slices(ii)];
    
    if or(~exist(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']),'file'),reload)
        param.exp.Animal = Animal;
        param.exp.Session = Session;
        param.exp.Type = stimType;
        param.exp.MotorStepList = Ref;
        param.exp.Path = fullfile(rootDir, param.exp.Animal);
        param.exp.RepositionBlockImages = RepositionBlockImagesFlag;
        % param.exp.bloc_size = 12;
        param.exp.SR = SR;
%         param.exp.sound_length = sound_length;
        % param.exp.ITI = 4;
        % param.exp.bloc_length = floor(((param.exp.bloc_size+1) * param.exp.sound_length + param.exp.ITI) * param.exp.SR);
        % param.exp.n_blocs_by_run = 60;
        param.nRep = nRep;
%         ReferenceName = dir([param.exp.Path '\Iconeus\FilteredData\sub-' param.exp.Animal '*_' param.exp.Type '_' Ref '_fus2D.source.scan']);
        ReferenceName = dir(fullfile(param.exp.Path, 'Iconeus','FilteredData',['*' param.exp.Type '_' Ref '*_fus2D.source.scan']));
        
        fUS_file = dir(fullfile(param.exp.Path, 'Iconeus', 'FilteredData', ReferenceName.name));
        assert(length(fUS_file)==1)
        
        trig_file = dir([fullfile(param.exp.Path, 'TrigFiles', param.exp.Animal) '_*_' param.exp.Type '_' Ref '*_triggerFUS.csv']);
        %         trig_file.name = 'Laguiole_2021_09_30_PIT_3_triggerFUS.csv';
        %         trig_file.folder = 'D:\fUS\Pitch\Laguiole\TrigFiles';
        assert(length(trig_file)==1)
        
        prm.fUS_ITI = fUS_ITI;
        prm.baphy_ITI = baphy_ITI;
        %prm.baphy_ITI_max = 4400; % Need a better solution for Baphy longer ISI...
        prm.strict = strict;
        prm.fUSfilter = fUSfilter;
        [b0, param.baseline_pre, param.baseline_post] = CutIntoTrials([fUS_file.folder '/' fUS_file.name], [trig_file.folder '/' trig_file.name],prm,1);
        set(get(gca,'title'),'string',Ref)
        
        % Remove incomplete time bins and calculate last complete trial in
        % case of interruption
        nanmat = isnan(squeeze(b0(1,1,:,:)));
        lastCompleteTrial = find(sum(nanmat,1) == size(nanmat,1),1,'first') - 1;
        if isempty(lastCompleteTrial)
            lastCompleteTrial = size(b0,4);
        end
        lastCompleteTimeBin = min(sum(~nanmat(:,1:lastCompleteTrial),1));
        if lastCompleteTimeBin < size(b0,3)
            warning('NaN values in some trials. Shortened trials by %i.',size(b0,3)-lastCompleteTimeBin)
            b0 = b0(:,:,1:lastCompleteTimeBin,:);
        end
        %         if sum(isnan(b0(1,1,:,end))) > 1
        %             disp('Last trial was aborted. Removed it from b0')
        %             b0 = b0(:,:,:,1:end-1);
        %         end
        %         while sum(isnan(b0(:))) > 0
        %             disp('NaN values in some trials. Shortened trials by 1.')
        %             b0 = b0(:,:,1:end-1,:);
        %         end
        
        figure;
        imagesc(snm(b0,[1 2]))
        title(Ref)
        %b0 = b0(:,:,1:param.exp.bloc_length,:);
        
        % Correct experimental troubles like sessions stopped early, or split
        % in 2 , adapt to each case
        
        %         b0 = FixExperimentalPb(b0,param); % check this one
        
        Anat_raw = sqrt(snm(b0,[3 4 5])); % Get anatomy image before motion correction
        
        % To save anat and data before repositionning and cutting frames, and
        % reordering trials too
        %data.AnatOrig(:,:,ii) = sqrt(nanmean(nanmean(b0,3),4));
        
        % Correct drift in images
        if param.exp.RepositionBlockImages
            param.exp.normcorre = 1;
            
            if param.exp.normcorre
                
                normcorre_set_arguments
                
                % set loop-variable arguments
                if vary_grid_size
                    grid_size = [size(b0,1),6];
                end
                % rehape b0 for use with normcorre
                sz = size(b0);
                Y = reshape(b0,[size(b0,[1 2]) prod(size(b0,[3 4]))]);
                nans = isnan(snm(Y,[1 2]));
                Y = Y(:,:,~nans);
                % apply arguments via NoRMCorreSetParms
                options_nonrigid = NoRMCorreSetParms('d1',size(Y,1),'d2',size(Y,2),'grid_size',grid_size,'mot_uf',mot_uf,'max_shift',max_shift,...
                    'max_dev',max_dev,'init_batch',init_batch,'overlap_pre',overlap_pre,'overlap_post',...
                    overlap_post,'shifts_method',shifts_method,'min_diff',min_diff,'correct_bidir',correct_bidir);
                % run run_normcorre_batch
                [b0_tmp,~,~,~] = normcorre_batch(Y,options_nonrigid);
                % b0 = reshape(b0_tmp,sz);
                b0 = nan(sz);
                b0(:,:,~nans) =  b0_tmp;
                
            else
                %%% CLEAN THIS. Don't know why first doesn't work directly.
                % Code from Agnes, maybe outdated ?
                
                b0 = RepositionAllImages(b0,'First',20,0); % ugly
                
                %%% need here to recut all pixels with nans
                % reposition it in the corner
                tmp = nansum(b0(:,:,1,:),4); % changed nansum to sum, don't know why it was a nansum
                xidx = ~all(isnan(tmp),2);
                yidx = ~all(isnan(tmp));
                b0tmp = zeros(size(b0)); % can put zeros because will be outside the image. Not perfect though.
                b0tmp(1:sum(xidx),1:sum(yidx),:,:) = b0(xidx,yidx,:,:);
                b0 = b0tmp;
                
            end
        end
        
        % Load baphy data, this depends on type of exp
        % switch param.exp.Type case 'NSD'
        
        % TO DO : ADD a clean way to handle exceptional cases.
        % ex:
        % if flag
        %    run(myscript)
        % else %go to default
        %   ...
        % end
        %         if strcmp(Ref,'01a') && strcmp(param.exp.Animal,'Banon')
        %             ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_NSD_' Ref '2.m']);
        %             run([param.exp.Path '/MatFiles/' ReferenceName.name]);
        %             EXPT(ii).exptparams = exptparams;
        %
        %             tp = cell(1,45);
        %             x = cell(1,45);
        %             blocs = {'Run2bis','Run2','Run1','Run3','Run3bis'};
        %             kk=0;
        %             for r = 1 : 5
        %                 for b = 0:8
        %                     kk = kk+1;
        %                     tp{kk} = [blocs{r} '_bloc' num2str(b) '.wav'];
        %                     x{kk} = [1-2  length(tp{kk})+2];
        %                 end
        %             end
        %         else
        
        ReferenceName = dir(fullfile(param.exp.Path, param.exp.Animal, 'MatFiles',[ '*_' param.exp.Type '_' Ref '.mat']));
        %             ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_PIT_3.mat']);
        if ~isempty(ReferenceName)
            ReferenceName = dir([param.exp.Path '/MatFiles/' param.exp.Animal '*_' param.exp.Type '_' Ref '.mat']);
            EXPT(ii) = load([param.exp.Path '/MatFiles/' ReferenceName.name]);
            
        else
            
            try
                FromMtoMat(param.exp.Path);
                ReferenceName = dir([param.exp.Path '\MatFiles\' param.exp.Animal '*_' param.exp.Type '_' Ref '*.mat']);
                EXPT(ii) = load([param.exp.Path '\MatFiles\' ReferenceName.name]);
                
            catch
                ReferenceName = dir([param.exp.Path '\MatFiles\' param.exp.Animal '*_' param.exp.Type '_' Ref '*.m']);
                run([param.exp.Path '/MatFiles/' ReferenceName.name]);
                EXPT(ii).exptevents = exptevents;
                EXPT(ii).exptparams = exptparams;
            end
        end
        
        
        tp = {EXPT(ii).exptevents.Note};
        tp = tp(cellfun(@(x) isequal(x,1),strfind(tp,'Stim')));
        tp = tp(arrayfun(@(x) isequal(x,0),contains(tp,'Silence')));
        %             x = strfind(tp,',');
        
        nTrial = max([EXPT(ii).exptevents.Trial]);
        % Specific case when baphy and fUS don't have same nb of trials
        if nTrial ~= size(b0,4)
            
            disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
            b0(:,:,:,min(nTrial,size(b0,4))+1:nTrial)=nan;
            stims.lastTrial = size(b0,4);
        end
        %         end
        %% Record of sounds presented
        % TO DO : Make a general case, leave option for custom case.
        % Check with Yves to get as much info as possible from baphy files 
        switch param.exp.Type
            case 'PIT'
                clear stims
                stims.stimType = regexp(tp,'Pitch2021_([a-zA-z0-9]*)_' ,'Tokens');
                stims.stimType = cellfun(@(x)(x{1}{1}),stims.stimType,'UniformOutput',false);
                stims.f0 = regexp(tp,'Pitch2021_[a-zA-z0-9]*_(\d{3,5})Hz' ,'Tokens');
                stims.f0 = cellfun(@(x)(x{1}{1}),stims.f0,'UniformOutput',false);
                
                % Works only if sound is in target !!
                param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                    EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                    EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
                    EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
                
                for i = 1:nTrial
                    stims.stimName{i} = [stims.stimType{i} stims.f0{i}];
                end
            case 'FTC'
                clear stims
                stims.stimName = regexp(tp,'(\d{3,5})' ,'Tokens');
                stims.stimName = cellfun(@(x)(x{1}{1}),stims.stimName,'UniformOutput',false);
                
                % Works only if sound is in target !!
                %                 param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                %                     EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                %                     EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
                %                     EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
                param.exp.PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
                    EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
                    EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence;
                
            otherwise
                error('Unknown stim type')
        end
        
        % Chunk b0 in repeats
        stims.stimList = unique(stims.stimName);
        nStim = length(stims.stimList);
        nRep = nTrial / nStim;
        b0_r = nan(size(b0,1),size(b0,2),size(b0,3),nStim,param.nRep);
        for i = 1:nStim
            idx = strcmp(stims.stimName,stims.stimList{i});
            b0_r(:,:,:,i,1:sum(idx)) = b0(:,:,:,idx);
        end
        %         b0_r = b0;
        
        %% Format and save
%         data.Anat = sqrt(snm(b0_r,[3 4 5]));
%         Anat = data.Anat;
        Anat = sqrt(snm(b0_r,[3 4 5]));
%         savefast(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'param','stims','b0_r','Anat','Anat_raw');
        save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'param','stims','b0_r','Anat','Anat_raw');

    end
    if doMask
        load(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']));
        %         b0_r = b0;
        % Load mask
        %         if param.msk.loadMask
        %             [param.msk.ManualMask,param.msk.OutMask] = LoadManualMask_WithOut(data,param);
        %         else
        % Add mask if needed
        %             if ~isfield(param.msk,'ManualMask')
        param.msk = maskFunction(b0_r);
        %             end
        %             param.msk.ManualMask = ones(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
        %             param.msk.OutMask = zeros(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
        %         end
        
        [x,y, n_tps, n_stims, n_reps]= size(b0_r);
        
        D_slice = reshape(b0_r, [x*y , n_tps, n_stims, n_reps]);
        
        Mask_slice = logical(param.msk.(inMask));
        Out_slice = logical(param.msk.(outMask));
        
        Din = permute(D_slice(Mask_slice(:),:,:,:),[2 3 4 1]);
        
        Dout = permute(D_slice(Out_slice(:),:,:,:),[2 3 4 1]);
        %    n_timepoints x n_stims x n_repetitions x n_voxels
        
        %Solve some issues with nans in some voxels due to slice
        %repositionning
        tmp = permute(Din,[4 1 2 3]);
        tmp = tmp(:,:);
        weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
        if ~isempty(find(weirdVox,1))
            disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in in'])
            Din = Din(:,:,:,~weirdVox);
            KeptVx = find(Mask_slice(:));
            Mask_slice(KeptVx(weirdVox)) = 0;
            param.msk.ManualMask(:,:,ii) = Mask_slice;
            
            assert(size(Din,4)==length(find(mat2vec(param.msk.ManualMask(:,:,ii)))))
        end
        
        tmp = permute(Dout,[4 1 2 3]);
        tmp = tmp(:,:);
        weirdVox = any(isnan(tmp(:,~all(isnan(tmp)))),2);
        if ~isempty(find(weirdVox,1))
            disp(['Removing ' num2str(length(find(weirdVox))) ' weird voxels in out'])
            Dout = Dout(:,:,:,~ weirdVox);
        end
        
%         savefast(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'Din','Dout','param','stims','b0_r','Anat');
          save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '.mat'])),'Din','Dout','param','stims','b0_r','Anat');

    end
    if doCCA % denoise by rCCA
        load(fullfile(rootDir,Animal,'processedData',[Animal '_' Ref '_' stimType '.mat']),'Din','Dout','param','stims');
        
        disp('Start CCA')
        Din_c = CCAcorrection(Din,Dout,cca_params);
        param.ManualMask = param.msk.(inMask);
        Din_c = Pixs2Mat(permute(Din_c,[4 1 2 3]),param);
        
        baseline_cca_params = cca_params;
        baseline_cca_params.recenterIn = 0;
        baseline_cca_params.recenterOut = 0;
        if ~isempty(param.baseline_post)
            [x, y, n_tps]= size(param.baseline_post);
            baseline_cca_params.baseline_tps = 1:n_tps;
            DBase_slice = reshape(param.baseline_post, [x*y , n_tps]);
            Mask_slice = logical(param.msk.(inMask));
            Out_slice = logical(param.msk.(outMask));
            DBasein = permute(DBase_slice(Mask_slice(:),:,:,:),[2 1]);
            DBaseout = permute(DBase_slice(Out_slice(:),:,:,:),[2 1]);
            baseline_post_c = CCAcorrection(DBasein,DBaseout,baseline_cca_params);
            baseline_post_c = Pixs2Mat(permute(baseline_post_c,[2 1]),param);

        else
            baseline_post_c = [];
        end
        if ~isempty(param.baseline_pre)
            [x, y, n_tps]= size(param.baseline_pre);
            baseline_cca_params.baseline_tps = 1:n_tps;
            DBase_slice = reshape(param.baseline_pre, [x*y , n_tps]);
            Mask_slice = logical(param.msk.(inMask));
            Out_slice = logical(param.msk.(outMask));
            DBasein = permute(DBase_slice(Mask_slice(:),:,:,:),[2 1]);
            DBaseout = permute(DBase_slice(Out_slice(:),:,:,:),[2 1]);
            baseline_pre_c = CCAcorrection(DBasein,DBaseout,baseline_cca_params);
            baseline_pre_c = Pixs2Mat(permute(baseline_pre_c,[2 1]),param);

        else
            baseline_pre_c = [];
        end
        %    n_timepoints x n_stims x n_repetitions x n_voxels
        
        save(mkpdir(fullfile(rootDir,param.exp.Animal,'processedData', [param.exp.Animal '_' Ref '_' param.exp.Type '_corr2.mat'])),'Din_c','baseline_pre_c','baseline_post_c','param','stims','cca_params');
        % corr for recentered, corr2 for non-recentered
    end
    
end

disp('Done !')

