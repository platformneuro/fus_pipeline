function HDR = HomeMadeHDR(data,param)
    %First load data roughly zscored
    [sig] = ActivationMap(data,param,'zscore',param.filtertype,1);
    %Then take the mean of all 'responding pixels'
    s = size(sig.SignalPixNorm);
    if length(s) == 5, s(6) = 1; end % dirty fix for condition with only 1 stim
    Idx = sig.BF;Idx(isnan(Idx)) = 1; %will be removed by Significance_Mask
    Idx = repmat(permute(Idx,[1 2 4 5 3]),[1 1 s(3) s(4) 1]);
    RealIdx = (0:size(Idx(:))-1)'*s(6)+Idx(:);
    tmp = reshape(sig.SignalPixNorm,[s(1)*s(2)*s(3)*s(4)*s(5),s(6)])';
    tmp = tmp(int64(RealIdx));
    tmp = reshape(tmp,[s(1) s(2) s(3) s(4) s(5)]) ...
        .*repmat(permute(sig.Significance_Mask,[1 2 4 5 3]), ...
        [1 1 size(sig.SignalPixNorm,3) size(sig.SignalPixNorm,4) 1]);
    x = permute(tmp,[1 2 5 4 6 3]);
    x = reshape(x,[size(x,1)*size(x,2)*size(x,3)*size(x,4)*size(x,5),size(x,6)]);
    x = nanmean(x);
    HDR = x/max(x);
%     HDR(1:10) = 0; % Why ??
end