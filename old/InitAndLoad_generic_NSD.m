function  [data, param, b0_all] = InitAndLoad_generic_NSD(param)

param.exp.Path = [LoadOS 'data/' param.exp.Animal];
if ~exist(param.exp.Path,'dir')
        error('Can''t find path')
end

for ii = 1:length(param.exp.MotorStepList)
    
    disp(['Loading slice ' num2str(ii) ])
    
    % Load data
    Ref = [param.exp.Session param.exp.MotorStepList(ii)];
    ReferenceName = dir([param.exp.Path '/FilteredData/' param.exp.Animal, '_*_' param.exp.Type '_' Ref]);
    list_files = dir([param.exp.Path '/FilteredData/' ReferenceName.name]);
    b0 = LoadData(list_files,ReferenceName,param.exp);
    param.exp.Files{ii} = ReferenceName.name;
    
    % Correct experimental troubles like sessions stopped early, or split
    % in 2 , adapt to each case
    b0 = FixExperimentalPb(b0,param,ii);
    
    % When trial is split in 2 (such a mess)
    if strcmp(param.exp.Session,'01') && (strcmp(param.exp.Animal,'Abondance') && or(strcmp(param.exp.MotorStepList(ii),'a'),strcmp(param.exp.MotorStepList(ii),'g')))
        split_in_2_exception = 1;

        % load second file here
        b0 = b0(:,:,:,1:400);
        switch param.exp.MotorStepList(ii)
            case 'a'
                SecondFile = 'h';
            case 'g'
               % correct_flash_frame=1; % dropped this one !!! be careful
                SecondFile = 'j';
        end
        Refsec = [param.exp.Session SecondFile];
        ReferenceNamesec = dir([param.exp.Path '/FilteredData/' param.exp.Animal, '_*_' param.exp.Type '_' Refsec]);
        list_files = dir([param.exp.Path '/FilteredData/' ReferenceNamesec.name]);
        b0sec = LoadData(list_files,ReferenceNamesec,param.exp);
        b0sec =  b0sec(:,:,:,1:400);
        b0 = cat(4,b0,b0sec);
    else
        split_in_2_exception = 0;
    end
    
    % To save anat and data before repositionning and cutting frames, and
    % reordering trials too
    data.AnatOrig(1:size(b0,1),:,ii) = sqrt(nanmean(nanmean(b0,3),4));
    b0_all(1:size(b0,1),:,:,1:size(b0,4),ii) = b0; % not predefined, not great

    % Correct drift in images
    if param.exp.RepositionBlockImages
        
        %%% CLEAN THIS. Don't know why first doesn't work directly.
        
        b0 = RepositionBlockImages(b0,'First',20,0); % ugly
        
        %%% need here to recut all pixels with nans
        % reposition it in the corner
        tmp = nansum(b0(:,:,1,:),4); % changed nansum to sum, don't know why it was a nansum
        xidx = ~all(isnan(tmp),2);
        yidx = ~all(isnan(tmp));
        b0tmp = zeros(size(b0)); % can put zeros because will be outside the image. Not perfect though.
        b0tmp(1:sum(xidx),1:sum(yidx),:,:) = b0(xidx,yidx,:,:);
        b0 = b0tmp;
        
    end
    
    % correction for slice too small
    if strcmp(param.exp.Session,'02') && (strcmp(param.exp.Animal,'Chabichou')...
            && strcmp(param.exp.MotorStepList(ii),'a'))
        s = size(b0);
        b0_tmp = nan([49 s(2:end)]);
        b0_tmp(1:s(1),:,:,:) = b0;
        b0 = b0_tmp;
    end
  
    % Load baphy data, this depends on type of exp
    % switch param.exp.Type case 'NSD'
    if ~ exist([param.exp.Path '/MatFiles/' ReferenceName.name(1:strfind(ReferenceName.name,Ref)+2) '.mat'],'file')
        FromMtoMat(param.exp.Path);
    end
    
    EXPT(ii) = load([param.exp.Path '/MatFiles/' ReferenceName.name(1:strfind(ReferenceName.name,Ref)+2) '.mat']);
    
    tmp = {EXPT(ii).exptevents.Note};
    tmp = tmp(cellfun(@(x) isequal(x,1),strfind(tmp,'Stim')));
    tmp = tmp(arrayfun(@(x) isequal(x,0),contains(tmp,'Silence')));
    x = strfind(tmp,',');
    

    
    % Specific case when baphy and fUS don't have same nb of trials
    if ~(length(x) == size(b0,4)) && ~ split_in_2_exception
        
        disp 'fUS data and baphy EXPT file don''t have the same number of trials... Recheck ?!'
        b0(:,:,:,min(length(x),size(b0,4))+1:end)=nan;
        
        % another specific case...
        if strcmp(param.exp.Animal,'Abondance')&& (strcmp(param.exp.Session,'04') && strcmp(param.exp.MotorStepList(ii),'l'))
            x=x(1:end-1);
            disp('Corrected')
        end
    end
    
    % Record of sounds presented, TO ADAPT
    % specific for param.exp.Exp VocalizationsSpM or AllMM
    AllSoundsList=cell(length(x),length(param.exp.MotorStepList));
    ModelsList=cell(length(x),length(param.exp.MotorStepList));
    
    for kk = 1:length(x)
        sdname = tmp{kk}(x{kk}(1)+2:x{kk}(2)-6);
        for md = 1:param.snd.Nmodels
            if endsWith(sdname,param.snd.models{md})
                ModelsList{kk,ii} = param.snd.models{md};
                break;
            end
        end
        AllSoundsList{kk,ii} = regexprep(sdname,['_' ModelsList{kk,ii}],'');
    end
    
    %%% again this same pb
    if exist('split_in_2_exception','var') && split_in_2_exception
        EXPTsec= load([param.exp.Path '/MatFiles/' ReferenceNamesec.name(1:strfind(ReferenceNamesec.name,Refsec)+2) '.mat']);
        tmp = {EXPTsec.exptevents.Note};
        tmp = tmp(cellfun(@(x) isequal(x,1),strfind(tmp,'Stim')));
        tmp = tmp(arrayfun(@(x) isequal(x,0),contains(tmp,'Silence')));
        
        xsec = strfind(tmp,',');
        xsec(401:end) = [];
        
        for kk = 1:length(xsec)
            sdname = tmp{kk}(xsec{kk}(1)+2:xsec{kk}(2)-6);
            for md = 1:param.snd.Nmodels
                if endsWith(sdname,param.snd.models{md})
                    ModelsList{kk+400,ii} = param.snd.models{md};
                    break;
                end
            end
            AllSoundsList{kk+400,ii} = regexprep(sdname,['_' ModelsList{kk+400,ii}],'');
        end
    end
    %%%
    
    
    AllSoundsList=regexprep(AllSoundsList,'_1','');
    SoundList_tmp=unique(AllSoundsList(~cellfun(@isempty,AllSoundsList(:,ii)),ii));
    
    % Works only if sound is in target !!
    PreStimSilence =  EXPT(ii).exptparams.TrialObject.ReferenceHandle.PreStimSilence + ...
        EXPT(ii).exptparams.TrialObject.ReferenceHandle.Duration + ...
        EXPT(ii).exptparams.TrialObject.ReferenceHandle.PostStimSilence + ...
        EXPT(ii).exptparams.TrialObject.TargetHandle.PreStimSilence;
    
    % Ensure consistency across slices
    if ii == 1
        param.exp.PreStimSilence = PreStimSilence;
        stims_list = SoundList_tmp;
        Nstims = length(stims_list)*length(param.snd.models);
    
    else
        assert(param.exp.PreStimSilence == PreStimSilence);
        assert(isequal(SoundList_tmp,stims_list));
    end
    

    % Remove first trial if repeated twice, based on experiment's date
    tmp = param.exp.Files{1};
    u_idx = strfind(tmp,'_');
    Date = str2double([tmp(u_idx(1)+1:u_idx(2)-1), tmp(u_idx(2)+1:u_idx(3)-1), tmp(u_idx(3)+1:u_idx(4)-1)]);
    if  ~strcmp(param.exp.Animal,'Chabichou') && Date > 20190420 % tmp correction of first trial pb --- clean that
        param.exp.FirstStim = 2;
    else
        param.exp.FirstStim = 1;
    end
    
    % Reorder data by sound, model and rep
    
    data.Resp(:,:,:,:,:,:,ii) = nan(size(b0,1), size(b0,2), size(b0,3), param.snd.n_reps ,length(stims_list) ,length(param.snd.models)); % 4 reps has to be hardcoded
    for kk = param.exp.FirstStim:min(size(AllSoundsList,1),size(b0,4))
        sdNumber = find(strcmp(stims_list,AllSoundsList(kk,ii)));
        mdNumber = find(strcmp(param.snd.models,ModelsList(kk,ii)));
        repNumber = ceil((kk-param.exp.FirstStim+1)/(Nstims));
        param.snd.OrigTrialNum(sdNumber,mdNumber,repNumber,ii) = kk;
        data.Resp(:,:,:,repNumber,sdNumber,mdNumber,ii)=b0(:,:,:,kk);
    end
    
      

    
    if isfield(param.exp,'pupil') && param.exp.pupil
        pp = {EXPT(ii).exptevents.Note};
        pp = pp(cellfun(@(x) isequal(x,1),strfind(pp,'PUPIL')));
        
        if ~isempty(pp)
            ppE = pp(arrayfun(@(x) isequal(x,0),contains(pp,'STOP')));
            ppS = pp(arrayfun(@(x) isequal(x,1),contains(pp,'STOP')));
            x = strfind(ppE,',');
            z = strfind(ppS,',');
            param.pp.PupilTimes=[arrayfun(@(y) str2double(ppE{y}(x{y}(2)+1:end-1)),1:length(ppE)) ;arrayfun(@(y) str2double(ppS{y}(z{y}(2)+1:end-1)),1:length(ppS))]';
            param.pp.FrameRate=30;
        end
    end
    
end

param.exp.Duration = 10; % hardcoded
param.exp.nSteps = size(data.Resp,3);
param.exp.nTrials = size(data.Resp,4);
param.snd.SoundList = stims_list;
param.snd.Nsound = length(stims_list);

data.Anat = sqrt(squeeze(nanmean(nanmean(nanmean(nanmean(data.Resp,3),4),5),6)));

% Load mask
if param.msk.loadMask
    param.msk.ManualMask = LoadManualMask_All(data,param);
else
    param.msk.ManualMask = ones(size(data.Resp,1),size(data.Resp,2),size(data.Resp,7));
end


end