% THIS PARAMETER FILE IS A TEMPLATE.
% PLEASE MAKE A COPY BEFORE CHANGING.

param.rootDir = fullfile('F:\','fUS','\dynlearn');
param.animal = 'Rigotte';
param.session = '01';
param.addSlice = 0; % [0/1]: new slices will be added to a pre-existing scan file.

param.dataPath = fullfile(param.rootDir,param.animal,'Iconeus','FilteredData');
param.scanLoadFuntion = @makeScan; % function used to generate the brain scan. @makeScan_manualscan or @makeScan
