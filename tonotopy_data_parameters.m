% THIS PARAMETER FILE IS A TEMPLATE.
% PLEASE MAKE A COPY BEFORE CHANGING.

% Data parameters
param.CCAcorr = 1; % Use CCA corrected data or not.
param.rootDir = fullfile('F:\','fUS','dynlearn');
% param.rootDir = fullfile('F:\','fUS','pitchfus');
param.Type = 'FrequencyMap';
param.Animal = 'Rigotte';
param.Filter = ''; % empty string, picks all slices, else slice letter ('abcd...')
param.Session = '01';
param.Type = 'NSD';
param.scale_factor = 1/0.4;
param.DataPath = fullfile(param.rootDir,param.Animal,'processedData');
